/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.exception;

import es.iti.dm.governance.integration.atlas.service.impl.EntityTypeMissmatchException;
import jakarta.validation.ConstraintViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.net.SocketException;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;

@RestControllerAdvice
public class ExceptionsHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {MetadataStorageException.class})
    public ResponseEntity<ApiError> metadataStorageException(MetadataStorageException exception, final WebRequest webRequest){
        return createApiResponseEntity(webRequest, exception.getMessage(), exception.getHttpStatus());
    }

    @ExceptionHandler(value = {SocketException.class})
    public ResponseEntity<ApiError> qualifiedNameException(SocketException exception, final WebRequest webRequest){
        return createApiResponseEntity(webRequest, "Metadata repository unavailable", HttpStatus.SERVICE_UNAVAILABLE);
    }

    @ExceptionHandler(value = {ConstraintViolationException.class, DatabaseExistsOtherCatalogException.class, InvalidInputException.class, EntityTypeMissmatchException.class})
    public ResponseEntity<ApiError> badRequestException(RuntimeException exception, final WebRequest webRequest){
        return createApiResponseEntity(webRequest, exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {DeleteResourceInUseException.class})
    public ResponseEntity<ApiError> conflictException(RuntimeException exception, final WebRequest webRequest){
        return createApiResponseEntity(webRequest, exception.getMessage(), HttpStatus.CONFLICT);
    }

    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        List<String> errorList = ex
                .getBindingResult()
                .getFieldErrors()
                .stream()
                .map(e -> format("%s: %s", e.getField(), e.getDefaultMessage()))
                .collect(Collectors.toList());

        ex.getBindingResult().getGlobalErrors().stream()
                .forEach(e -> errorList.add(format("%s: %s", e.getObjectName(), e.getDefaultMessage())));

        var responseEntity = createApiResponseEntity(request, String.join(", ", errorList), HttpStatus.valueOf(status.value()));
        return new ResponseEntity<>(responseEntity.getBody(), responseEntity.getStatusCode());
    }

    //TODO REMOVE. only for develop purpose
    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<ApiError> generalException(RuntimeException exception, final WebRequest webRequest){
        return createApiResponseEntity(webRequest, exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<ApiError> createApiResponseEntity(WebRequest webRequest, String message, HttpStatus httpStatus) {
        var uri = ((ServletWebRequest) webRequest).getRequest().getRequestURI();
        return new ResponseEntity<>(
                new ApiError(message, uri, httpStatus),
                httpStatus
        );
    }

}
