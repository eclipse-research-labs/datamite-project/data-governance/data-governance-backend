/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.exception;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import es.iti.dm.governance.integration.atlas.utils.AtlasErrorResponse;
import lombok.Data;
import org.apache.atlas.AtlasServiceException;
import org.springframework.http.HttpStatus;

import static es.iti.dm.governance.util.ErrorMessages.METADATA_REPOSITORY_ERROR;

@Data
public class MetadataStorageException extends RuntimeException{
    private static final long serialVersionUID = -4507387430002604434L;
    private static final int ATLAS_SPLIT_ERROR_MESSAGE_NUMBER = 3;

    private final HttpStatus httpStatus;

    public MetadataStorageException(AtlasServiceException atlasServiceException){
        // con split [()] se parte en 4 siendo la parte 4 la que contiene el código y mensaje de error
        super(
                (atlasServiceException.getMessage().split("[()]").length < 4)
                ? METADATA_REPOSITORY_ERROR
                : ((AtlasErrorResponse)new Gson().fromJson(atlasServiceException.getMessage().split("[()]")[ATLAS_SPLIT_ERROR_MESSAGE_NUMBER], new TypeToken<AtlasErrorResponse>(){}.getType())).getErrorMessage()
                );
        this.httpStatus = HttpStatus.valueOf(atlasServiceException.getStatus().getStatusCode());
    }
}
