/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.mapper;

import com.google.gson.Gson;
import es.iti.dm.governance.dto.abstractmodel.*;
import es.iti.dm.governance.dto.catalog.CatalogEntityDto;
import es.iti.dm.governance.dto.catalog.CreateCatalogEntityDto;
import es.iti.dm.governance.dto.column.ColumnEntityDto;
import es.iti.dm.governance.dto.column.CreateDatabaseColumnEntityDto;
import es.iti.dm.governance.dto.database.CreateDatabaseEntityDto;
import es.iti.dm.governance.dto.database.DatabaseEntityDto;
import es.iti.dm.governance.dto.distribution.CreateDistributionDto;
import es.iti.dm.governance.dto.distribution.DistributionDto;
import es.iti.dm.governance.dto.domain.DomainRelationDto;
import es.iti.dm.governance.dto.glossary.term.DisassociateTermDto;
import es.iti.dm.governance.dto.quality.QualityMeasurementDto;
import es.iti.dm.governance.dto.quality.QualityMeasurementParameterDto;
import es.iti.dm.governance.dto.search.EntityType;
import es.iti.dm.governance.dto.search.SearchResultDto;
import es.iti.dm.governance.dto.structureddataset.CreateStructuredDatasetEntityDto;
import es.iti.dm.governance.dto.structureddataset.StructuredDatasetEntityDto;
import es.iti.dm.governance.dto.table.CreateTableEntityDto;
import es.iti.dm.governance.dto.table.TableEntityDto;
import org.apache.atlas.model.glossary.relations.AtlasTermAssignmentHeader;
import org.apache.atlas.model.instance.AtlasClassification;
import org.apache.atlas.model.instance.AtlasEntity;
import org.apache.atlas.model.instance.AtlasEntityHeader;
import org.apache.atlas.model.instance.AtlasRelatedObjectId;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static es.iti.dm.governance.integration.atlas.utils.Constants.*;
import static es.iti.dm.governance.util.EntitiesConstants.*;

@Mapper(uses = {AtlasEntityAttributesUtil.class})
public interface AtlasEntityMapper {
    AtlasEntityMapper MAPPER = Mappers.getMapper(AtlasEntityMapper.class);

    public static final String PARENT_BASE_GUID = "-1";

    //CREATE

    @Mapping(target = "typeName", constant = CATALOG_ENTITY_TYPE)
    AtlasEntity fromCreateOrUpdateCatalogEntity(CreateCatalogEntityDto createCatalogEntityDto, String qualifiedName);
    @AfterMapping
    default void finishFromCreateOrUpdateCatalogEntity(@MappingTarget AtlasEntity atlasEntity, CreateCatalogEntityDto createCatalogEntityDto, String qualifiedName) {
        Map<String, Object> attributes = createCommonAtlasEntityAttributes(createCatalogEntityDto, qualifiedName);
        if (createCatalogEntityDto.getMetadata() != null) {
            addResourceMetadata(attributes, createCatalogEntityDto.getMetadata());
        }
        atlasEntity.setAttributes(attributes);
    }

    default List<AtlasEntity> fromCreateOrUpdateDatabaseEntityList(CreateDatabaseEntityDto createDatabaseEntityDto, String qualifiedName) {
        List<AtlasEntity> atlasEntityList = new ArrayList<>();
        AtomicInteger idGenerator = new AtomicInteger(Integer.valueOf(PARENT_BASE_GUID));
        String id = String.valueOf(idGenerator.getAndDecrement());
        atlasEntityList.add(fromCreateOrUpdateDatabaseEntity(createDatabaseEntityDto, qualifiedName, id));
        if (createDatabaseEntityDto.getTables() != null) {
            //crear tablas pasando id = -1 y qualifiedName
            atlasEntityList.addAll(fromCreateTablesToTablesEntities(createDatabaseEntityDto.getTables(), qualifiedName, id, idGenerator));
        }
        atlasEntityList.addAll(fromCreateDistributionsToDistributionEntities(createDatabaseEntityDto.getDistributions(), qualifiedName, id, idGenerator));
        return atlasEntityList;
    }

    @Mapping(target = "typeName", constant = DATABASE_ENTITY_TYPE)
    @Mapping(target = "guid", source = "id")
    AtlasEntity fromCreateOrUpdateDatabaseEntity(CreateDatabaseEntityDto createDatabaseEntityDto, String qualifiedName, String id);
    @AfterMapping
    default void finishFromCreateOrUpdateDatabaseEntity(@MappingTarget AtlasEntity atlasEntity, CreateDatabaseEntityDto createDatabaseEntityDto, String qualifiedName, String id) {
        Map<String, Object> attributes = createCommonAtlasEntityAttributes(createDatabaseEntityDto, qualifiedName);
        addResourceMetadata(attributes, createDatabaseEntityDto.getMetadata());
        if (createDatabaseEntityDto.getMetadata().getType() != null) {
            attributes.put(DB_TYPE_ATTRIBUTE, createDatabaseEntityDto.getMetadata().getType());
        }
        atlasEntity.setAttributes(attributes);
        Map<String, Object> catalogRelation = new HashMap<>();
        catalogRelation.put(ATTRIBUTE_TYPE_NAME, CATALOG_ENTITY_TYPE);
        catalogRelation.put(ATTRIBUTE_GUID, createDatabaseEntityDto.getCatalogId());
        atlasEntity.setRelationshipAttribute(CATALOG_RELATION_ATTRIBUTE, catalogRelation);
    }

    //TODO consider refactoring - instead of having these methods in mapper move them to services
    default List<AtlasEntity> fromCreateTablesToTablesEntities(List<CreateTableEntityDto> createTableEntityDtoList, String parentQualifiedName, String parentId, AtomicInteger idGenerator) {
        List<AtlasEntity> atlasEntityList = new ArrayList<>();
        createTableEntityDtoList.stream().forEach(tableDto -> {
            String qualifiedName = parentQualifiedName.concat("@").concat(tableDto.getMetadata().getTitle());
            String id = String.valueOf(idGenerator.getAndDecrement());
            atlasEntityList.add(fromCreateTableToTableEntity(tableDto, qualifiedName, id, parentId));
            atlasEntityList.addAll(fromCreateColumnsToColumnsEntities(tableDto.getColumns(), qualifiedName, id, idGenerator));
        });
        return atlasEntityList;
    }

    @Mapping(target = "typeName", constant = TABLE_ENTITY_TYPE)
    @Mapping(target = "guid", source = "id")
    AtlasEntity fromCreateTableToTableEntity(CreateTableEntityDto createTableEntityDto, String qualifiedName, String id, String parentId);

    @AfterMapping
    default void finishFromCreateTableToTableEntity(@MappingTarget AtlasEntity atlasEntity, CreateTableEntityDto createTableEntityDto, String qualifiedName, String id, String parentId) {
        Map<String, Object> attributes = createCommonAtlasEntityAttributes(createTableEntityDto, qualifiedName);
        addResourceMetadata(attributes, createTableEntityDto.getMetadata());
        if (createTableEntityDto.getMetadata().getSchema() != null) {
            attributes.put(SCHEMA_ATTRIBUTE, createTableEntityDto.getMetadata().getSchema());
        }
        if (createTableEntityDto.getMetadata().getIsPrivate() != null) {
            attributes.put(IS_PRIVATE_ATTRIBUTE, createTableEntityDto.getMetadata().getIsPrivate());
        }
        atlasEntity.setAttributes(attributes);

        Map<String, Object> dbRelation = new HashMap<>();
        dbRelation.put(ATTRIBUTE_TYPE_NAME, DATABASE_ENTITY_TYPE);
        dbRelation.put(ATTRIBUTE_GUID, parentId);
        atlasEntity.setRelationshipAttribute(DATABASE_RELATION_ATTRIBUTE, dbRelation);
    }

    default List<AtlasEntity> fromCreateColumnsToColumnsEntities(List<CreateDatabaseColumnEntityDto> createDatabaseColumnEntityDtos, String parentQualifiedName, String parentId, AtomicInteger idGenerator) {
        List<AtlasEntity> atlasEntityList = new ArrayList<>();
        createDatabaseColumnEntityDtos.stream().forEach(columnDto -> {
            String qualifiedName = parentQualifiedName.concat(".").concat(columnDto.getMetadata().getTitle());
            String id = String.valueOf(idGenerator.getAndDecrement());
            atlasEntityList.add(fromCreateColumnToColumnEntity(columnDto, qualifiedName, id, parentId));
            if ( columnDto.getQualityMeasurements() != null) {
                atlasEntityList.addAll(fromQualityMeasurements(columnDto.getQualityMeasurements(), id, idGenerator, qualifiedName));
            }
        });
        return atlasEntityList;
    }

    @Mapping(target = "typeName", constant = COLUMN_ENTITY_TYPE)
    @Mapping(target = "guid", source = "id")
    AtlasEntity fromCreateColumnToColumnEntity(CreateDatabaseColumnEntityDto createDatabaseColumnEntityDto, String qualifiedName, String id, String parentId);

    @AfterMapping
    default void finishFromCreateColumnToColumnEntity(@MappingTarget AtlasEntity atlasEntity, CreateDatabaseColumnEntityDto createDatabaseColumnEntityDto, String qualifiedName, String id, String parentId) {
        Map<String, Object> attributes = createCommonAtlasEntityAttributes(createDatabaseColumnEntityDto, qualifiedName);
        addResourceMetadata(attributes, createDatabaseColumnEntityDto.getMetadata());
        if (createDatabaseColumnEntityDto.getMetadata().getDataType() != null) {
            attributes.put(DATA_TYPE_ATTRIBUTE, createDatabaseColumnEntityDto.getMetadata().getDataType());
        }
        if (createDatabaseColumnEntityDto.getMetadata().getPosition() != null) {
            attributes.put(POSITION_ATTRIBUTE, createDatabaseColumnEntityDto.getMetadata().getPosition());
        } else {
            attributes.put(POSITION_ATTRIBUTE, -1);
        }
        if (createDatabaseColumnEntityDto.getMetadata().getNumRecords() != null) {
            attributes.put(NUM_RECORDS_ATTRIBUTE, createDatabaseColumnEntityDto.getMetadata().getNumRecords());
        }
        if (createDatabaseColumnEntityDto.getMetadata().getIsPrivate() != null) {
            attributes.put(IS_PRIVATE_ATTRIBUTE, createDatabaseColumnEntityDto.getMetadata().getIsPrivate());
        }
        atlasEntity.setAttributes(attributes);

        Map<String, Object> tableRelation = new HashMap<>();
        tableRelation.put(ATTRIBUTE_TYPE_NAME, TABLE_ENTITY_TYPE);
        tableRelation.put(ATTRIBUTE_GUID, parentId);
        atlasEntity.setRelationshipAttribute(DATASET_RELATION_ATTRIBUTE, tableRelation);
    }

    default List<AtlasEntity> fromCreateStructuredDatasetToStructuredDatasetEntities(CreateStructuredDatasetEntityDto createStructuredDatasetEntityDto, String qualifiedName) {
        List<AtlasEntity> atlasEntityList = new ArrayList<>();
        AtomicInteger idGenerator = new AtomicInteger(Integer.valueOf(PARENT_BASE_GUID));
        String id = String.valueOf(idGenerator.getAndDecrement());
        atlasEntityList.add(fromCreateStructuredDatasetToStructuredDatasetEntity(createStructuredDatasetEntityDto, qualifiedName, id));
        atlasEntityList.addAll(fromCreateDistributionsToDistributionEntities(createStructuredDatasetEntityDto.getDistributions(), qualifiedName, id, idGenerator));
        return atlasEntityList;
    }

    @Mapping(target = "typeName", constant = STRUCTURED_DATASET_ENTITY_TYPE)
    @Mapping(target = "guid", source = "id")
    AtlasEntity fromCreateStructuredDatasetToStructuredDatasetEntity(CreateStructuredDatasetEntityDto createStructuredDatasetEntityDto, String qualifiedName, String id);

    @AfterMapping
    default void finishFromCreateStructuredDatasetToStructuredDatasetEntity(@MappingTarget AtlasEntity atlasEntity, CreateStructuredDatasetEntityDto createStructuredDatasetEntityDto, String qualifiedName, String id) {
        Map<String, Object> attributes = createCommonAtlasEntityAttributes(createStructuredDatasetEntityDto, qualifiedName);
        addResourceMetadata(attributes, createStructuredDatasetEntityDto.getMetadata());
        atlasEntity.setAttributes(attributes);

        Map<String, Object> catalogRelation = new HashMap<>();
        catalogRelation.put(ATTRIBUTE_TYPE_NAME, CATALOG_ENTITY_TYPE);
        catalogRelation.put(ATTRIBUTE_GUID, createStructuredDatasetEntityDto.getCatalogId());
        atlasEntity.setRelationshipAttribute(CATALOG_RELATION_ATTRIBUTE, catalogRelation);
    }

    default List<AtlasEntity> fromCreateDistributionsToDistributionEntities(List<CreateDistributionDto> createDistributionDtos, String parentQualifiedName, String parentId, AtomicInteger idGenerator) {
        List<AtlasEntity> atlasEntityList = new ArrayList<>();
        createDistributionDtos.stream().forEach(distributionDto -> {
            //TODO maybe should use accessUrl or mediaType instead name
            String qualifiedName = parentQualifiedName.concat("@").concat(distributionDto.getTitle());
            String id = String.valueOf(idGenerator.getAndDecrement());
            atlasEntityList.add(fromCreateDistributionToDistributionEntity(distributionDto, qualifiedName, id, parentId));
        });
        return atlasEntityList;
    }

    @Mapping(target = "typeName", constant = DISTRIBUTION_ENTITY)
    @Mapping(target = "guid", source = "id")
    AtlasEntity fromCreateDistributionToDistributionEntity(CreateDistributionDto createDistributionDto, String qualifiedName, String id, String parentId);

    @AfterMapping
    default void finishFromCreateDistributionToDistributionEntity(@MappingTarget AtlasEntity atlasEntity, CreateDistributionDto createDistributionDto, String qualifiedName, String id, String parentId) {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put(ATTRIBUTE_QUALIFIED_NAME, qualifiedName);
        attributes.put(ENTITY_NAME_ATTRIBUTE, createDistributionDto.getTitle());
        attributes.put(DESCRIPTION_ATTRIBUTE, createDistributionDto.getDescription());
        attributes.put(ACCESS_URL_ATTRIBUTE, createDistributionDto.getAccessUrl());
        //TODO we should allow that byteSize in database can be 0?
        attributes.put(BYTE_SIZE_ATTRIBUTE, (createDistributionDto.getByteSize() == null) ? 0 : createDistributionDto.getByteSize());
        attributes.put(MEDIA_TYPE_ATTRIBUTE, createDistributionDto.getMediaType());
        attributes.put(FORMAT_ATTRIBUTE, createDistributionDto.getFormat());
        attributes.put(CONFORMS_TO_ATTRIBUTE, createDistributionDto.getConformsTo());
        attributes.put(CHECKSUM_ATTRIBUTE, createDistributionDto.getChecksum());
        attributes.put(DECIMAL_DELIMITER_ATTRIBUTE, createDistributionDto.getDecimalDelimiter());
        attributes.put(FIELD_DELIMITER_ATTRIBUTE, createDistributionDto.getFieldDelimiter());
        atlasEntity.setAttributes(attributes);

        Map<String, Object> datasetRelation = new HashMap<>();
        datasetRelation.put(ATTRIBUTE_TYPE_NAME, DATASET_ENTITY_TYPE);
        datasetRelation.put(ATTRIBUTE_GUID, parentId);
        atlasEntity.setRelationshipAttribute(DATASET_RELATION_ATTRIBUTE, datasetRelation);
    }

    //To update, first recover the original entity because the mandatory QualifiedName.
    default AtlasEntity updateEntity(AtlasEntity atlasEntity, CreateOrUpdateEntityDto createOrUpdateEntityDto) {
        AtlasEntity newAtlasEntity = new AtlasEntity(atlasEntity);
        if (createOrUpdateEntityDto.getName() != null) {
            newAtlasEntity.setAttribute(ENTITY_NAME_ATTRIBUTE, createOrUpdateEntityDto.getName());
        }
        if (createOrUpdateEntityDto.getDescription() != null) {
            newAtlasEntity.setAttribute(ENTITY_DESCRIPTION_ATTRIBUTE, createOrUpdateEntityDto.getDescription());
        }
        if (createOrUpdateEntityDto.getSummary() != null) {
            newAtlasEntity.setAttribute(ENTITY_SUMMARY_ATTRIBUTE, createOrUpdateEntityDto.getSummary());
        }
        return newAtlasEntity;
    }

    default List<AtlasEntity> fromQualityMeasurements(List<QualityMeasurementDto> qualityMeasurementDtos, String parentId, AtomicInteger idGenerator, String parentQualifiedName) {
        List<AtlasEntity> atlasEntityList = new ArrayList<>();
        qualityMeasurementDtos.stream().forEach(qualityMeasurementDto -> {
            String qualifiedName = parentQualifiedName.concat("_").concat(qualityMeasurementDto.getMetric());
            String id = String.valueOf(idGenerator.getAndDecrement());
            atlasEntityList.add(fromQualityMeasurement(qualityMeasurementDto, qualifiedName, id, parentId));
        });
        return atlasEntityList;
    }

    @Mapping(target = "typeName", constant = QUALITY_MEASUREMENT_ENTITY_TYPE)
    @Mapping(target = "guid", source = "id")
    AtlasEntity fromQualityMeasurement(QualityMeasurementDto qualityMeasurementDto, String qualifiedName, String id, String parentId);

    @AfterMapping
    default void finishFromQualityMeasurement(@MappingTarget AtlasEntity atlasEntity, QualityMeasurementDto qualityMeasurementDto, String qualifiedName, String id, String parentId) {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put(ENTITY_NAME_ATTRIBUTE, qualityMeasurementDto.getName());
        attributes.put(ATTRIBUTE_QUALIFIED_NAME, qualifiedName);
        attributes.put(VALUE_ATTRIBUTE, qualityMeasurementDto.getValue());
        attributes.put(CREATED_ATTRIBUTE, (qualityMeasurementDto.getCreatedAt() == null) ? Instant.now().getEpochSecond() : qualityMeasurementDto.getCreatedAt().getEpochSecond());
        attributes.put(CREATED_BY_ATTRIBUTE, String.valueOf(qualityMeasurementDto.getCreatedBy()));
        attributes.put(METRIC_ATTRIBUTE, qualityMeasurementDto.getMetric());
        attributes.put(PARAMETERS_ATTRIBUTE, qualityMeasurementParametersToString(qualityMeasurementDto.getParameters()));
        atlasEntity.setAttributes(attributes);

        Map<String, Object> datasetRelation = new HashMap<>();
        datasetRelation.put(ATTRIBUTE_TYPE_NAME, DATASET_ENTITY_TYPE);
        datasetRelation.put(ATTRIBUTE_GUID, parentId);

        atlasEntity.setRelationshipAttribute(DATASET_RELATION_ATTRIBUTE, datasetRelation);
    }

    default String qualityMeasurementParameterToString(QualityMeasurementParameterDto qualityMeasurementParameterDto) {
        return new Gson().toJson(qualityMeasurementParameterDto);
    }

    List<String> qualityMeasurementParametersToString(List<QualityMeasurementParameterDto> qualityMeasurementParameterDtoList);

    //GET

    @AtlasEntityToEntityCommon
    @AtlasEntityToMetadataCommon
    @Mapping(target = "datasets", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeDatasetsRelation.class)
    CatalogEntityDto toCatalogEntity(AtlasEntity atlasEntity);

    @AtlasEntityToEntityCommon
    @AtlasEntityToMetadataCommon
    @Mapping(target = "metadata.type", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeDBType.class)
    @Mapping(target = "tables", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeTablesRelation.class)
    @Mapping(target = "catalog", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeCatalogRelation.class)
    DatabaseEntityDto toDatabaseEntity(AtlasEntity atlasEntity);

    @AtlasEntityToEntityCommon
    @AtlasEntityToMetadataCommon
    @Mapping(target = "metadata.schema", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeSchema.class)
    @Mapping(target = "metadata.isPrivate", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeIsPrivate.class)
    @Mapping(target = "columns", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeColumnsRelation.class)
    @Mapping(target = "database", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeDatabaseRelation.class)
    TableEntityDto toTableEntity(AtlasEntity atlasEntity);

    @AtlasEntityToEntityCommon
    @AtlasEntityToMetadataCommon
    @Mapping(target = "metadata.dataType", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeDataType.class)
    @Mapping(target = "metadata.position", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributePosition.class)
    @Mapping(target = "metadata.numRecords", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeNumRecords.class)
    @Mapping(target = "metadata.isPrivate", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeIsPrivate.class)
    @Mapping(target = "dataset", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeDatasetRelation.class)
    @Mapping(target = "qualityMeasurements", ignore = true)
    ColumnEntityDto toColumnEntity(AtlasEntity atlasEntity);

    @AtlasEntityToEntityCommon
    @AtlasEntityToMetadataCommon
    @Mapping(target = "catalog", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeCatalogRelation.class)
    @Mapping(target = "columns", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeColumnsRelation.class)
    StructuredDatasetEntityDto toStructuredDatasetEntity(AtlasEntity atlasEntity);

    @Mapping(target = "id", source = "guid")
    @Mapping(target = "title", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeEntityName.class)
    @Mapping(target = "description", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeDescription.class)
    @Mapping(target = "accessUrl", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeAccessUrl.class)
    @Mapping(target = "byteSize", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeByteSize.class)
    @Mapping(target = "mediaType", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeMediaType.class)
    @Mapping(target = "format", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeFormat.class)
    @Mapping(target = "conformsTo", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeConformsTo.class)
    @Mapping(target = "checksum", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeChecksum.class)
    @Mapping(target = "decimalDelimiter", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeDecimalDelimiter.class)
    @Mapping(target = "fieldDelimiter", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeFieldDelimiter.class)
    DistributionDto toDistributionEntity(AtlasEntity atlasEntity);

    @Mapping(target = "name", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeEntityName.class)
    @Mapping(target = "metric", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeMetric.class)
    @Mapping(target = "value", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeValue.class)
    @Mapping(target = "createdAt", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeCreatedInstant.class)
    @Mapping(target = "createdBy", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeCreatedBy.class)
    @Mapping(target = "parameters", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeQualityMeasurementParameters.class)
    QualityMeasurementDto toQualityMeasurement(AtlasEntity atlasEntity);

    @Mapping(target = "id", source = "guid")
    @Mapping(target = "name", source = "displayText")
    @Mapping(target = "type", source = "typeName")
    @Mapping(target = "relationId", source = "relationshipGuid")
    EntityRelationHeaderDto fromRelatedObjectId(AtlasRelatedObjectId atlasRelatedObjectId);

    @InheritInverseConfiguration
    AtlasRelatedObjectId toRelatedObjectId(EntityRelationHeaderDto entityRelationHeaderDto);

    List<AtlasRelatedObjectId> toRelatedObjectIdList(List<EntityRelationHeaderDto> entityRelationHeaderDtoList);

    @Mapping(target = "guid", source = "id")
    @Mapping(target = "relationshipGuid", source = "relationId")
    AtlasRelatedObjectId disassociateTermToRelatedObjectId(DisassociateTermDto disassociateTermDto);

    List<AtlasRelatedObjectId> disassociateTermListToRelatedObjectIdList(List<DisassociateTermDto> disassociateTermDtoList);

    @Mapping(target = "id", source = "guid")
    @Mapping(target = "type", source = "typeName")
    @Mapping(target = "name", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeEntityName.class)
    @Mapping(target = "description", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeEntityDescription.class)
    @Mapping(target = "summary", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeEntitySummary.class)
    @Mapping(target = "terms", source = "meanings")
    @Mapping(target = "domains", source = "classifications")
    SearchResultDto fromAtlasEntityHeaderToSearchResult(AtlasEntityHeader atlasEntityHeader);
    List<SearchResultDto> fromAtlasEntityHeaderToSearchResultList(List<AtlasEntityHeader> atlasEntityHeaderList);

    @Mapping(target = "id", source = "termGuid")
    @Mapping(target = "relationId", source = "relationGuid")
    @Mapping(target = "name", source = "displayText")
    EntityRelationHeaderDto fromAtlasTermAssigmentHeader(AtlasTermAssignmentHeader atlasTermAssignmentHeader);

    List<EntityRelationHeaderDto> fromAtlasTermAssigmentHeaderList(List<AtlasTermAssignmentHeader> atlasTermAssignmentHeader);

    default EntityType fromString(String type) {
        return EntityType.findByValue(type);
    }

    @Mapping(target = "id", source = "guid")
    @Mapping(target = "name", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeEntityName.class)
    @Mapping(target = "type", source = "typeName")
    @Mapping(target = "qualifiedName", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeQualifiedName.class)
    @Mapping(target = "terms", source = "meanings")
    @Mapping(target = "domains", source = "classificationNames")
    EntityHeaderExtDto fromAtlasEntityHeader(AtlasEntityHeader atlasEntityHeader);

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    List<EntityHeaderExtDto> fromAtlasEntityHeaderToList(List<AtlasEntityHeader> atlasEntityHeaderList);

    private Map<String, Object> createCommonAtlasEntityAttributes(CreateOrUpdateEntityDto createOrUpdateEntityDto, String qualifiedName) {
        //Name is mandatory. Qualified is mandatory but the user does not inform it. Others only added if not null to not override in updates
        Map<String, Object> attributes = new HashMap<>();
        if (qualifiedName != null) {
            attributes.put(ATTRIBUTE_QUALIFIED_NAME, qualifiedName);
        }
        attributes.put(ENTITY_NAME_ATTRIBUTE, createOrUpdateEntityDto.getName());
        if (createOrUpdateEntityDto.getDescription() != null) {
            attributes.put(ENTITY_DESCRIPTION_ATTRIBUTE, createOrUpdateEntityDto.getDescription());
        }
        if (createOrUpdateEntityDto.getSummary() != null) {
            attributes.put(ENTITY_SUMMARY_ATTRIBUTE, createOrUpdateEntityDto.getSummary());
        }
        //TODO authentication
//        attributes.put(ENTITY_OWNER_ATTRIBUTE, );
//        attributes.put(ENTITY_OWNER_NAME_ATTRIBUTE, ownerName);
        return attributes;
    }

    private void addResourceMetadata(Map<String, Object> attributes, ResourceDto resourceDto) {
        //TODO maybe should remove if conditions to allow remove attribute values in pipelines ingestion
        if (resourceDto.getIdentifier() != null) {
            attributes.put(IDENTIFIER_ATTRIBUTE, resourceDto.getIdentifier());
        }
        if (resourceDto.getTitle() != null) {
            attributes.put(TITLE_ATTRIBUTE, resourceDto.getTitle());
        }
        if (resourceDto.getDescription() != null) {
            attributes.put(DESCRIPTION_ATTRIBUTE, resourceDto.getDescription());
        }
        if (resourceDto.getCreator() != null) {
            attributes.put(CREATOR_ATTRIBUTE, resourceDto.getCreator());
        }
        if (resourceDto.getCreated() != null) {
            attributes.put(CREATED_ATTRIBUTE, resourceDto.getCreated().getEpochSecond());
        }
    }

    @Retention(RetentionPolicy.CLASS)
    @Mapping(target = "id", source = "guid")
//    @Mapping(target = "qualifiedName", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeQualifiedName.class)
    @Mapping(target = "name", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeEntityName.class)
    @Mapping(target = "description", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeEntityDescription.class)
    @Mapping(target = "summary", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeEntitySummary.class)
    @Mapping(target = "owner", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeEntityOwner.class)
    @Mapping(target = "ownerName", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeEntityOwnerName.class)
    @Mapping(target = "createdAt", source = "createTime")
    @Mapping(target = "updatedAt", source = "updateTime")
    @Mapping(target = "updatedBy", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeEntityUpdatedBy.class)
    @Mapping(target = "labels", source = "labels")
    @Mapping(target = "terms", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeTermsRelation.class)
    @Mapping(target = "domains", source = ".", qualifiedByName = "getDomainsFromEntity")
    public @interface AtlasEntityToEntityCommon {}

    @Mapping(target = "metadata.identifier", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeIdentifier.class)
    @Mapping(target = "metadata.title", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeTitle.class)
    @Mapping(target = "metadata.description", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeDescription.class)
    @Mapping(target = "metadata.creator", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeCreator.class)
    @Mapping(target = "metadata.created", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeCreatedInstant.class)
    public @interface AtlasEntityToMetadataCommon {}

    @Mapping(target = "name", source = "typeName")
    @Mapping(target = "propagatedBy", source = "entityGuid")
    DomainRelationDto atlasClassificationToDomainRelationDto(AtlasClassification atlasClassification);
    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    List<DomainRelationDto> atlasClassificationListToDomainRelationDtoList(List<AtlasClassification> atlasClassification);

    @Named("getDomainsFromEntity")
    default List<DomainRelationDto> getDomainsFromEntity(AtlasEntity atlasEntity) {
        List<DomainRelationDto> domainRelationDtoList = atlasClassificationListToDomainRelationDtoList(atlasEntity.getClassifications());
        domainRelationDtoList.stream()
                .filter(domainRelationDto -> domainRelationDto.getPropagatedBy().toString().equals(atlasEntity.getGuid()))
                .forEach(domainRelationDto -> domainRelationDto.setPropagatedBy(null));
        return  domainRelationDtoList;
    }
    default String fromAtlasClassificationGetName(AtlasClassification atlasClassification) {
        return atlasClassification.getTypeName();
    }
}
