/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.mapper;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.iti.dm.governance.dto.column.ColumnSimplDto;
import es.iti.dm.governance.dto.quality.QualityMeasurementSimplDto;
import es.iti.dm.governance.dto.table.TableSimplDto;
import org.apache.atlas.model.instance.AtlasEntity;
import org.apache.atlas.model.instance.AtlasRelatedObjectId;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.List;

import static es.iti.dm.governance.util.EntitiesConstants.*;

@Mapper(uses = {AtlasEntityAttributesUtil.class})
public interface AtlasEntitySimplMapper {
    AtlasEntitySimplMapper MAPPER = Mappers.getMapper(AtlasEntitySimplMapper.class);

    @AtlasToEntitySimplCommon
    TableSimplDto mapToTable(AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo);

    @AtlasToEntitySimplCommon
    @Mapping(target = "dataType", source = "entity.attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeDataType.class)
    @Mapping(target = "position", source = "entity.attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributePosition.class)
    @Mapping(target = "qualityMeasurements", source = "atlasEntityWithExtInfo", qualifiedByName = "GetQualityMeasurementsFromExtInfo")
    ColumnSimplDto mapToColumn(AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo);

    @Mapping(target = "metric", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeMetric.class)
    @Mapping(target = "value", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeValue.class)
    QualityMeasurementSimplDto mapToQualityMeasurement(AtlasEntity atlasEntity);

    @Mapping(target = "id", source = "entity.guid")
    @Mapping(target = "name", source = "entity.attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeEntityName.class)
    public @interface AtlasToEntitySimplCommon {}

    default List<TableSimplDto> getTablesSimplFromExtInfo(AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo) {
        List<TableSimplDto> tableSimplDtoList = new ArrayList<>();
        var tables = atlasEntityWithExtInfo.getEntity().getRelationshipAttribute(TABLES_RELATION_ATTRIBUTE);
        if (tables != null) {
            List<AtlasRelatedObjectId> atlasObjectIdList = new ObjectMapper().convertValue(tables, new TypeReference<List<AtlasRelatedObjectId>>() {});
            atlasObjectIdList.stream()
                    .filter(atlasRelatedObjectId -> atlasRelatedObjectId.getEntityStatus().equals(AtlasEntity.Status.ACTIVE))
                    .forEach(atlasRelatedObjectId -> {
                        AtlasEntity tableEntity = atlasEntityWithExtInfo.getReferredEntities().get(atlasRelatedObjectId.getGuid());
                        tableSimplDtoList.add(mapToTable(new AtlasEntity.AtlasEntityWithExtInfo(tableEntity, new AtlasEntity.AtlasEntityExtInfo(atlasEntityWithExtInfo.getReferredEntities()))));
                    });
        }
        return tableSimplDtoList;
    }

    @Named("GetColumnsFromExtInfo")
    default List<ColumnSimplDto> getColumnsSimplFromExtInfo(AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo) {
        List<ColumnSimplDto> columnSimplDtoList = new ArrayList<>();
        var columns = atlasEntityWithExtInfo.getEntity().getRelationshipAttribute(COLUMNS_RELATION_ATTRIBUTE);
        if (columns != null) {
            List<AtlasRelatedObjectId> atlasObjectIdList = new ObjectMapper().convertValue(columns, new TypeReference<List<AtlasRelatedObjectId>>() {});
            atlasObjectIdList.stream()
                    .filter(atlasRelatedObjectId -> atlasRelatedObjectId.getEntityStatus().equals(AtlasEntity.Status.ACTIVE))
                    .forEach(atlasRelatedObjectId -> {
                        AtlasEntity columnEntity = atlasEntityWithExtInfo.getReferredEntities().get(atlasRelatedObjectId.getGuid());
                        columnSimplDtoList.add(mapToColumn(new AtlasEntity.AtlasEntityWithExtInfo(columnEntity, new AtlasEntity.AtlasEntityExtInfo(atlasEntityWithExtInfo.getReferredEntities()))));
                    });
        }
        return columnSimplDtoList;
    }

    @Named("GetQualityMeasurementsFromExtInfo")
    default List<QualityMeasurementSimplDto> getQualityMeasurementSimplFromExtInfo(AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo) {
        List<QualityMeasurementSimplDto> qualityMeasurementSimplDtoList = new ArrayList<>();
        var columns = atlasEntityWithExtInfo.getEntity().getRelationshipAttribute(QUALITY_MEASUREMENTS_RELATION_ATTRIBUTE);
        if (columns != null) {
            List<AtlasRelatedObjectId> atlasObjectIdList = new ObjectMapper().convertValue(columns, new TypeReference<List<AtlasRelatedObjectId>>() {});
            atlasObjectIdList.stream()
                    .filter(atlasRelatedObjectId -> atlasRelatedObjectId.getEntityStatus().equals(AtlasEntity.Status.ACTIVE))
                    .forEach(atlasRelatedObjectId -> {
                        AtlasEntity qualityMeasurementEntity = atlasEntityWithExtInfo.getReferredEntities().get(atlasRelatedObjectId.getGuid());
                        qualityMeasurementSimplDtoList.add(mapToQualityMeasurement(qualityMeasurementEntity));
                    });
        }
        return qualityMeasurementSimplDtoList;
    }
}
