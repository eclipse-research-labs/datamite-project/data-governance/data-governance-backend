/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.service.impl;

import es.iti.dm.governance.dto.glossary.term.CreateTermRelationDto;
import es.iti.dm.governance.integration.atlas.repository.AtlasRelationsRepository;
import es.iti.dm.governance.integration.atlas.service.AtlasRelationsService;
import lombok.AllArgsConstructor;
import org.apache.atlas.model.instance.AtlasObjectId;
import org.apache.atlas.model.instance.AtlasRelationship;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static es.iti.dm.governance.integration.atlas.utils.Constants.*;
import static es.iti.dm.governance.util.ErrorMessages.DELETE_TERM_TERM_RELATION_TYPE_MISMATCH_EXCEPTION;

@Service
@AllArgsConstructor
public class AtlasRelationsServiceImpl implements AtlasRelationsService {

    private AtlasRelationsRepository atlasRelationsRepository;

    @Override
    public void createTermRelation(UUID termId, CreateTermRelationDto createTermRelationDto) {
        AtlasRelationship atlasRelationship = new AtlasRelationship();
        atlasRelationship.setEnd1(new AtlasObjectId());
        atlasRelationship.setEnd2(new AtlasObjectId());

        if (createTermRelationDto.getRelationAttributes() != null) {
            atlasRelationship.setAttribute(ATTRIBUTE_EXPRESSION, createTermRelationDto.getRelationAttributes().getExpression());
            atlasRelationship.setAttribute(ATTRIBUTE_STEWARD, createTermRelationDto.getRelationAttributes().getSteward());
            atlasRelationship.setAttribute(ATTRIBUTE_SOURCE, createTermRelationDto.getRelationAttributes().getSource());
            atlasRelationship.setAttribute(ATTRIBUTE_DESCRIPTION, createTermRelationDto.getRelationAttributes().getDescription());
        }

        switch (createTermRelationDto.getRelationType()) {
            case SEEALSO:
                atlasRelationship.setTypeName(RELATED_TERM);
                atlasRelationship.getEnd1().setGuid(String.valueOf(termId));
                atlasRelationship.getEnd2().setGuid(String.valueOf(createTermRelationDto.getRelatedTermId()));
                break;
            case SYNONYM:
                atlasRelationship.setTypeName(SYNONYM_TERM);
                atlasRelationship.getEnd1().setGuid(String.valueOf(termId));
                atlasRelationship.getEnd2().setGuid(String.valueOf(createTermRelationDto.getRelatedTermId()));
                break;
            case ANTONYM:
                atlasRelationship.setTypeName(ANTONYM_TERM);
                atlasRelationship.getEnd1().setGuid(String.valueOf(termId));
                atlasRelationship.getEnd2().setGuid(String.valueOf(createTermRelationDto.getRelatedTermId()));
                break;
            case PREFERRED:
                atlasRelationship.setTypeName(PREFERRED_TERM);
                atlasRelationship.getEnd1().setGuid(String.valueOf(termId));
                atlasRelationship.getEnd2().setGuid(String.valueOf(createTermRelationDto.getRelatedTermId()));
                break;
            case PREFERREDTO:
                atlasRelationship.setTypeName(PREFERRED_TERM);
                atlasRelationship.getEnd1().setGuid(String.valueOf(createTermRelationDto.getRelatedTermId()));
                atlasRelationship.getEnd2().setGuid(String.valueOf(termId));
                break;
            case REPLACEDBY:
                atlasRelationship.setTypeName(REPLACEMENT_TERM);
                atlasRelationship.getEnd1().setGuid(String.valueOf(termId));
                atlasRelationship.getEnd2().setGuid(String.valueOf(createTermRelationDto.getRelatedTermId()));
                break;
            case REPLACEMENT:
                atlasRelationship.setTypeName(REPLACEMENT_TERM);
                atlasRelationship.getEnd1().setGuid(String.valueOf(createTermRelationDto.getRelatedTermId()));
                atlasRelationship.getEnd2().setGuid(String.valueOf(termId));
                break;
            case TRANSLATED:
                atlasRelationship.setTypeName(TRANSLATION_TERM);
                atlasRelationship.getEnd1().setGuid(String.valueOf(termId));
                atlasRelationship.getEnd2().setGuid(String.valueOf(createTermRelationDto.getRelatedTermId()));
                break;
            case TRANSLATION:
                atlasRelationship.setTypeName(TRANSLATION_TERM);
                atlasRelationship.getEnd1().setGuid(String.valueOf(createTermRelationDto.getRelatedTermId()));
                atlasRelationship.getEnd2().setGuid(String.valueOf(termId));
                break;
            case CLASSIFIES:
                atlasRelationship.setTypeName(IS_A_TERM);
                atlasRelationship.getEnd1().setGuid(String.valueOf(termId));
                atlasRelationship.getEnd2().setGuid(String.valueOf(createTermRelationDto.getRelatedTermId()));
                break;
            case ISA:
                atlasRelationship.setTypeName(IS_A_TERM);
                atlasRelationship.getEnd1().setGuid(String.valueOf(createTermRelationDto.getRelatedTermId()));
                atlasRelationship.getEnd2().setGuid(String.valueOf(termId));
                break;
            case VALIDVALUESFOR:
                atlasRelationship.setTypeName(VALID_VALUE_TERM);
                atlasRelationship.getEnd1().setGuid(String.valueOf(termId));
                atlasRelationship.getEnd2().setGuid(String.valueOf(createTermRelationDto.getRelatedTermId()));
                break;
            case VALIDVALUES:
                atlasRelationship.setTypeName(VALID_VALUE_TERM);
                atlasRelationship.getEnd1().setGuid(String.valueOf(createTermRelationDto.getRelatedTermId()));
                atlasRelationship.getEnd2().setGuid(String.valueOf(termId));
                break;
        }

        atlasRelationsRepository.createRelationship(atlasRelationship);
    }

    @Override
    public void delete(UUID relationId) {
        atlasRelationsRepository.deleteRelationship(String.valueOf(relationId));
    }

    @Override
    public void deleteTermRelation(UUID relationId) {
        AtlasRelationship.AtlasRelationshipWithExtInfo atlasRelationshipWithExtInfo = atlasRelationsRepository.get(String.valueOf(relationId));
        List<String> validTermRelations = List.of(RELATED_TERM,SYNONYM_TERM,ANTONYM_TERM,PREFERRED_TERM,
                REPLACEMENT_TERM,TRANSLATION_TERM,IS_A_TERM,VALID_VALUE_TERM);
        if (!validTermRelations.contains(atlasRelationshipWithExtInfo.getRelationship().getTypeName())) {
            throw new EntityTypeMissmatchException(String.format(DELETE_TERM_TERM_RELATION_TYPE_MISMATCH_EXCEPTION, relationId));
        }
        delete(relationId);
    }

}
