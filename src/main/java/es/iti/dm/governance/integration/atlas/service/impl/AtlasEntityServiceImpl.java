/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.service.impl;

import es.iti.dm.governance.integration.atlas.mapper.AtlasUtilMapper;
import es.iti.dm.governance.integration.atlas.repository.AtlasEntityRepository;
import es.iti.dm.governance.integration.atlas.service.AtlasEntityService;
import lombok.AllArgsConstructor;
import org.apache.atlas.model.instance.AtlasEntity;
import org.apache.atlas.model.instance.AtlasEntityHeader;
import org.apache.atlas.model.instance.EntityMutationResponse;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static es.iti.dm.governance.integration.atlas.utils.Constants.ATTRIBUTE_QUALIFIED_NAME;

@Service
@AllArgsConstructor
public class AtlasEntityServiceImpl implements AtlasEntityService {
    private AtlasEntityRepository atlasEntityRepository;

    @Override
    public AtlasEntity createEntity(AtlasEntity atlasEntity) {
        AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo = new AtlasEntity.AtlasEntityWithExtInfo(atlasEntity);
        return createAtlasEntity(atlasEntityWithExtInfo);
    }

    @Override
    public EntityMutationResponse updateEntity(AtlasEntity atlasEntity) {
        AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo = new AtlasEntity.AtlasEntityWithExtInfo(atlasEntity);
        return atlasEntityRepository.update(atlasEntityWithExtInfo);
    }

    @Override
    public AtlasEntity.AtlasEntityWithExtInfo getEntity(String entityId) {
        return atlasEntityRepository.getById(entityId);
    }

    @Override
    public AtlasEntity.AtlasEntityWithExtInfo getEntity(String entityId,boolean minExtInfo, boolean ignoreRelationships) {
        return atlasEntityRepository.getById(entityId, minExtInfo, ignoreRelationships);
    }

    @Override
    public void deleteEntities(List<String> guids) {
        atlasEntityRepository.deleteByIds(guids);
    }

    @Override
    public void deleteEntity(String entityId) {
        atlasEntityRepository.deleteById(entityId);
    }

    private AtlasEntity createAtlasEntity(AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo) {
        EntityMutationResponse entityMutationResponse = atlasEntityRepository.create(atlasEntityWithExtInfo);
        if (entityMutationResponse.getMutatedEntities() == null) {
            return atlasEntityRepository.getById(entityMutationResponse.getGuidAssignments().values().stream().findFirst().get()).getEntity();
        }
        AtlasEntityHeader atlasEntityHeader;
        if (entityMutationResponse.getCreatedEntities() != null) {
            atlasEntityHeader = entityMutationResponse.getCreatedEntities().get(0);
        } else {
            atlasEntityHeader = entityMutationResponse.getUpdatedEntities().get(0);
        }
        return AtlasUtilMapper.MAPPER.atlasEntityHeaderToAtlasEntity(atlasEntityHeader);
    }

    @Override
    public EntityMutationResponse createEntities(List<AtlasEntity> atlasEntities) {
        return  atlasEntityRepository.createBulk(new AtlasEntity.AtlasEntitiesWithExtInfo(atlasEntities));
    }

    @Override
    public AtlasEntity.AtlasEntityWithExtInfo getEntityByQualifiedName(String qualifiedName, String entityType) {
        Map<String, String> uniqueAttributes = new HashMap<>();
        uniqueAttributes.put(ATTRIBUTE_QUALIFIED_NAME, qualifiedName);
        return atlasEntityRepository.getByUniqueAttribute(uniqueAttributes, entityType);
    }
}
