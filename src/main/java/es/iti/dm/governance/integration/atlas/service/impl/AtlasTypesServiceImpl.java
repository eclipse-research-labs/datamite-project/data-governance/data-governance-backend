/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.service.impl;

import es.iti.dm.governance.dto.domain.CreateDomainDto;
import es.iti.dm.governance.dto.domain.DomainDto;
import es.iti.dm.governance.dto.domain.UpdateDomainDto;
import es.iti.dm.governance.integration.atlas.mapper.AtlasClassificationMapper;
import es.iti.dm.governance.integration.atlas.mapper.AtlasEntityMapper;
import es.iti.dm.governance.integration.atlas.repository.AtlasEntityRepository;
import es.iti.dm.governance.integration.atlas.repository.AtlasTypesRepository;
import es.iti.dm.governance.integration.atlas.service.AtlasDiscoveryService;
import es.iti.dm.governance.integration.atlas.service.AtlasTypesService;
import lombok.AllArgsConstructor;
import org.apache.atlas.model.SearchFilter;
import org.apache.atlas.model.instance.AtlasEntityHeader;
import org.apache.atlas.model.instance.ClassificationAssociateRequest;
import org.apache.atlas.model.typedef.AtlasClassificationDef;
import org.apache.atlas.model.typedef.AtlasTypesDef;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static es.iti.dm.governance.integration.atlas.utils.Constants.SEARCH_PARAMETER_CLASSIFICATION;
import static es.iti.dm.governance.util.EntitiesConstants.ATLAS_GLOSSARY_TERM;
import static es.iti.dm.governance.util.EntitiesConstants.COMMON_ENTITY_TYPE;

@Service
@AllArgsConstructor
public class AtlasTypesServiceImpl implements AtlasTypesService {

    private AtlasTypesRepository atlasTypesRepository;
    private AtlasEntityRepository atlasEntityRepository;
    private AtlasDiscoveryService atlasDiscoveryService;

    @Override
    public DomainDto createClassification(CreateDomainDto createDomainDto) {
        AtlasClassificationDef atlasClassificationDef = AtlasClassificationMapper.MAPPER.fromCreateClassificationToDef(createDomainDto);
        AtlasTypesDef atlasTypesDef = new AtlasTypesDef(null, null, Collections.singletonList(atlasClassificationDef), null);
        atlasTypesDef = atlasTypesRepository.createTypeDefs(atlasTypesDef);
        return AtlasClassificationMapper.MAPPER.toDomain(atlasTypesDef.getClassificationDefs().stream().findFirst().get());
    }

    @Override
    public DomainDto updateClassification(UpdateDomainDto updateDomainDto, String classificationName) {
        CreateDomainDto createDomainDto = AtlasClassificationMapper.MAPPER.fromUpdateToCreate(updateDomainDto);
        createDomainDto.setName(classificationName);
        AtlasClassificationDef atlasClassificationDef = AtlasClassificationMapper.MAPPER.fromCreateClassificationToDef(createDomainDto);
        AtlasTypesDef atlasTypesDef = new AtlasTypesDef(null, null, Collections.singletonList(atlasClassificationDef), null);
        atlasTypesDef = atlasTypesRepository.updateTypeDefs(atlasTypesDef);
        return AtlasClassificationMapper.MAPPER.toDomain(atlasTypesDef.getClassificationDefs().stream().findFirst().get());
    }

    @Override
    public void deleteClassification(String classificationName) {
        atlasTypesRepository.deleteTypeByName(classificationName);
    }

    @Override
    public DomainDto getClassification(String classificationName, int limit, int offset) {
        AtlasClassificationDef atlasClassificationDef = atlasTypesRepository.getClassificationByName(classificationName);
        DomainDto domainDto = AtlasClassificationMapper.MAPPER.toDomain(atlasClassificationDef);
        List<AtlasEntityHeader> atlasEntityHeaderList = atlasDiscoveryService.getEntitiesAssignedToClassification(classificationName, COMMON_ENTITY_TYPE, limit, offset);
        List<AtlasEntityHeader> atlasTermEntityHeaderList = atlasDiscoveryService.getEntitiesAssignedToClassification(classificationName, ATLAS_GLOSSARY_TERM, limit, offset);
        domainDto.setAssignations(AtlasEntityMapper.MAPPER.fromAtlasEntityHeaderToList(atlasEntityHeaderList));
        domainDto.setTermAssignations(AtlasEntityMapper.MAPPER.fromAtlasEntityHeaderToList(atlasTermEntityHeaderList));
        return domainDto;
    }

    @Override
    public List<DomainDto> getAllClassifications() {
        SearchFilter searchFilter = new SearchFilter();
        searchFilter.setParam(SearchFilter.PARAM_TYPE, SEARCH_PARAMETER_CLASSIFICATION);
        List<AtlasClassificationDef> atlasClassificationDefList = atlasTypesRepository.getAllTypeDefs(searchFilter).getClassificationDefs();
        return AtlasClassificationMapper.MAPPER.toClassifications(atlasClassificationDefList);
    }

    @Override
    public void addClassification(String classificationName, UUID entityId, Boolean propagate) {
        ClassificationAssociateRequest classificationAssociateRequest = AtlasClassificationMapper.MAPPER.toClassificationAssociateRequest(classificationName, String.valueOf(entityId), propagate);
        atlasEntityRepository.addClassification(classificationAssociateRequest);
    }

    @Override
    public void removeClassification(String classificationName, UUID entityId) {
        atlasEntityRepository.deleteClassification(String.valueOf(entityId), classificationName);
    }
}
