/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.service.impl;

import es.iti.dm.governance.dto.search.SearchDto;
import es.iti.dm.governance.dto.search.SearchResultDto;
import es.iti.dm.governance.dto.search.TermSearchResultDto;
import es.iti.dm.governance.integration.atlas.mapper.AtlasEntityMapper;
import es.iti.dm.governance.integration.atlas.mapper.AtlasGlossaryTermMapper;
import es.iti.dm.governance.integration.atlas.repository.AtlasDiscoveryRepository;
import es.iti.dm.governance.integration.atlas.service.AtlasDiscoveryService;
import lombok.AllArgsConstructor;
import org.apache.atlas.model.discovery.AtlasSearchResult;
import org.apache.atlas.model.discovery.SearchParameters;
import org.apache.atlas.model.instance.AtlasEntityHeader;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static es.iti.dm.governance.integration.atlas.utils.Constants.*;
import static es.iti.dm.governance.util.EntitiesConstants.*;

@Service
@AllArgsConstructor
public class AtlasDiscoveryServiceImpl implements AtlasDiscoveryService {

    private AtlasDiscoveryRepository atlasDiscoveryRepository;

    private static final String LIKE_KEYWORD = " like ";
    private static final String WHERE_KEYWORD = " where ";
    private static final String SELECT_KEYWORD = " select ";
    private static final String HAS_TERM_KEYWORD = " hasTerm ";
    private static final String ORDER_BY_KEYWORD = " orderby ";
    private static final String EQUAL_KEYWORD = " = ";
    private static final String OR_KEYWORD = " or ";
    private static final String AND_KEYWORD = " and ";

    @Override
    public AtlasSearchResult getDistributionSizeByQualifiedName(String qualifiedName) {
        String query = DISTRIBUTION_ENTITY.concat(WHERE_KEYWORD)
                .concat(ATTRIBUTE_QUALIFIED_NAME).concat(EQUAL_KEYWORD).concat("\"").concat(qualifiedName).concat("\"")
                .concat(AND_KEYWORD).concat(ATTRIBUTE_STATE_INTERNAL).concat(EQUAL_KEYWORD).concat("\"").concat(STATE_VALUE_ACTIVE).concat("\"")
                .concat(SELECT_KEYWORD).concat(BYTE_SIZE_ATTRIBUTE);
        return atlasDiscoveryRepository.findByQuery(query);
    }

    @Override
    public List<SearchResultDto> search(SearchDto searchDto) {
        SearchParameters searchParameters = defaultSearchParameters();
        searchParameters.setQuery(searchDto.getText());
        searchParameters.setLimit(searchDto.getLimit());
        searchParameters.setOffset(searchDto.getOffset());
        searchParameters.setTypeName(searchDto.getType().getValue());
        searchParameters.setClassification(searchDto.getClassification());
        searchParameters.setTermName(searchDto.getTerm());

        SearchParameters.FilterCriteria filterCriteria = new SearchParameters.FilterCriteria();
        filterCriteria.setCondition(SearchParameters.FilterCriteria.Condition.AND);
        filterCriteria.setCriterion(new ArrayList<>());

        if (searchDto.getName() != null) {
            SearchParameters.FilterCriteria nameFilter = new SearchParameters.FilterCriteria();
            nameFilter.setAttributeName(ENTITY_NAME_ATTRIBUTE);
            nameFilter.setOperator(SearchParameters.Operator.fromString(searchDto.getName().getOperator().getValue()));
            nameFilter.setAttributeValue(searchDto.getName().getText());
            filterCriteria.getCriterion().add(nameFilter);
        }

        if (searchDto.getDescription() != null) {
            SearchParameters.FilterCriteria descriptionFilter = new SearchParameters.FilterCriteria();
            descriptionFilter.setAttributeName(ENTITY_DESCRIPTION_ATTRIBUTE);
            descriptionFilter.setOperator(SearchParameters.Operator.fromString(searchDto.getDescription().getOperator().getValue()));
            descriptionFilter.setAttributeValue(searchDto.getDescription().getText());
            filterCriteria.getCriterion().add(descriptionFilter);
        }

        if (searchDto.getSummary() != null) {
            SearchParameters.FilterCriteria summaryFilter = new SearchParameters.FilterCriteria();
            summaryFilter.setAttributeName(ENTITY_SUMMARY_ATTRIBUTE);
            summaryFilter.setOperator(SearchParameters.Operator.fromString(searchDto.getSummary().getOperator().getValue()));
            summaryFilter.setAttributeValue(searchDto.getSummary().getText());
            filterCriteria.getCriterion().add(summaryFilter);
        }

        if (!filterCriteria.getCriterion().isEmpty()) {
            searchParameters.setEntityFilters(filterCriteria);
        }

        Set attributes = new HashSet<>();
        attributes.add(ENTITY_DESCRIPTION_ATTRIBUTE);
        attributes.add(ENTITY_SUMMARY_ATTRIBUTE);
        searchParameters.setAttributes(attributes);

        AtlasSearchResult atlasSearchResult = atlasDiscoveryRepository.findByParameters(searchParameters);

        return AtlasEntityMapper.MAPPER.fromAtlasEntityHeaderToSearchResultList(atlasSearchResult.getEntities());
    }

    @Override
    public List<TermSearchResultDto> getTermsByText(String text) {
        AtlasSearchResult atlasSearchResult = atlasDiscoveryRepository.findByAttribute(ATLAS_GLOSSARY_TERM, null, text, 10, 0);
        if (atlasSearchResult.getEntities() == null) {
            return new ArrayList<>();
        }
        return AtlasGlossaryTermMapper.MAPPER.fromAtlasEntityHeaderList(atlasSearchResult.getEntities());
    }

    @Override
    public List<AtlasEntityHeader> getGlossaries(int limit, int offset) {
        SearchParameters searchParameters = defaultSearchParameters();
        searchParameters.setTypeName(ATLAS_GLOSSARY);
        searchParameters.setLimit(limit);
        searchParameters.setOffset(offset);

        Set attributes = new HashSet<>();
        attributes.add(ATTRIBUTE_SHORT_DESCRIPTION);
        attributes.add(ATTRIBUTE_LONG_DESCRIPTION);
        attributes.add(ATTRIBUTE_USAGE);
        attributes.add(ATTRIBUTE_LANGUAGE);
        attributes.add(TERMS_RELATION_ATTRIBUTE);
        attributes.add(ATTRIBUTE_CREATED_AT);
        attributes.add(ATTRIBUTE_UPDATED_AT);
        searchParameters.setAttributes(attributes);

        AtlasSearchResult atlasSearchResult = atlasDiscoveryRepository.findByParameters(searchParameters);
        return atlasSearchResult.getEntities();
    }

    @Override
    public String findCatalogIdFromDatasetQualifiedName(String qualifiedName) {
        String query = DATASET_ENTITY_TYPE + WHERE_KEYWORD + ATTRIBUTE_QUALIFIED_NAME + " = \"" + qualifiedName + "\"" + SELECT_KEYWORD + CATALOG_RELATION_ATTRIBUTE;
        AtlasSearchResult atlasSearchResult = atlasDiscoveryRepository.findByQuery(query);
        if (atlasSearchResult.getEntities() != null) {
            return atlasSearchResult.getEntities().get(0).getGuid();
        }
        return null;
    }

    @Override
    public List<AtlasEntityHeader> getEntitiesAssignedToClassification(String domainName, String typeName, int limit, int offset) {
        SearchParameters searchParameters = defaultSearchParameters();
        if (typeName != null) {
            searchParameters.setTypeName(typeName);
        }
        searchParameters.setClassification(domainName);
        searchParameters.setLimit(limit);
        searchParameters.setOffset(offset);
        return atlasDiscoveryRepository.findByParameters(searchParameters).getEntities();
    }

    @Override
    public List<AtlasEntityHeader> getEntitiesAssignedToTerm(String termQualifiedName, int limit, int offset) {
        SearchParameters searchParameters = defaultSearchParameters();
        searchParameters.setTermName(termQualifiedName);
        searchParameters.setLimit(limit);
        searchParameters.setOffset(offset);
        return atlasDiscoveryRepository.findByParameters(searchParameters).getEntities();
    }

    private SearchParameters defaultSearchParameters() {
        SearchParameters searchParameters = new SearchParameters();
        searchParameters.setExcludeDeletedEntities(true);
        searchParameters.setIncludeSubClassifications(true);
        searchParameters.setIncludeSubTypes(true);
        searchParameters.setIncludeClassificationAttributes(true);
        searchParameters.setLimit(1);
        searchParameters.setOffset(0);
        return searchParameters;
    }
}
