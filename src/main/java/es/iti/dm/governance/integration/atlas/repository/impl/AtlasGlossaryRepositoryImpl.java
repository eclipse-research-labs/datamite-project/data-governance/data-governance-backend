/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.repository.impl;

import es.iti.dm.governance.exception.DeleteResourceInUseException;
import es.iti.dm.governance.exception.MetadataStorageException;
import es.iti.dm.governance.integration.atlas.repository.AtlasGlossaryRepository;
import org.apache.atlas.AtlasClientV2;
import org.apache.atlas.AtlasServiceException;
import org.apache.atlas.model.glossary.AtlasGlossary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static es.iti.dm.governance.util.ErrorMessages.DELETE_GLOSSARY_IN_USE_EXCEPTION;
import static es.iti.dm.governance.util.ErrorMessages.DELETE_GLOSSARY_TERM_IN_USE_EXCEPTION;

@Service
public class AtlasGlossaryRepositoryImpl implements AtlasGlossaryRepository {
    @Autowired
    private AtlasClientV2 atlasClientV2;

    @Override
    public AtlasGlossary create(AtlasGlossary atlasGlossary) {
        try {
            return atlasClientV2.createGlossary(atlasGlossary);
        } catch (AtlasServiceException e) {
            throw new MetadataStorageException(e);
        }
    }

    @Override
    public void deleteById(String id) {
        try {
            atlasClientV2.deleteGlossaryByGuid(id);
        } catch (AtlasServiceException e) {
            //ATLAS-400-00-084 Term (guid={0}) cannot be deleted as it has been assigned to {1} entities.
            if (e.getMessage().contains("ATLAS-400-00-084")) {
                Pattern pattern = Pattern.compile("[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[89abAB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}");
                Matcher matcher = pattern.matcher(e.getMessage());
                if (matcher.find()) {
                    throw new DeleteResourceInUseException(String.format(DELETE_GLOSSARY_TERM_IN_USE_EXCEPTION, id, matcher.group()));
                }
                else {
                    throw new DeleteResourceInUseException(String.format(DELETE_GLOSSARY_IN_USE_EXCEPTION, id));
                }
            } else {
                throw new MetadataStorageException(e);
            }
        }
    }

    @Override
    public AtlasGlossary update(AtlasGlossary atlasGlossary) {
        try {
            return atlasClientV2.updateGlossaryByGuid(atlasGlossary.getGuid(), atlasGlossary);
        } catch (AtlasServiceException e) {
            throw new MetadataStorageException(e);
        }
    }

    @Override
    public AtlasGlossary getById(String id){
        try {
            return atlasClientV2.getGlossaryByGuid(id);
        } catch (AtlasServiceException e) {
            throw new MetadataStorageException(e);
        }
    }

    @Override
    public List<AtlasGlossary> getAll(String orderBy, int limit, int offset) {
        try {
            return atlasClientV2.getAllGlossaries(orderBy, limit, offset);
        } catch (AtlasServiceException e) {
            throw new MetadataStorageException(e);
        }
    }

}
