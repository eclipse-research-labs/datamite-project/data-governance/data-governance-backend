/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.service.impl;

import es.iti.dm.governance.dto.abstractmodel.CreateOrUpdateEntityDto;
import es.iti.dm.governance.dto.table.CreateTableEntityDto;
import es.iti.dm.governance.dto.table.TableEntityDto;
import es.iti.dm.governance.dto.table.TableSimplDto;
import es.iti.dm.governance.integration.atlas.mapper.AtlasEntityMapper;
import es.iti.dm.governance.integration.atlas.mapper.AtlasEntitySimplMapper;
import es.iti.dm.governance.integration.atlas.service.AtlasEntityService;
import es.iti.dm.governance.integration.atlas.service.AtlasTableEntityService;
import lombok.AllArgsConstructor;
import org.apache.atlas.model.instance.AtlasEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import static es.iti.dm.governance.integration.atlas.mapper.AtlasEntityMapper.PARENT_BASE_GUID;
import static es.iti.dm.governance.integration.atlas.utils.Constants.ATTRIBUTE_QUALIFIED_NAME;
import static es.iti.dm.governance.util.EntitiesConstants.TABLE_ENTITY_TYPE;
import static es.iti.dm.governance.util.ErrorMessages.ENTITY_TYPE_MISMATCH_ERROR;

@Service
@AllArgsConstructor
public class AtlasTableEntityServiceImpl implements AtlasTableEntityService {

    private AtlasEntityService atlasEntityService;

    @Override
    public TableEntityDto get(UUID tableId) {
        AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo = atlasEntityService.getEntity(String.valueOf(tableId), true, false);
        checkTableType(atlasEntityWithExtInfo.getEntity().getTypeName(), tableId);
        return AtlasEntityMapper.MAPPER.toTableEntity(atlasEntityWithExtInfo.getEntity());
    }

    @Override
    public void delete(UUID tableId) {
        atlasEntityService.deleteEntity(String.valueOf(tableId));
    }

    @Override
    public TableEntityDto update(CreateOrUpdateEntityDto createOrUpdateEntityDto, UUID tableId) {
        AtlasEntity atlasEntity = atlasEntityService.getEntity(String.valueOf(tableId), true, false).getEntity();
        checkTableType(atlasEntity.getTypeName(), tableId);
        atlasEntity = AtlasEntityMapper.MAPPER.updateEntity(atlasEntity, createOrUpdateEntityDto);
        atlasEntityService.updateEntity(atlasEntity);
        return get(tableId);
    }

    @Override
    public void addTables(UUID databaseId, List<CreateTableEntityDto> createTableEntityDtoList) {
        AtlasEntity.AtlasEntityWithExtInfo databaseEntity = atlasEntityService.getEntity(String.valueOf(databaseId), false, true);
        String databaseQualifiedName = (String) databaseEntity.getEntity().getAttribute(ATTRIBUTE_QUALIFIED_NAME);
        AtomicInteger idGenerator = new AtomicInteger(Integer.valueOf(PARENT_BASE_GUID));
        List<AtlasEntity> atlasEntityList = AtlasEntityMapper.MAPPER.fromCreateTablesToTablesEntities(createTableEntityDtoList, databaseQualifiedName, String.valueOf(databaseId), idGenerator);
        atlasEntityService.createEntities(atlasEntityList);
    }

    @Override
    public List<TableSimplDto> getTables(UUID databaseId) {
        AtlasEntity.AtlasEntityWithExtInfo databaseEntity = atlasEntityService.getEntity(String.valueOf(databaseId), true, false);
        return AtlasEntitySimplMapper.MAPPER.getTablesSimplFromExtInfo(databaseEntity);
    }

    private void checkTableType(String type, UUID id) {
        if (!TABLE_ENTITY_TYPE.equals(type)) {
            throw new EntityTypeMissmatchException(String.format(ENTITY_TYPE_MISMATCH_ERROR, id, TABLE_ENTITY_TYPE));
        }
    }
}
