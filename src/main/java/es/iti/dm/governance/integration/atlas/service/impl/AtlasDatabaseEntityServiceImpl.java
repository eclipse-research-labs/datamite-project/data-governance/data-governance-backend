/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.iti.dm.governance.dto.abstractmodel.CreateOrUpdateEntityDto;
import es.iti.dm.governance.dto.database.CreateDatabaseEntityDto;
import es.iti.dm.governance.dto.database.DatabaseEntityDto;
import es.iti.dm.governance.dto.distribution.DistributionDto;
import es.iti.dm.governance.exception.DatabaseExistsOtherCatalogException;
import es.iti.dm.governance.integration.atlas.mapper.AtlasEntityMapper;
import es.iti.dm.governance.integration.atlas.service.*;
import lombok.AllArgsConstructor;
import org.apache.atlas.model.instance.AtlasEntity;
import org.apache.atlas.model.instance.AtlasObjectId;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static es.iti.dm.governance.util.EntitiesConstants.*;
import static es.iti.dm.governance.util.ErrorMessages.ENTITY_TYPE_MISMATCH_ERROR;

@Service
@AllArgsConstructor
public class AtlasDatabaseEntityServiceImpl implements AtlasDatabaseEntityService {

    private AtlasEntityService atlasEntityService;
    private AtlasDistributionEntityService atlasDistributionEntityService;
    private AtlasCatalogEntityService atlasCatalogEntityService;
    private AtlasDiscoveryService atlasDiscoveryService;

    @Override
    public DatabaseEntityDto create(CreateDatabaseEntityDto createDatabaseEntityDto) {

        String qualifiedName = createDatabaseEntityDto.getDistributions().get(0).getAccessUrl();

        String relatedCatalogId = atlasDiscoveryService.findCatalogIdFromDatasetQualifiedName(qualifiedName);
        if (relatedCatalogId != null && !relatedCatalogId.equals(String.valueOf(createDatabaseEntityDto.getCatalogId()))) {
            throw new DatabaseExistsOtherCatalogException("Database already exists in catalog " + relatedCatalogId);
        }

        List<AtlasEntity> atlasEntityList = AtlasEntityMapper.MAPPER.fromCreateOrUpdateDatabaseEntityList(createDatabaseEntityDto, qualifiedName);

        AtlasEntity newDistributionEntity = atlasEntityList.stream().filter(atlasEntity -> atlasEntity.getTypeName().equals(DISTRIBUTION_ENTITY)).findFirst().get();

        atlasEntityList.add(atlasCatalogEntityService.getUpdatedCatalogSize(createDatabaseEntityDto.getCatalogId(), newDistributionEntity));

        atlasEntityService.createEntities(atlasEntityList);
        AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo = atlasEntityService.getEntityByQualifiedName(qualifiedName, DATABASE_ENTITY_TYPE);
        DatabaseEntityDto databaseEntityDto = AtlasEntityMapper.MAPPER.toDatabaseEntity(atlasEntityWithExtInfo.getEntity());
        databaseEntityDto.setDistributions(atlasDistributionEntityService.getDistributionsByEntityWithExtInfo(atlasEntityWithExtInfo));
        return databaseEntityDto;
    }

    @Override
    public DatabaseEntityDto get(UUID databaseId) {
        AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo = atlasEntityService.getEntity(String.valueOf(databaseId), true, false);
        checkDatabaseType(atlasEntityWithExtInfo.getEntity().getTypeName(), databaseId);
        DatabaseEntityDto databaseEntityDto = AtlasEntityMapper.MAPPER.toDatabaseEntity(atlasEntityWithExtInfo.getEntity());
        databaseEntityDto.setDistributions(atlasDistributionEntityService.getDistributionsByEntityWithExtInfo(atlasEntityWithExtInfo));
        return databaseEntityDto;
    }

    @Override
    public DatabaseEntityDto update(CreateOrUpdateEntityDto createOrUpdateEntityDto, UUID databaseId) {
        AtlasEntity atlasEntity = atlasEntityService.getEntity(String.valueOf(databaseId)).getEntity();
        checkDatabaseType(atlasEntity.getTypeName(), databaseId);
        atlasEntity = AtlasEntityMapper.MAPPER.updateEntity(atlasEntity, createOrUpdateEntityDto);
        atlasEntityService.updateEntity(atlasEntity);
        return get(databaseId);
    }

    @Override
    public void delete(UUID databaseId) {
        AtlasEntity.AtlasEntityWithExtInfo databaseEntity = atlasEntityService.getEntity(String.valueOf(databaseId));
        checkDatabaseType(databaseEntity.getEntity().getTypeName(), databaseId);
        AtlasObjectId atlasObjectId = new ObjectMapper().convertValue(databaseEntity.getEntity().getAttribute(CATALOG_RELATION_ATTRIBUTE), new TypeReference<AtlasObjectId>() {});
        AtlasEntity.AtlasEntityWithExtInfo catalogEntity = atlasEntityService.getEntity(atlasObjectId.getGuid());
        Long catalogSize = Long.valueOf((Integer) catalogEntity.getEntity().getAttribute(BYTE_SIZE_ATTRIBUTE));
        catalogEntity.getEntity().setAttribute(BYTE_SIZE_ATTRIBUTE, catalogSize - atlasDistributionEntityService.getDistributionsByEntityWithExtInfo(databaseEntity).stream().collect(Collectors.summingLong(DistributionDto::getByteSize)));
        atlasEntityService.updateEntity(catalogEntity.getEntity());
        atlasEntityService.deleteEntity(String.valueOf(databaseId));
    }

    private void checkDatabaseType(String type, UUID id) {
        if (!DATABASE_ENTITY_TYPE.equals(type)) {
            throw new EntityTypeMissmatchException(String.format(ENTITY_TYPE_MISMATCH_ERROR, id, DATABASE_ENTITY_TYPE));
        }
    }
}
