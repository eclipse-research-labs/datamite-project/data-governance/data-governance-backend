/*
 * MIT License
 * Copyright (c) 2024-2025.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.config;

import org.apache.atlas.ApplicationProperties;
import org.apache.atlas.AtlasClientV2;
import org.apache.atlas.AtlasException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static es.iti.dm.governance.integration.atlas.utils.Constants.ATLAS_REST_ADDRESS;

@Configuration
public class AtlasClientV2Config {
    @Value("${ATLAS_HOST}")
    private String atlasHost;

    @Value("${ATLAS_PORT}")
    private String atlasPort;

    @Value("${ATLAS_USER}")
    private String atlasUser;

    @Value("${ATLAS_PASSWORD}")
    private String atlasPassword;

    @Bean
    public AtlasClientV2 atlasClientV2() throws AtlasException {
        ApplicationProperties.get().setProperty(ATLAS_REST_ADDRESS,atlasHost+":"+atlasPort);
        org.apache.commons.configuration.Configuration configuration = ApplicationProperties.get();
        return new AtlasClientV2(configuration.getStringArray(ATLAS_REST_ADDRESS), new String[]{atlasUser, atlasPassword});
    }

}
