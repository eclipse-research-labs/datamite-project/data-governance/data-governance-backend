/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.service.impl;

import es.iti.dm.governance.dto.glossary.term.CreateOrUpdateGlossaryTermEntityDto;
import es.iti.dm.governance.dto.glossary.term.DisassociateTermDto;
import es.iti.dm.governance.dto.glossary.term.GlossaryTermEntityDto;
import es.iti.dm.governance.integration.atlas.mapper.AtlasEntityMapper;
import es.iti.dm.governance.integration.atlas.mapper.AtlasGlossaryTermMapper;
import es.iti.dm.governance.integration.atlas.repository.AtlasGlossaryTermRepository;
import es.iti.dm.governance.integration.atlas.service.AtlasDiscoveryService;
import es.iti.dm.governance.integration.atlas.service.AtlasEntityService;
import es.iti.dm.governance.integration.atlas.service.AtlasGlossaryTermService;
import lombok.AllArgsConstructor;
import org.apache.atlas.model.glossary.AtlasGlossaryTerm;
import org.apache.atlas.model.instance.AtlasEntity;
import org.apache.atlas.model.instance.AtlasEntityHeader;
import org.apache.atlas.model.instance.AtlasRelatedObjectId;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AtlasGlossaryTermServiceImpl implements AtlasGlossaryTermService {

    private AtlasGlossaryTermRepository atlasGlossaryTermRepository;
    private AtlasEntityService atlasEntityService;
    private AtlasDiscoveryService atlasDiscoveryService;

    @Override
    public GlossaryTermEntityDto create(CreateOrUpdateGlossaryTermEntityDto createOrUpdateGlossaryTermEntityDto) {
        AtlasGlossaryTerm atlasGlossaryTerm = AtlasGlossaryTermMapper.MAPPER.fromCreateGlossaryEntity(createOrUpdateGlossaryTermEntityDto);
        atlasGlossaryTerm = atlasGlossaryTermRepository.create(atlasGlossaryTerm);
        return AtlasGlossaryTermMapper.MAPPER.toGlossaryEntity(atlasGlossaryTerm);
    }

    @Override
    public GlossaryTermEntityDto get(UUID glossaryTermId, int limit, int offset) {
        AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo = atlasEntityService.getEntity(String.valueOf(glossaryTermId));
        GlossaryTermEntityDto glossaryTermEntityDto = AtlasGlossaryTermMapper.MAPPER.fromAtlasEntity(atlasEntityWithExtInfo.getEntity());
        AtlasGlossaryTerm atlasGlossaryTerm = atlasGlossaryTermRepository.getById(String.valueOf(glossaryTermId));
        List<AtlasEntityHeader> atlasEntityHeaderList = atlasDiscoveryService.getEntitiesAssignedToTerm(atlasGlossaryTerm.getQualifiedName(), limit, offset);
        glossaryTermEntityDto.setAssignations(AtlasEntityMapper.MAPPER.fromAtlasEntityHeaderToList(atlasEntityHeaderList));
        return glossaryTermEntityDto;
    }

    @Override
    public GlossaryTermEntityDto update(CreateOrUpdateGlossaryTermEntityDto createOrUpdateGlossaryTermEntityDto, UUID glossaryTermId) {
        AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo = atlasEntityService.getEntity(String.valueOf(glossaryTermId));
        AtlasGlossaryTermMapper.MAPPER.updateGlossaryTermEntityAttributes(createOrUpdateGlossaryTermEntityDto, atlasEntityWithExtInfo.getEntity().getAttributes());
        atlasEntityService.updateEntity(atlasEntityWithExtInfo.getEntity());
        //TODO transform atlasEntity from update/create instead use get
        return AtlasGlossaryTermMapper.MAPPER.toGlossaryEntity(atlasGlossaryTermRepository.getById(String.valueOf(glossaryTermId)));
    }

    @Override
    public void delete(UUID glossaryTermId) {
        atlasGlossaryTermRepository.deleteById(String.valueOf(glossaryTermId));
    }

    @Override
    public void assign(UUID termId, List<UUID> entities) {
        List<AtlasRelatedObjectId> atlasRelatedObjectIdList = new ArrayList<>();
        entities.stream()
                .forEach(uuid -> {
                    AtlasRelatedObjectId atlasRelatedObjectId = new AtlasRelatedObjectId();
                    atlasRelatedObjectId.setGuid(String.valueOf(uuid));
                    atlasRelatedObjectIdList.add(atlasRelatedObjectId);
                });
        atlasGlossaryTermRepository.assignToEntities(String.valueOf(termId), atlasRelatedObjectIdList);
    }

    @Override
    public void disassociate(UUID termId, List<DisassociateTermDto> entities) {
        atlasGlossaryTermRepository.disassociateFromEntities(String.valueOf(termId), AtlasEntityMapper.MAPPER.disassociateTermListToRelatedObjectIdList(entities));
    }
}
