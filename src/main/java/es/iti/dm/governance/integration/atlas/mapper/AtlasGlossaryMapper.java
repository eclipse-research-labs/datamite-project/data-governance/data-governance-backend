/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.mapper;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.iti.dm.governance.dto.abstractmodel.EntityHeaderDto;
import es.iti.dm.governance.dto.abstractmodel.EntityRelationHeaderDto;
import es.iti.dm.governance.dto.glossary.CreateOrUpdateGlossaryEntityDto;
import es.iti.dm.governance.dto.glossary.GlossaryEntityDto;
import org.apache.atlas.model.glossary.AtlasGlossary;
import org.apache.atlas.model.instance.AtlasEntity;
import org.apache.atlas.model.instance.AtlasEntityHeader;
import org.apache.atlas.model.instance.AtlasObjectId;
import org.apache.atlas.model.instance.AtlasRelatedObjectId;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.*;

import static es.iti.dm.governance.integration.atlas.utils.Constants.ATTRIBUTE_QUALIFIED_NAME;
import static es.iti.dm.governance.util.EntitiesConstants.ATLAS_GLOSSARY_TERM;
import static es.iti.dm.governance.util.EntitiesConstants.TERMS_RELATION_ATTRIBUTE;

@Mapper(uses = {AtlasGlossaryUtilMapper.class, AtlasEntityAttributesUtil.class, AtlasEntityMapper.class})
public interface AtlasGlossaryMapper {
    AtlasGlossaryMapper MAPPER = Mappers.getMapper(AtlasGlossaryMapper.class);

    @Mapping(target = "shortDescription", source = "summary")
    @Mapping(target = "longDescription", source = "description")
    //TODO use name in qualifiedName or other?
    @Mapping(target = "qualifiedName", source = "name")
    AtlasGlossary fromCreateGlossaryEntity(CreateOrUpdateGlossaryEntityDto createOrUpdateGlossaryEntityDto);

    @Mapping(target = "id", source = "guid")
    @Mapping(target = "summary", source = "shortDescription")
    @Mapping(target = "description", source = "longDescription")
    GlossaryEntityDto toGlossaryEntity(AtlasGlossary atlasGlossary);

    default List<GlossaryEntityDto> toGlossaryEntityList(List<AtlasGlossary> atlasGlossaryList) {
        if (atlasGlossaryList == null) {
            return Collections.emptyList();
        }
        List<GlossaryEntityDto> list = new ArrayList<>(atlasGlossaryList.size());
        for (AtlasGlossary atlasGlossary : new ObjectMapper().convertValue(atlasGlossaryList, new TypeReference<List<AtlasGlossary>>() {
        })) {
            list.add(toGlossaryEntity(atlasGlossary));
        }
        return list;
    }

    @Mapping(target = "createdAt", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeCreatedAt.class)
    @Mapping(target = "updatedAt", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeUpdatedAt.class)
    @Mapping(target = "terms", source = "attributes", qualifiedByName = "getTermsFromAttributes")
    @AtlasEntityToGlossaryCommon
    GlossaryEntityDto fromAtlasEntityHeader(AtlasEntityHeader atlasEntityHeader);
    List<GlossaryEntityDto> fromAtlasEntityHeaderList(List<AtlasEntityHeader> atlasEntityHeaderList);

    @Mapping(target = "createdAt", source = "createTime")
    @Mapping(target = "updatedAt", source = "updateTime")
    @Mapping(target = "terms", source = "relationshipAttributes", qualifiedByName = "getTermsFromRelationAttributes")
    @AtlasEntityToGlossaryCommon
    GlossaryEntityDto fromAtlasEntity(AtlasEntity atlasEntity);

    @Named("getTermsFromAttributes")
    default List<EntityHeaderDto> getTermsFromAttributes(Map<String, Object> attributes) {
        List<EntityHeaderDto> terms = new ArrayList<>();
        if (attributes.get(TERMS_RELATION_ATTRIBUTE) != null) {
            List<AtlasObjectId> atlasObjectIds = new ObjectMapper().convertValue(attributes.get(TERMS_RELATION_ATTRIBUTE), new TypeReference<List<AtlasObjectId>>() {
            });
            atlasObjectIds.stream().forEach( atlasObjectId -> {
                        String qualifiedName = (String) atlasObjectId.getUniqueAttributes().get(ATTRIBUTE_QUALIFIED_NAME);
                        terms.add(
                                EntityHeaderDto.builder()
                                        .id(UUID.fromString(atlasObjectId.getGuid()))
                                        .name(qualifiedName.split("@")[0])
                                        .type(ATLAS_GLOSSARY_TERM)
                                        .qualifiedName(qualifiedName)
                                        .build());
                    }
            );
        }
        return terms;
    }

    @Named("getTermsFromRelationAttributes")
    default List<EntityHeaderDto> getTermsFromRelationAttributes(Map<String, Object> attributes) {
        List<EntityHeaderDto> terms = new ArrayList<>();
        if (attributes.get(TERMS_RELATION_ATTRIBUTE) != null) {
            List<AtlasRelatedObjectId> atlasObjectIds = new ObjectMapper().convertValue(attributes.get(TERMS_RELATION_ATTRIBUTE), new TypeReference<List<AtlasRelatedObjectId>>() {
            });
            atlasObjectIds.stream().forEach( atlasObjectId -> {
                        terms.add(
                                EntityRelationHeaderDto.builder()
                                        .id(UUID.fromString(atlasObjectId.getGuid()))
                                        .name(atlasObjectId.getDisplayText())
                                        .type(ATLAS_GLOSSARY_TERM)
                                        .qualifiedName(atlasObjectId.getQualifiedName())
                                        .relationId(UUID.fromString(atlasObjectId.getRelationshipGuid()))
                                        .build());
                    }
            );
        }
        return terms;
    }

    @Mapping(target = "id", source = "guid")
    @Mapping(target = "name", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeEntityName.class)
    @Mapping(target = "description", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeLongDescription.class)
    @Mapping(target = "summary", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeShortDescription.class)
    @Mapping(target = "usage", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeUsage.class)
    @Mapping(target = "language", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeLanguage.class)
    @Mapping(target = "domains", source = "classifications")
    public @interface AtlasEntityToGlossaryCommon {}
}
