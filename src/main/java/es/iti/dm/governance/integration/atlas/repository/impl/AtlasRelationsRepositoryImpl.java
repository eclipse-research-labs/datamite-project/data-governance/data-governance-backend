/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.repository.impl;

import es.iti.dm.governance.exception.MetadataStorageException;
import es.iti.dm.governance.integration.atlas.repository.AtlasRelationsRepository;
import lombok.AllArgsConstructor;
import org.apache.atlas.AtlasClientV2;
import org.apache.atlas.AtlasServiceException;
import org.apache.atlas.model.instance.AtlasRelationship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AtlasRelationsRepositoryImpl implements AtlasRelationsRepository {

    @Autowired
    private AtlasClientV2 atlasClientV2;

    @Override
    public void createRelationship(AtlasRelationship atlasRelationship) {
        try {
            atlasClientV2.createRelationship(atlasRelationship);
        } catch (AtlasServiceException e) {
            throw new MetadataStorageException(e);
        }
    }

    @Override
    public void deleteRelationship(String guid) {
        try {
            atlasClientV2.deleteRelationshipByGuid(guid);
        } catch (AtlasServiceException e) {
            throw new MetadataStorageException(e);
        }
    }

    @Override
    public AtlasRelationship.AtlasRelationshipWithExtInfo get(String guid) {
        try {
           return atlasClientV2.getRelationshipByGuid(guid);
        } catch (AtlasServiceException e) {
            throw new MetadataStorageException(e);
        }
    }
}
