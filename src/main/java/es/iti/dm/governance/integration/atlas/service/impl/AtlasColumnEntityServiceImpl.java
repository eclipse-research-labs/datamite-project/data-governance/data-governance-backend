/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.iti.dm.governance.dto.abstractmodel.CreateOrUpdateEntityDto;
import es.iti.dm.governance.dto.column.ColumnEntityDto;
import es.iti.dm.governance.dto.column.ColumnSimplDto;
import es.iti.dm.governance.dto.column.CreateColumnEntityDto;
import es.iti.dm.governance.dto.quality.QualityMeasurementDto;
import es.iti.dm.governance.integration.atlas.mapper.AtlasEntityMapper;
import es.iti.dm.governance.integration.atlas.mapper.AtlasEntitySimplMapper;
import es.iti.dm.governance.integration.atlas.service.AtlasColumnEntityService;
import es.iti.dm.governance.integration.atlas.service.AtlasEntityService;
import lombok.AllArgsConstructor;
import org.apache.atlas.model.instance.AtlasEntity;
import org.apache.atlas.model.instance.AtlasRelatedObjectId;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static es.iti.dm.governance.integration.atlas.mapper.AtlasEntityMapper.PARENT_BASE_GUID;
import static es.iti.dm.governance.util.EntitiesConstants.*;
import static es.iti.dm.governance.util.ErrorMessages.ENTITY_TYPE_MISMATCH_ERROR;

@Service
@AllArgsConstructor
public class AtlasColumnEntityServiceImpl implements AtlasColumnEntityService {

    private AtlasEntityService atlasEntityService;

    @Override
    public ColumnEntityDto get(UUID columnId) {
        AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo = atlasEntityService.getEntity(String.valueOf(columnId));
        checkColumnType(atlasEntityWithExtInfo.getEntity().getTypeName(), columnId);
        ColumnEntityDto columnEntityDto = AtlasEntityMapper.MAPPER.toColumnEntity(atlasEntityWithExtInfo.getEntity());

        List<QualityMeasurementDto> qualityMeasurementDtoList = new ArrayList<>();
        if (atlasEntityWithExtInfo.getEntity().getRelationshipAttribute(QUALITY_MEASUREMENTS_RELATION_ATTRIBUTE) != null) {
            List<AtlasEntity> atlasEntityList = new ArrayList<>();
            List<AtlasRelatedObjectId> atlasObjectIdList = new ObjectMapper().convertValue(atlasEntityWithExtInfo.getEntity().getRelationshipAttribute(QUALITY_MEASUREMENTS_RELATION_ATTRIBUTE), new TypeReference<List<AtlasRelatedObjectId>>() {});
            atlasObjectIdList.stream().forEach( atlasRelatedObjectId -> atlasEntityList.add(atlasEntityWithExtInfo.getReferredEntity(atlasRelatedObjectId.getGuid())));
            atlasEntityList.stream().forEach( atlasEntity -> qualityMeasurementDtoList.add(
                    AtlasEntityMapper.MAPPER.toQualityMeasurement(atlasEntity)
            ));
        }
        columnEntityDto.setQualityMeasurements(qualityMeasurementDtoList);
        return columnEntityDto;
    }

    @Override
    public void delete(UUID columnId) {
        atlasEntityService.deleteEntity(String.valueOf(columnId));
    }

    @Override
    public ColumnEntityDto update(CreateOrUpdateEntityDto createOrUpdateEntityDto, UUID columnId) {
        AtlasEntity atlasEntity = atlasEntityService.getEntity(String.valueOf(columnId)).getEntity();
        checkColumnType(atlasEntity.getTypeName(), columnId);
        atlasEntity = AtlasEntityMapper.MAPPER.updateEntity(atlasEntity, createOrUpdateEntityDto);
        atlasEntityService.updateEntity(atlasEntity);
        return get(columnId);
    }

    @Override
    public ColumnEntityDto create(CreateColumnEntityDto createColumnEntityDto) {
        String qualifiedName = String.valueOf(createColumnEntityDto.getStructuredDatasetId()).concat(".").concat(createColumnEntityDto.getMetadata().getTitle());
        AtomicInteger idGenerator = new AtomicInteger(Integer.valueOf(PARENT_BASE_GUID));
        List<AtlasEntity> atlasEntityList = AtlasEntityMapper.MAPPER.fromCreateColumnsToColumnsEntities(Collections.singletonList(createColumnEntityDto), String.valueOf(createColumnEntityDto.getStructuredDatasetId()), String.valueOf(createColumnEntityDto.getStructuredDatasetId()), idGenerator);
        atlasEntityService.createEntities(atlasEntityList);
        return AtlasEntityMapper.MAPPER.toColumnEntity(atlasEntityService.getEntityByQualifiedName(qualifiedName, COLUMN_ENTITY_TYPE).getEntity());
    }

    @Override
    public List<ColumnSimplDto> getColumns(UUID structuredDatasetId) {
        AtlasEntity.AtlasEntityWithExtInfo structuredDatasetEntity = atlasEntityService.getEntity(String.valueOf(structuredDatasetId));
        return AtlasEntitySimplMapper.MAPPER.getColumnsSimplFromExtInfo(structuredDatasetEntity).stream()
                .sorted(Comparator.comparing(ColumnSimplDto::getPosition))
                .toList();
    }

    private void checkColumnType(String type, UUID id) {
        if (!COLUMN_ENTITY_TYPE.equals(type)) {
            throw new EntityTypeMissmatchException(String.format(ENTITY_TYPE_MISMATCH_ERROR, id, COLUMN_ENTITY_TYPE));
        }
    }
}
