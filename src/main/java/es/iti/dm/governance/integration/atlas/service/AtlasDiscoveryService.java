/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.service;

import es.iti.dm.governance.dto.search.SearchDto;
import es.iti.dm.governance.dto.search.SearchResultDto;
import es.iti.dm.governance.dto.search.TermSearchResultDto;
import org.apache.atlas.model.discovery.AtlasSearchResult;
import org.apache.atlas.model.instance.AtlasEntityHeader;

import java.util.List;

public interface AtlasDiscoveryService {
    AtlasSearchResult getDistributionSizeByQualifiedName(String qualifiedName);
    List<SearchResultDto> search(SearchDto searchDto);
    List<TermSearchResultDto> getTermsByText(String text);
    List<AtlasEntityHeader> getGlossaries(int limit, int offset);
    String findCatalogIdFromDatasetQualifiedName(String qualifiedName);
    List<AtlasEntityHeader> getEntitiesAssignedToClassification(String domainName, String typeName, int limit, int offset);
    List<AtlasEntityHeader> getEntitiesAssignedToTerm(String termQualifiedName, int limit, int offset);
}
