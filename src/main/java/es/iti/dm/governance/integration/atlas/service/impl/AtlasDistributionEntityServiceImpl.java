/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.iti.dm.governance.dto.distribution.DistributionDto;
import es.iti.dm.governance.integration.atlas.mapper.AtlasEntityMapper;
import es.iti.dm.governance.integration.atlas.service.AtlasDiscoveryService;
import es.iti.dm.governance.integration.atlas.service.AtlasDistributionEntityService;
import es.iti.dm.governance.integration.atlas.service.AtlasEntityService;
import es.iti.dm.governance.integration.persistence.repository.PersistenceRepository;
import lombok.AllArgsConstructor;
import org.apache.atlas.model.discovery.AtlasSearchResult;
import org.apache.atlas.model.instance.AtlasEntity;
import org.apache.atlas.model.instance.AtlasRelatedObjectId;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static es.iti.dm.governance.integration.atlas.utils.Constants.ATTRIBUTE_GUID;
import static es.iti.dm.governance.util.EntitiesConstants.*;

@Service
@AllArgsConstructor
public class AtlasDistributionEntityServiceImpl implements AtlasDistributionEntityService {

    private AtlasDiscoveryService atlasDiscoveryService;
    private PersistenceRepository persistenceRepository;
    private AtlasEntityService atlasEntityService;
    
    @Override
    public List<DistributionDto> getDistributionsByEntityWithExtInfo(AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo) {
        List<AtlasRelatedObjectId> distributionsRelation = new ObjectMapper().convertValue(atlasEntityWithExtInfo.getEntity().getRelationshipAttribute(DISTRIBUTIONS_RELATION_ATTRIBUTE), new TypeReference<List<AtlasRelatedObjectId>>(){});
        List<DistributionDto> distributionDtoList = new ArrayList<>();
        distributionsRelation.stream()
                .filter(atlasRelatedObjectId -> atlasRelatedObjectId.getEntityStatus().equals(AtlasEntity.Status.ACTIVE))
                .forEach(atlasRelatedObjectId -> {
                    AtlasEntity atlasEntity = atlasEntityWithExtInfo.getReferredEntity(atlasRelatedObjectId.getGuid());
                    distributionDtoList.add(AtlasEntityMapper.MAPPER.toDistributionEntity(atlasEntity));
                });
        return distributionDtoList;
    }

    @Override
    public Long getDistributionSizeByQualifiedName(String qualifiedName) {
        AtlasSearchResult atlasSearchResult = atlasDiscoveryService.getDistributionSizeByQualifiedName(qualifiedName);
        if (atlasSearchResult.getAttributes() == null) {
            return null;
        }
        return Long.valueOf((Integer)atlasSearchResult.getAttributes().getValues().get(0).get(0));
    }

    @Override
    public void createFileDistribution(AtlasEntity distributionEntity, String fileId, String fileName) {
        HashMap distributionDatasetMap = (HashMap) distributionEntity.getRelationshipAttributes().get(DATASET_RELATION_ATTRIBUTE);
        distributionDatasetMap.put(ATTRIBUTE_GUID, fileId);
        distributionEntity.getRelationshipAttributes().put(DATASET_RELATION_ATTRIBUTE, distributionDatasetMap);
        distributionEntity.setAttribute(ACCESS_URL_ATTRIBUTE, persistenceRepository.getFileUrl(UUID.fromString(fileId), fileName).getUrl());

        atlasEntityService.createEntity(distributionEntity);
    }
}
