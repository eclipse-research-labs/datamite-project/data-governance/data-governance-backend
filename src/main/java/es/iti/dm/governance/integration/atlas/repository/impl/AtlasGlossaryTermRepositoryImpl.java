/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.repository.impl;

import es.iti.dm.governance.exception.DeleteResourceInUseException;
import es.iti.dm.governance.exception.InvalidInputException;
import es.iti.dm.governance.exception.MetadataStorageException;
import es.iti.dm.governance.integration.atlas.repository.AtlasGlossaryTermRepository;
import org.apache.atlas.AtlasClientV2;
import org.apache.atlas.AtlasServiceException;
import org.apache.atlas.model.glossary.AtlasGlossaryTerm;
import org.apache.atlas.model.instance.AtlasRelatedObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static es.iti.dm.governance.util.ErrorMessages.DISASSOCIATE_TERM_INVALID_INPUT_ERROR;

@Service
public class AtlasGlossaryTermRepositoryImpl implements AtlasGlossaryTermRepository {
    @Autowired
    private AtlasClientV2 atlasClientV2;

    @Override
    public AtlasGlossaryTerm create(AtlasGlossaryTerm atlasGlossaryTerm) {
        try {
            return atlasClientV2.createGlossaryTerm(atlasGlossaryTerm);
        } catch (AtlasServiceException e) {
            throw new MetadataStorageException(e);
        }
    }

    @Override
    public void deleteById(String id) {
        try {
            atlasClientV2.deleteGlossaryTermByGuid(id);
        } catch (AtlasServiceException e) {
            if (e.getMessage().contains("ATLAS-400-00-084")) {
                throw new DeleteResourceInUseException(e.getMessage().split("\"")[7].replace("guid","id"));
            } else {
                throw new MetadataStorageException(e);
            }
        }
    }

    @Override
    public AtlasGlossaryTerm update(AtlasGlossaryTerm atlasGlossaryTerm) {
        try {
            return atlasClientV2.updateGlossaryTermByGuid(atlasGlossaryTerm.getGuid(), atlasGlossaryTerm);
        } catch (AtlasServiceException e) {
            throw new MetadataStorageException(e);
        }
    }

    @Override
    public AtlasGlossaryTerm getById(String id) {
        try {
            return atlasClientV2.getGlossaryTerm(id);
        } catch (AtlasServiceException e) {
            throw new MetadataStorageException(e);
        }
    }

    @Override
    public void assignToEntities(String id, List<AtlasRelatedObjectId> entities) {
        try {
            atlasClientV2.assignTermToEntities(id, entities);
        } catch (AtlasServiceException e) {
            throw new MetadataStorageException(e);
        }
    }

    @Override
    public void disassociateFromEntities(String id, List<AtlasRelatedObjectId> entities) {
        try {
            atlasClientV2.disassociateTermFromEntities(id, entities);
        } catch (AtlasServiceException e) {
            if (e.getMessage().contains("ATLAS-400-00-080")) {
                throw new InvalidInputException(e.getMessage().split("\"")[7].replace("guid","id").replace("relationshipGuid","relationId"));
            }
            if (e.getMessage().contains("status 500")) {
                throw new InvalidInputException(DISASSOCIATE_TERM_INVALID_INPUT_ERROR);
            } else {
                throw new MetadataStorageException(e);
            }
        }
    }

}
