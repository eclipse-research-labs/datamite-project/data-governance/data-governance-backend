/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.mapper;

import es.iti.dm.governance.dto.abstractmodel.EntityRelationHeaderDto;
import org.apache.atlas.model.glossary.relations.AtlasGlossaryHeader;
import org.apache.atlas.model.glossary.relations.AtlasRelatedTermHeader;
import org.apache.atlas.model.instance.AtlasRelatedObjectId;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Set;

import static es.iti.dm.governance.util.EntitiesConstants.GLOSSARY;
import static es.iti.dm.governance.util.EntitiesConstants.GLOSSARY_TERM;

@Mapper(nullValueIterableMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
public interface AtlasGlossaryUtilMapper {

    AtlasGlossaryUtilMapper MAPPER = Mappers.getMapper(AtlasGlossaryUtilMapper.class);

    @Mapping(target = "id", source = "glossaryGuid")
    @Mapping(target = "name", source = "displayText")
    @Mapping(target = "type", constant = GLOSSARY)
    EntityRelationHeaderDto fromAtlasGlossaryHeader(AtlasGlossaryHeader atlasGlossaryHeader);

    @Mapping(target = "id", source = "guid")
    @Mapping(target = "name", source = "displayText")
    @Mapping(target = "type", source = "typeName")
    EntityRelationHeaderDto fromAtlasRelatedObjectId(AtlasRelatedObjectId atlasRelatedObjectId);

    List<EntityRelationHeaderDto> fromAtlasRelatedObjectIdSet(Set<AtlasRelatedObjectId> atlasRelatedObjectIdSet);

    @Mapping(target = "id", source = "termGuid")
    @Mapping(target = "name", source = "displayText")
    @Mapping(target = "type", constant = GLOSSARY_TERM)
    EntityRelationHeaderDto fromAtlasRelatedTermHeader(AtlasRelatedTermHeader atlasRelatedTermHeader);

    List<EntityRelationHeaderDto> fromAtlasRelatedTermHeaderSet(Set<AtlasRelatedTermHeader> atlasRelatedTermHeaderSet);

}
