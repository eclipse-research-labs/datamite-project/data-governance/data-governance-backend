/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.utils;

public final class Constants {
    private Constants() {}

    public static final String ATLAS_REST_ADDRESS = "atlas.rest.address";

    //attributes
    public static final String ATTRIBUTE_TYPE_NAME = "typeName";
    public static final String ATTRIBUTE_QUALIFIED_NAME = "qualifiedName";
    public static final String ATTRIBUTE_GUID = "guid";
    public static final String ATTRIBUTE_MEANINGS = "meanings";
    public static final String ATTRIBUTE_ANCHOR = "anchor";
    public static final String ATTRIBUTE_ASSIGNED_ENTITIES = "assignedEntities";
    public static final String ATTRIBUTE_SEE_ALSO = "seeAlso";
    public static final String ATTRIBUTE_SYNONYMS = "synonyms";
    public static final String ATTRIBUTE_ANTONYMS = "antonyms";
    public static final String ATTRIBUTE_PREFERRED_TERMS = "preferredTerms";
    public static final String ATTRIBUTE_PREFERRED_TO_TERMS = "preferredToTerms";
    public static final String ATTRIBUTE_REPLACEMENT_TERMS = "replacementTerms";
    public static final String ATTRIBUTE_REPLACED_BY = "replacedBy";
    public static final String ATTRIBUTE_TRANSLATION_TERMS = "translationTerms";
    public static final String ATTRIBUTE_TRANSLATED_TERMS = "translatedTerms";
    public static final String ATTRIBUTE_IS_A = "isA";
    public static final String ATTRIBUTE_CLASSIFIES = "classifies";
    public static final String ATTRIBUTE_VALID_VALUES = "validValues";
    public static final String ATTRIBUTE_VALID_VALUES_FOR = "validValuesFor";

    public static final String ATTRIBUTE_SHORT_DESCRIPTION = "shortDescription";
    public static final String ATTRIBUTE_LONG_DESCRIPTION = "longDescription";
    public static final String ATTRIBUTE_USAGE = "usage";
    public static final String ATTRIBUTE_LANGUAGE = "language";
    public static final String ATTRIBUTE_EXAMPLES = "examples";
    public static final String ATTRIBUTE_ABBREVIATION = "abbreviation";

    public static final String ATTRIBUTE_CREATED_AT = "__timestamp";
    public static final String ATTRIBUTE_UPDATED_AT = "__modificationTimestamp";

    public static final String SEARCH_PARAMETER_CLASSIFICATION = "classification";

    public static final String ATTRIBUTE_STATE_INTERNAL = "__state";
    public static final String STATE_VALUE_ACTIVE = "ACTIVE";

    //term relations
    public static final String SYNONYM_TERM = "AtlasGlossarySynonym";
    public static final String REPLACEMENT_TERM = "AtlasGlossaryReplacementTerm";
    public static final String TRANSLATION_TERM = "AtlasGlossaryTranslation";
    public static final String VALID_VALUE_TERM = "AtlasGlossaryValidValue";
    public static final String PREFERRED_TERM = "AtlasGlossaryPreferredTerm";
    public static final String ANTONYM_TERM = "AtlasGlossaryAntonym";
    public static final String RELATED_TERM = "AtlasGlossaryRelatedTerm";
    public static final String IS_A_TERM = "AtlasGlossaryIsARelationship";

    public static final String ATTRIBUTE_EXPRESSION = "expression";
    public static final String ATTRIBUTE_STEWARD = "steward";
    public static final String ATTRIBUTE_SOURCE = "source";
    public static final String ATTRIBUTE_DESCRIPTION = "description";
//    AtlasGlossarySynonym
//            AtlasGlossaryReplacementTerm - replacedBy end2 - replacementTerms end1
//    AtlasGlossaryTranslation - translated end2 - translation end1
//            AtlasGlossaryValidValue - validValuesFor end2 - validValues end1
//    AtlasGlossaryPreferredTerm - preferredTerms end2 - preferredToTerms end 1
//    AtlasGlossaryAntonym
//            AtlasGlossaryRelatedTerm - seeAlso
//            AtlasGlossaryIsARelationship - classifiese end2 - isA end1
}
