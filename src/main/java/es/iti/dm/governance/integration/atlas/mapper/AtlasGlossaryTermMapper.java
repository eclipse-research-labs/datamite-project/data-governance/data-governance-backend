/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.mapper;

import es.iti.dm.governance.dto.glossary.term.CreateOrUpdateGlossaryTermEntityDto;
import es.iti.dm.governance.dto.glossary.term.GlossaryTermEntityDto;
import es.iti.dm.governance.dto.glossary.term.TermEntityRelationHeaderDto;
import es.iti.dm.governance.dto.search.TermSearchResultDto;
import org.apache.atlas.model.glossary.AtlasGlossaryTerm;
import org.apache.atlas.model.instance.AtlasEntity;
import org.apache.atlas.model.instance.AtlasEntityHeader;
import org.apache.atlas.model.instance.AtlasRelatedObjectId;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Map;

import static es.iti.dm.governance.integration.atlas.utils.Constants.*;
import static es.iti.dm.governance.util.EntitiesConstants.ENTITY_NAME_ATTRIBUTE;

@Mapper(uses = {AtlasGlossaryUtilMapper.class, AtlasEntityAttributesUtil.class, AtlasEntityMapper.class})
public interface AtlasGlossaryTermMapper {
    AtlasGlossaryTermMapper MAPPER = Mappers.getMapper(AtlasGlossaryTermMapper.class);

    @Mapping(target = "shortDescription", source = "summary")
    @Mapping(target = "longDescription", source = "description")
    //TODO use name in qualifiedName or other?
    @Mapping(target = "qualifiedName", source = "name")
    @Mapping(target = "anchor.glossaryGuid", source = "glossaryId")
    AtlasGlossaryTerm fromCreateGlossaryEntity(CreateOrUpdateGlossaryTermEntityDto createOrUpdateGlossaryTermEntityDto);

    default void updateGlossaryTermEntityAttributes(CreateOrUpdateGlossaryTermEntityDto createOrUpdateGlossaryTermEntityDto, Map<String, Object> attributes) {
        if (!((String)attributes.get(ENTITY_NAME_ATTRIBUTE)).equals(createOrUpdateGlossaryTermEntityDto.getName())) {
            var oldQualifiedName = ((String)attributes.get(ATTRIBUTE_QUALIFIED_NAME)).split("@");
            attributes.put(ATTRIBUTE_QUALIFIED_NAME, createOrUpdateGlossaryTermEntityDto.getName().concat("@").concat(oldQualifiedName[1]));
        }

        attributes.put(ATTRIBUTE_EXAMPLES, createOrUpdateGlossaryTermEntityDto.getExamples());
        attributes.put(ATTRIBUTE_ABBREVIATION, createOrUpdateGlossaryTermEntityDto.getAbbreviation());
        attributes.put(ATTRIBUTE_USAGE, createOrUpdateGlossaryTermEntityDto.getUsage());
        attributes.put(ATTRIBUTE_SHORT_DESCRIPTION, createOrUpdateGlossaryTermEntityDto.getSummary());
        attributes.put(ATTRIBUTE_LONG_DESCRIPTION, createOrUpdateGlossaryTermEntityDto.getDescription());
        attributes.put(ENTITY_NAME_ATTRIBUTE, createOrUpdateGlossaryTermEntityDto.getName());

    }

    @Mapping(target = "id", source = "guid")
    @Mapping(target = "summary", source = "shortDescription")
    @Mapping(target = "description", source = "longDescription")
    @Mapping(target = "glossary", source = "anchor")
    @Mapping(target = "assignations", source = "assignedEntities")
    GlossaryTermEntityDto toGlossaryEntity(AtlasGlossaryTerm atlasGlossaryTerm);

    @Mapping(target = "id", source = "guid")
    @Mapping(target = "name", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeEntityName.class)
    @Mapping(target = "qualifiedName", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeQualifiedName.class)
    TermSearchResultDto fromAtlasEntityHeader(AtlasEntityHeader atlasEntityHeader);
    List<TermSearchResultDto> fromAtlasEntityHeaderList(List<AtlasEntityHeader> atlasEntityHeaderList);

    @Mapping(target = "id", source = "guid")
    @Mapping(target = "name", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeEntityName.class)
    @Mapping(target = "description", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeLongDescription.class)
    @Mapping(target = "summary", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeShortDescription.class)
    @Mapping(target = "usage", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeUsage.class)
    @Mapping(target = "abbreviation", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeAbbreviation.class)
    @Mapping(target = "examples", source = "attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeExamples.class)
    @Mapping(target = "glossary", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeGlossaryRelation.class)
    @Mapping(target = "createdAt", source = "createTime")
    @Mapping(target = "updatedAt", source = "updateTime")
    @Mapping(target = "seeAlso", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeTermSeeAlsoRelation.class)
    @Mapping(target = "synonyms", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeTermSynonymsRelation.class)
    @Mapping(target = "antonyms", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeTermAntonymsRelation.class)
    @Mapping(target = "preferredTerms", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeTermPreferredTermsRelation.class)
    @Mapping(target = "preferredToTerms", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeTermPreferredToTermsRelation.class)
    @Mapping(target = "replacementTerms", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeTermReplacementTermsRelation.class)
    @Mapping(target = "replacedBy", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeTermReplacedByRelation.class)
    @Mapping(target = "translationTerms", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeTermTranslationTermsRelation.class)
    @Mapping(target = "translatedTerms", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeTermTranslatedTermsRelation.class)
    @Mapping(target = "isA", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeTermIsARelation.class)
    @Mapping(target = "classifies", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeTermClassifiesRelation.class)
    @Mapping(target = "validValues", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeTermValidValuesRelation.class)
    @Mapping(target = "validValuesFor", source = "relationshipAttributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeTermValidValuesForRelation.class)
    @Mapping(target = "domains", source = "classifications")
    GlossaryTermEntityDto fromAtlasEntity(AtlasEntity atlasEntity);

    @Mapping(target = "id", source = "guid")
    @Mapping(target = "name", source = "displayText")
    @Mapping(target = "type", source = "typeName")
    @Mapping(target = "relationId", source = "relationshipGuid")
    @Mapping(target = "relationAttributes.expression", source = "relationshipAttributes.attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeExpression.class)
    @Mapping(target = "relationAttributes.steward", source = "relationshipAttributes.attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeSteward.class)
    @Mapping(target = "relationAttributes.description", source = "relationshipAttributes.attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeDescription.class)
    @Mapping(target = "relationAttributes.source", source = "relationshipAttributes.attributes", qualifiedBy = AtlasEntityAttributesUtil.AtlasEntityAttributeSource.class)
    TermEntityRelationHeaderDto fromRelatedObjectId(AtlasRelatedObjectId atlasRelatedObjectId);

}
