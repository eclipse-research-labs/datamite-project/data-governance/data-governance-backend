/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.mapper;

import es.iti.dm.governance.dto.domain.DomainDto;
import es.iti.dm.governance.dto.domain.CreateDomainDto;
import es.iti.dm.governance.dto.domain.UpdateDomainDto;
import org.apache.atlas.model.instance.ClassificationAssociateRequest;
import org.apache.atlas.model.typedef.AtlasClassificationDef;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = {UtilMapper.class})
public interface AtlasClassificationMapper {
    AtlasClassificationMapper MAPPER = Mappers.getMapper(AtlasClassificationMapper.class);

    @Mapping(target = "superTypes", source = "parent", qualifiedBy = UtilMapper.StringToSet.class)
    AtlasClassificationDef fromCreateClassificationToDef(CreateDomainDto createDomainDto);

    @Mapping(target = "parent", source = "superTypes", qualifiedBy = UtilMapper.FirstElementOfSet.class)
    @Mapping(target = "subDomains", source = "subTypes")
    DomainDto toDomain(AtlasClassificationDef atlasClassificationDef);

    List<DomainDto> toClassifications(List<AtlasClassificationDef> atlasClassificationDefList);

    CreateDomainDto fromUpdateToCreate(UpdateDomainDto updateDomainDto);

    @Mapping(target = "classification.typeName", source = "classificationName")
    @Mapping(target = "entityGuids", source = "entityId", qualifiedBy = UtilMapper.StringToList.class)
    @Mapping(target = "classification.propagate", source = "propagate")
    ClassificationAssociateRequest toClassificationAssociateRequest(String classificationName, String entityId, Boolean propagate);

}
