/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.repository.impl;

import es.iti.dm.governance.exception.MetadataStorageException;
import es.iti.dm.governance.integration.atlas.repository.AtlasEntityRepository;
import org.apache.atlas.AtlasClientV2;
import org.apache.atlas.AtlasServiceException;
import org.apache.atlas.model.instance.AtlasEntity;
import org.apache.atlas.model.instance.ClassificationAssociateRequest;
import org.apache.atlas.model.instance.EntityMutationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AtlasEntityRepositoryImpl implements AtlasEntityRepository {

    @Autowired
    private AtlasClientV2 atlasClientV2;

    @Override
    public EntityMutationResponse deleteById(String entityId) {
        try {
            return atlasClientV2.deleteEntityByGuid(entityId);
        } catch (AtlasServiceException e) {
            throw new MetadataStorageException(e);
        }
    }

    @Override
    public EntityMutationResponse deleteByIds(List<String> ids) {
        try {
            return atlasClientV2.deleteEntitiesByGuids(ids);
        } catch (AtlasServiceException e) {
            throw new MetadataStorageException(e);
        }
    }

    @Override
    public EntityMutationResponse create(AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo) {
        try {
            return atlasClientV2.createEntity(atlasEntityWithExtInfo);
        } catch (AtlasServiceException e) {
            throw new MetadataStorageException(e);
        }
    }

    @Override
    public EntityMutationResponse update(AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo) {
        try {
            return atlasClientV2.updateEntity(atlasEntityWithExtInfo);
        } catch (AtlasServiceException e) {
            throw new MetadataStorageException(e);
        }
    }

    @Override
    public AtlasEntity.AtlasEntityWithExtInfo getById(String entityId) {
        return getById(entityId, false, false);
    }

    @Override
    public AtlasEntity.AtlasEntityWithExtInfo getById(String entityId, boolean minExtInfo, boolean ignoreRelationships) {
        try {
            return atlasClientV2.getEntityByGuid(entityId, minExtInfo, ignoreRelationships);
        } catch (AtlasServiceException e) {
            throw new MetadataStorageException(e);
        }
    }

    @Override
    public EntityMutationResponse createBulk(AtlasEntity.AtlasEntitiesWithExtInfo atlasEntitiesWithExtInfo) {
        try {
            return atlasClientV2.createEntities(atlasEntitiesWithExtInfo);
        } catch (AtlasServiceException e) {
            throw new MetadataStorageException(e);
        }
    }

    @Override
    public AtlasEntity.AtlasEntityWithExtInfo getByUniqueAttribute(Map<String, String> uniqueAttribute, String entityType) {
        try {
            return atlasClientV2.getEntityByAttribute(entityType, uniqueAttribute);
        } catch (AtlasServiceException e) {
            throw new MetadataStorageException(e);
        }
    }

    @Override
    public void addClassification(ClassificationAssociateRequest classificationAssociateRequest) {
        try {
            atlasClientV2.addClassification(classificationAssociateRequest);
        } catch (AtlasServiceException e) {
            if (!e.getMessage().contains("already associated with classification: " + classificationAssociateRequest.getClassification().getTypeName())) { //no dar error si la clasificación ya está asignada
                throw new MetadataStorageException(e);
            }
        }
    }

    @Override
    public void deleteClassification(String uuid, String classificationName) {
        try {
            atlasClientV2.deleteClassification(uuid, classificationName);
        } catch (AtlasServiceException e) {
            if (!e.getMessage().contains("Classification " + classificationName + " is not associated with entity")) { //no dar error si la clasificación no está asignada
                throw new MetadataStorageException(e);
            }
        }
    }

}
