/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.iti.dm.governance.dto.abstractmodel.CreateOrUpdateEntityDto;
import es.iti.dm.governance.dto.distribution.DistributionDto;
import es.iti.dm.governance.dto.structureddataset.CreateStructuredDatasetEntityDto;
import es.iti.dm.governance.dto.structureddataset.StructuredDatasetEntityDto;
import es.iti.dm.governance.integration.atlas.mapper.AtlasEntityMapper;
import es.iti.dm.governance.integration.atlas.service.AtlasCatalogEntityService;
import es.iti.dm.governance.integration.atlas.service.AtlasDistributionEntityService;
import es.iti.dm.governance.integration.atlas.service.AtlasEntityService;
import es.iti.dm.governance.integration.atlas.service.AtlasStructuredDatasetEntityService;
import lombok.AllArgsConstructor;
import org.apache.atlas.model.instance.AtlasEntity;
import org.apache.atlas.model.instance.AtlasObjectId;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static es.iti.dm.governance.util.EntitiesConstants.*;
import static es.iti.dm.governance.util.ErrorMessages.ENTITY_TYPE_MISMATCH_ERROR;

@Service
@AllArgsConstructor
public class AtlasStructuredDatasetEntityServiceImpl implements AtlasStructuredDatasetEntityService {
    private AtlasEntityService atlasEntityService;
    private AtlasDistributionEntityService atlasDistributionEntityService;
    private AtlasCatalogEntityService atlasCatalogEntityService;

    @Override
    public StructuredDatasetEntityDto create(CreateStructuredDatasetEntityDto createStructuredDatasetEntityDto) {
        //TODO random ID or something like hash? with hash cannot create the same file in different catalogs
        String qualifiedName = String.valueOf(UUID.randomUUID());

        List<AtlasEntity> atlasEntityList = AtlasEntityMapper.MAPPER.fromCreateStructuredDatasetToStructuredDatasetEntities(createStructuredDatasetEntityDto, qualifiedName);
        AtlasEntity newDistributionEntity = atlasEntityList.stream().filter(atlasEntity -> atlasEntity.getTypeName().equals(DISTRIBUTION_ENTITY)).findFirst().get();
        AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo;
        atlasEntityList.add(atlasCatalogEntityService.getUpdatedCatalogSize(createStructuredDatasetEntityDto.getCatalogId(), newDistributionEntity));
        if (createStructuredDatasetEntityDto.getDistributions().get(0).getAccessUrl() == null) {
            AtlasEntity fileEntity = atlasEntityList.stream().filter( atlasEntity -> atlasEntity.getTypeName().equals(STRUCTURED_DATASET_ENTITY_TYPE)).findFirst().get();
            fileEntity = atlasEntityService.createEntity(fileEntity);
            atlasDistributionEntityService.createFileDistribution(newDistributionEntity, fileEntity.getGuid(), createStructuredDatasetEntityDto.getMetadata().getTitle());
            atlasEntityWithExtInfo = atlasEntityService.getEntity(fileEntity.getGuid());
        } else {
            atlasEntityService.createEntities(atlasEntityList);
            atlasEntityWithExtInfo = atlasEntityService.getEntityByQualifiedName(qualifiedName, STRUCTURED_DATASET_ENTITY_TYPE);
        }
        StructuredDatasetEntityDto structuredDatasetEntityDto = AtlasEntityMapper.MAPPER.toStructuredDatasetEntity(atlasEntityWithExtInfo.getEntity());
        structuredDatasetEntityDto.setDistributions(atlasDistributionEntityService.getDistributionsByEntityWithExtInfo(atlasEntityWithExtInfo));
        return structuredDatasetEntityDto;
    }

    @Override
    public StructuredDatasetEntityDto get(UUID structuredDatasetId) {
        AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo = atlasEntityService.getEntity(String.valueOf(structuredDatasetId), true, false);
        checkStructuredDatasetType(atlasEntityWithExtInfo.getEntity().getTypeName(), structuredDatasetId);
        StructuredDatasetEntityDto structuredDatasetEntityDto = AtlasEntityMapper.MAPPER.toStructuredDatasetEntity(atlasEntityWithExtInfo.getEntity());
        structuredDatasetEntityDto.setDistributions(atlasDistributionEntityService.getDistributionsByEntityWithExtInfo(atlasEntityWithExtInfo));
        return structuredDatasetEntityDto;
    }

    @Override
    public StructuredDatasetEntityDto update(CreateOrUpdateEntityDto createOrUpdateEntityDto, UUID structuredDatasetId) {
        AtlasEntity atlasEntity = atlasEntityService.getEntity(String.valueOf(structuredDatasetId)).getEntity();
        checkStructuredDatasetType(atlasEntity.getTypeName(), structuredDatasetId);
        atlasEntity = AtlasEntityMapper.MAPPER.updateEntity(atlasEntity, createOrUpdateEntityDto);
        atlasEntityService.updateEntity(atlasEntity);
        return get(structuredDatasetId);
    }

    @Override
    public void delete(UUID structuredDatasetId) {
        AtlasEntity.AtlasEntityWithExtInfo fileEntity = atlasEntityService.getEntity(String.valueOf(structuredDatasetId), true, false);
        checkStructuredDatasetType(fileEntity.getEntity().getTypeName(), structuredDatasetId);
        AtlasObjectId atlasObjectId = new ObjectMapper().convertValue(fileEntity.getEntity().getAttribute(CATALOG_RELATION_ATTRIBUTE), new TypeReference<AtlasObjectId>() {});
        AtlasEntity.AtlasEntityWithExtInfo catalogEntity = atlasEntityService.getEntity(atlasObjectId.getGuid());
        Long catalogSize = Long.valueOf((Integer) catalogEntity.getEntity().getAttribute(BYTE_SIZE_ATTRIBUTE));
        catalogEntity.getEntity().setAttribute(BYTE_SIZE_ATTRIBUTE, catalogSize - atlasDistributionEntityService.getDistributionsByEntityWithExtInfo(fileEntity).stream().collect(Collectors.summingLong(DistributionDto::getByteSize)));
        atlasEntityService.updateEntity(catalogEntity.getEntity());

        atlasEntityService.deleteEntity(String.valueOf(structuredDatasetId));
    }

    private void checkStructuredDatasetType(String type, UUID id) {
        if (!STRUCTURED_DATASET_ENTITY_TYPE.equals(type)) {
            throw new EntityTypeMissmatchException(String.format(ENTITY_TYPE_MISMATCH_ERROR, id, STRUCTURED_DATASET_ENTITY_TYPE));
        }
    }
}
