/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.service.impl;

import es.iti.dm.governance.dto.glossary.CreateOrUpdateGlossaryEntityDto;
import es.iti.dm.governance.dto.glossary.GlossaryEntityDto;
import es.iti.dm.governance.integration.atlas.mapper.AtlasGlossaryMapper;
import es.iti.dm.governance.integration.atlas.repository.AtlasGlossaryRepository;
import es.iti.dm.governance.integration.atlas.service.AtlasDiscoveryService;
import es.iti.dm.governance.integration.atlas.service.AtlasEntityService;
import es.iti.dm.governance.integration.atlas.service.AtlasGlossaryService;
import lombok.AllArgsConstructor;
import org.apache.atlas.model.glossary.AtlasGlossary;
import org.apache.atlas.model.instance.AtlasEntity;
import org.apache.atlas.model.instance.AtlasEntityHeader;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AtlasGlossaryServiceImpl implements AtlasGlossaryService {

    private AtlasGlossaryRepository atlasGlossaryRepository;
    private AtlasDiscoveryService atlasDiscoveryService;
    private AtlasEntityService atlasEntityService;

    @Override
    public GlossaryEntityDto create(CreateOrUpdateGlossaryEntityDto createOrUpdateGlossaryEntityDto) {
        AtlasGlossary atlasGlossary = AtlasGlossaryMapper.MAPPER.fromCreateGlossaryEntity(createOrUpdateGlossaryEntityDto);
        atlasGlossary = atlasGlossaryRepository.create(atlasGlossary);
        return AtlasGlossaryMapper.MAPPER.toGlossaryEntity(atlasGlossary);
    }

    @Override
    public GlossaryEntityDto get(UUID glossaryId) {
        AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo = atlasEntityService.getEntity(String.valueOf(glossaryId));
        return AtlasGlossaryMapper.MAPPER.fromAtlasEntity(atlasEntityWithExtInfo.getEntity());
    }

    @Override
    public GlossaryEntityDto update(CreateOrUpdateGlossaryEntityDto createOrUpdateGlossaryEntityDto, UUID glossaryId) {
        AtlasGlossary atlasGlossary = AtlasGlossaryMapper.MAPPER.fromCreateGlossaryEntity(createOrUpdateGlossaryEntityDto);
        atlasGlossary.setGuid(String.valueOf(glossaryId));
        atlasGlossary = atlasGlossaryRepository.update(atlasGlossary);
        return AtlasGlossaryMapper.MAPPER.toGlossaryEntity(atlasGlossary);
    }

    @Override
    public void delete(UUID glossaryId) {
        atlasGlossaryRepository.deleteById(String.valueOf(glossaryId));
    }

    @Override
    public List<GlossaryEntityDto> getAll(int limit, int offset) {
        List<AtlasEntityHeader> atlasEntityHeaderList = atlasDiscoveryService.getGlossaries(limit, offset);
        return AtlasGlossaryMapper.MAPPER.fromAtlasEntityHeaderList(atlasEntityHeaderList);
    }
}
