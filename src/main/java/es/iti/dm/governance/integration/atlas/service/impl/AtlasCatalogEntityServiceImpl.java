/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import es.iti.dm.governance.dto.abstractmodel.CreateOrUpdateEntityDto;
import es.iti.dm.governance.dto.catalog.CatalogEntityDto;
import es.iti.dm.governance.dto.catalog.CreateCatalogEntityDto;
import es.iti.dm.governance.integration.atlas.mapper.AtlasEntityMapper;
import es.iti.dm.governance.integration.atlas.service.AtlasCatalogEntityService;
import es.iti.dm.governance.integration.atlas.service.AtlasDistributionEntityService;
import es.iti.dm.governance.integration.atlas.service.AtlasEntityService;
import lombok.AllArgsConstructor;
import org.apache.atlas.model.instance.AtlasEntity;
import org.apache.atlas.model.instance.AtlasObjectId;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static es.iti.dm.governance.integration.atlas.utils.Constants.ATTRIBUTE_QUALIFIED_NAME;
import static es.iti.dm.governance.util.EntitiesConstants.*;
import static es.iti.dm.governance.util.EntitiesConstants.DATASETS_RELATION_ATTRIBUTE;
import static es.iti.dm.governance.util.ErrorMessages.ENTITY_TYPE_MISMATCH_ERROR;

@Service
@AllArgsConstructor
public class AtlasCatalogEntityServiceImpl implements AtlasCatalogEntityService {
    private AtlasEntityService atlasEntityService;
    private AtlasDistributionEntityService atlasDistributionEntityService;

    private static final int MAX_DELETE_PER_QUERY = 50;

    @Override
    public CatalogEntityDto create(CreateCatalogEntityDto createCatalogEntityDto) {
        AtlasEntity atlasEntity = AtlasEntityMapper.MAPPER.fromCreateOrUpdateCatalogEntity(createCatalogEntityDto, String.valueOf(UUID.randomUUID()));
        atlasEntity = atlasEntityService.createEntity(atlasEntity);
        return get(UUID.fromString(atlasEntity.getGuid()));
    }

    @Override
    public CatalogEntityDto get(UUID catalogId) {
        AtlasEntity.AtlasEntityWithExtInfo atlasEntityWithExtInfo = atlasEntityService.getEntity(String.valueOf(catalogId));
        checkCatalogType(atlasEntityWithExtInfo.getEntity().getTypeName(), catalogId);
        return AtlasEntityMapper.MAPPER.toCatalogEntity(atlasEntityWithExtInfo.getEntity());
    }

    @Override
    public CatalogEntityDto update(CreateOrUpdateEntityDto createOrUpdateEntityDto, UUID catalogId) {
        AtlasEntity atlasEntity = atlasEntityService.getEntity(String.valueOf(catalogId)).getEntity();
        checkCatalogType(atlasEntity.getTypeName(), catalogId);
        atlasEntity = AtlasEntityMapper.MAPPER.updateEntity(atlasEntity, createOrUpdateEntityDto);
        atlasEntityService.updateEntity(atlasEntity);
        return get(catalogId);
    }

    @Override
    public void delete(UUID catalogId) {
        AtlasEntity.AtlasEntityWithExtInfo catalogEntity = atlasEntityService.getEntity(String.valueOf(catalogId));
        checkCatalogType(catalogEntity.getEntity().getTypeName(), catalogId);
        List<AtlasObjectId> atlasObjectIdList = new ObjectMapper().convertValue(catalogEntity.getEntity().getRelationshipAttribute(DATASETS_RELATION_ATTRIBUTE), new TypeReference<List<AtlasObjectId>>() {});
        List<String> guids = new ArrayList<>();
        atlasObjectIdList.stream().forEach(atlasObjectId -> guids.add(atlasObjectId.getGuid()));

        guids.add(catalogEntity.getEntity().getGuid());

        List<List<String>> guidsSplited = Lists.partition(guids, MAX_DELETE_PER_QUERY);
        guidsSplited.stream().forEach(ids -> atlasEntityService.deleteEntities(ids));

        atlasEntityService.deleteEntity(String.valueOf(catalogId));
    }

    @Override
    public AtlasEntity getUpdatedCatalogSize(UUID catalogId, AtlasEntity newDistributionEntity) {
        AtlasEntity.AtlasEntityWithExtInfo catalogAtlasEntity = atlasEntityService.getEntity(String.valueOf(catalogId));
        Long catalogSize = (catalogAtlasEntity.getEntity().getAttribute(BYTE_SIZE_ATTRIBUTE) == null) ? 0 : Long.valueOf((Integer) catalogAtlasEntity.getEntity().getAttribute(BYTE_SIZE_ATTRIBUTE));

        //In case of update distribution (example in database), should update the size in catalog
        //if distribution exists, add difference to catalog size. If not, add new distribution.
        Long oldDistributionSize = atlasDistributionEntityService.getDistributionSizeByQualifiedName((String) newDistributionEntity.getAttribute(ATTRIBUTE_QUALIFIED_NAME));
        Long newDistributionSize = (Long)newDistributionEntity.getAttribute(BYTE_SIZE_ATTRIBUTE);
        //if distribution exists, add difference to catalog size. If not, add new distribution.
        if (oldDistributionSize != null) {
            catalogSize = catalogSize + (newDistributionSize - oldDistributionSize);
        } else {
            catalogSize = catalogSize + newDistributionSize;
        }

        //update catalog size
        catalogAtlasEntity.getEntity().setAttribute(BYTE_SIZE_ATTRIBUTE, catalogSize);

        //Remove relation attributes to avoid error
        catalogAtlasEntity.getEntity().getAttributes().remove(DATASETS_RELATION_ATTRIBUTE);
        catalogAtlasEntity.getEntity().getRelationshipAttributes().remove(DATASETS_RELATION_ATTRIBUTE);
        return catalogAtlasEntity.getEntity();
    }

    private void checkCatalogType(String type, UUID id) {
        if (!CATALOG_ENTITY_TYPE.equals(type)) {
            throw new EntityTypeMissmatchException(String.format(ENTITY_TYPE_MISMATCH_ERROR, id, CATALOG_ENTITY_TYPE));
        }
    }
}
