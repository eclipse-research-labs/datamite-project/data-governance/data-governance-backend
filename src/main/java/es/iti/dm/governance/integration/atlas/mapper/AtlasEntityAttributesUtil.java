/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.integration.atlas.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.iti.dm.governance.dto.abstractmodel.EntityRelationHeaderDto;
import es.iti.dm.governance.dto.glossary.term.TermEntityRelationHeaderDto;
import es.iti.dm.governance.dto.quality.QualityMeasurementParameterDto;
import org.apache.atlas.model.instance.AtlasRelatedObjectId;
import org.apache.atlas.model.instance.AtlasRelationship;
import org.mapstruct.Qualifier;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static es.iti.dm.governance.integration.atlas.utils.Constants.*;
import static es.iti.dm.governance.util.EntitiesConstants.*;

public class AtlasEntityAttributesUtil {

    private static boolean isAttributeNotNull(Map<String, Object> attributes, String key) {
        return attributes != null && attributes.get(key) != null;
    }

    private static String getAttribute(Map<String, Object> attributes, String key) {
        if (isAttributeNotNull(attributes, key)) {
            return String.valueOf(attributes.get(key));
        } else {
            return null;
        }
    }

    @AtlasEntityAttributeIdentifier
    public String atlasEntityAttributeIdentifier(Map<String, Object> attributes) {
        var key = IDENTIFIER_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeTitle
    public String atlasEntityAttributeTitle(Map<String, Object> attributes) {
        var key = TITLE_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeEntityName
    public String atlasEntityAttributeEntityName(Map<String, Object> attributes) {
        var key = ENTITY_NAME_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeDescription
    public String atlasEntityAttributeDescription(Map<String, Object> attributes) {
        var key = DESCRIPTION_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeEntityDescription
    public String atlasEntityAttributeEntityDescription(Map<String, Object> attributes) {
        var key = ENTITY_DESCRIPTION_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeEntitySummary
    public String atlasEntityAttributeEntitySummary(Map<String, Object> attributes) {
        var key = ENTITY_SUMMARY_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeShortDescription
    public String atlasEntityAttributeShortDescription(Map<String, Object> attributes) {
        var key = ATTRIBUTE_SHORT_DESCRIPTION;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeLongDescription
    public String atlasEntityAttributeLongDescription(Map<String, Object> attributes) {
        var key = ATTRIBUTE_LONG_DESCRIPTION;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeCreator
    public String atlasEntityAttributeCreator(Map<String, Object> attributes) {
        var key = CREATOR_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeEntityOwnerName
    public String atlasEntityAttributeOwnerName(Map<String, Object> attributes) {
        var key = ENTITY_OWNER_NAME_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeEntityOwner
    public String atlasEntityAttributeEntityOwner(Map<String, Object> attributes) {
        var key = ENTITY_OWNER_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeEntityUpdatedBy
    public String atlasEntityAttributeUpdatedBy(Map<String, Object> attributes) {
        var key = ENTITY_UPDATED_BY_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeMediaTypeList
    public List<String> atlasEntityAttributeMediaTypeList(Map<String, Object> attributes) {
        var key = MEDIA_TYPE_ATTRIBUTE;
        var artifactTypeList = attributes.get(key);
        List<String> types = new ArrayList<>();
        if (artifactTypeList == null) {
            return types;
        }
        for (List<String> type : (List<List<String>>) artifactTypeList) {
            types.add(type.get(0));
        }
        return types;
    }

    @AtlasEntityAttributeMediaType
    public String atlasEntityAttributeMediaType(Map<String, Object> attributes) {
        var key = MEDIA_TYPE_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeConformsTo
    public String atlasEntityAttributeEncoding(Map<String, Object> attributes) {
        var key = CONFORMS_TO_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeChecksum
    public String atlasEntityAttributeChecksum(Map<String, Object> attributes) {
        var key = CHECKSUM_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeAccessUrl
    public String atlasEntityAttributeAccessUrl(Map<String, Object> attributes) {
        var key = ACCESS_URL_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeByteSize
    public Integer atlasEntityAttributeByteSize(Map<String, Object> attributes) {
        var key = BYTE_SIZE_ATTRIBUTE;
        var size = getAttribute(attributes, key);
        if (size == null) {
            return 0;
        }
        return Double.valueOf(size).intValue();
    }

    @AtlasEntityAttributeFormat
    public String atlasEntityFormat(Map<String, Object> attributes) {
        var key = FORMAT_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeFieldDelimiter
    public String atlasEntityFieldDelimiter(Map<String, Object> attributes) {
        var key = FIELD_DELIMITER_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeDecimalDelimiter
    public String atlasEntityDecimalDelimiter(Map<String, Object> attributes) {
        var key = DECIMAL_DELIMITER_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeQualifiedName
    public String atlasEntityQualifiedName(Map<String, Object> attributes) {
        return (String) attributes.get(ATTRIBUTE_QUALIFIED_NAME);
    }

    @AtlasEntityAttributeDBType
    public String atlasEntityAttributeDBType(Map<String, Object> attributes) {
        var key = DB_TYPE_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeDataType
    public String atlasEntityAttributeDataType(Map<String, Object> attributes) {
        var key = DATA_TYPE_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributePosition
    public Integer atlasEntityAttributePosition(Map<String, Object> attributes) {
        var key = POSITION_ATTRIBUTE;
        if (isAttributeNotNull(attributes, key)) {
            return (Integer) attributes.get(key);
        }
        else {
            return null;
        }
    }

    @AtlasEntityAttributeNumRecords
    public Integer atlasEntityAttributeNumRecords(Map<String, Object> attributes) {
        var key = NUM_RECORDS_ATTRIBUTE;
        if (isAttributeNotNull(attributes, key)) {
            return (Integer) attributes.get(key);
        }
        else {
            return null;
        }
    }

    @AtlasEntityAttributeIsPrivate
    public Boolean atlasEntityAttributeIsPrivate(Map<String, Object> attributes) {
        var key = IS_PRIVATE_ATTRIBUTE;
        if (isAttributeNotNull(attributes, key)) {
            return (Boolean) attributes.get(key);
        }
        else {
            return null;
        }
    }

    @AtlasEntityAttributeSchema
    public String atlasEntityAttributeSchema(Map<String, Object> attributes) {
        var key = SCHEMA_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeUsage
    public String atlasEntityAttributeUsage(Map<String, Object> attributes) {
        var key = ATTRIBUTE_USAGE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeAbbreviation
    public String atlasEntityAttributeAbbreviation(Map<String, Object> attributes) {
        var key = ATTRIBUTE_ABBREVIATION;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeExamples
    public List<String> atlasEntityAttributeExamples(Map<String, Object> attributes) {
        var key = ATTRIBUTE_EXAMPLES;
        if (isAttributeNotNull(attributes, key)) {
            return (List<String>) attributes.get(key);
        } else {
            return new ArrayList<>();
        }
    }

    @AtlasEntityAttributeLanguage
    public String atlasEntityAttributeLanguage(Map<String, Object> attributes) {
        var key = ATTRIBUTE_LANGUAGE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeMetric
    public String atlasEntityAttributeMetric(Map<String, Object> attributes) {
        var key = METRIC_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeValue
    public String atlasEntityAttributeValue(Map<String, Object> attributes) {
        var key = VALUE_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeCreatedInstant
    public Instant atlasEntityAttributeCreatedInstant(Map<String, Object> attributes) {
        var stringDate = attributes.get(CREATED_ATTRIBUTE).toString();
        return (stringDate.equals("0")) ? null : Instant.ofEpochSecond(Long.parseLong(stringDate));
    }

    @AtlasEntityAttributeCreatedAt
    public Instant atlasEntityAttributeCreatedAt(Map<String, Object> attributes) {
        var stringDate = attributes.get(ATTRIBUTE_CREATED_AT).toString();
        return Instant.ofEpochMilli(Long.parseLong(stringDate));
    }

    @AtlasEntityAttributeUpdatedAt
    public Instant atlasEntityAttributeUpdatedAt(Map<String, Object> attributes) {
        var stringDate = attributes.get(ATTRIBUTE_UPDATED_AT).toString();
        return Instant.ofEpochMilli(Long.parseLong(stringDate));
    }

    @AtlasEntityAttributeCreatedBy
    public String atlasEntityAttributeCreatedBy(Map<String, Object> attributes) {
        var key = CREATED_BY_ATTRIBUTE;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeQualityMeasurementParameters
    public List<QualityMeasurementParameterDto> atlasEntityAttributeQualityMeasurementParameters(Map<String, Object> attributes) {
        var key = PARAMETERS_ATTRIBUTE;
        if (isAttributeNotNull(attributes, key)) {
            try {
                return new ObjectMapper().readValue(((List<String>)attributes.get(key)).toString(), new TypeReference<List<QualityMeasurementParameterDto>>() {});
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        } else {
            return new ArrayList<>();
        }
    }

    @AtlasEntityAttributeCatalogRelation
    public EntityRelationHeaderDto atlasEntityAttributeCatalogRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeEntityHeaderRelation(attributes, CATALOG_RELATION_ATTRIBUTE);
    }

    @AtlasEntityAttributeTablesRelation
    public List<EntityRelationHeaderDto> atlasEntityAttributeTableRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeEntityHeaderRelations(attributes, TABLES_RELATION_ATTRIBUTE);
    }

    @AtlasEntityAttributeDatabaseRelation
    public EntityRelationHeaderDto atlasEntityAttributeDatabaseRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeEntityHeaderRelation(attributes, DATABASE_RELATION_ATTRIBUTE);
    }

    @AtlasEntityAttributeColumnsRelation
    public List<EntityRelationHeaderDto> atlasEntityAttributeColumnsRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeEntityHeaderRelations(attributes, COLUMNS_RELATION_ATTRIBUTE);
    }

    @AtlasEntityAttributeDatasetsRelation
    public List<EntityRelationHeaderDto> atlasEntityAttributeDatasetsRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeEntityHeaderRelations(attributes, DATASETS_RELATION_ATTRIBUTE);
    }

    @AtlasEntityAttributeDatasetRelation
    public EntityRelationHeaderDto atlasEntityAttributeDatasetRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeEntityHeaderRelation(attributes, DATASET_RELATION_ATTRIBUTE);
    }

    @AtlasEntityAttributeTermsRelation
    public List<EntityRelationHeaderDto> atlasEntityAttributeTermsRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeEntityHeaderRelations(attributes, ATTRIBUTE_MEANINGS);
    }

    @AtlasEntityAttributeTermsInGlossaryRelation
    public List<EntityRelationHeaderDto> atlasEntityAttributeTermsInGlossaryRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeEntityHeaderRelations(attributes, TERMS_RELATION_ATTRIBUTE);
    }

    @AtlasEntityAttributeGlossaryRelation
    public EntityRelationHeaderDto atlasEntityAttributeGlossaryRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeEntityHeaderRelation(attributes, ATTRIBUTE_ANCHOR);
    }

    @AtlasEntityAttributeTermAssignationsRelation
    public List<EntityRelationHeaderDto> atlasEntityAttributeTermAssignationsRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeEntityHeaderRelations(attributes, ATTRIBUTE_ASSIGNED_ENTITIES);
    }

    @AtlasEntityAttributeTermSeeAlsoRelation
    public List<TermEntityRelationHeaderDto> atlasEntityAttributeTermSeeAlsoRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeTermEntityHeaderRelations(attributes, ATTRIBUTE_SEE_ALSO);
    }

    @AtlasEntityAttributeTermSynonymsRelation
    public List<TermEntityRelationHeaderDto> atlasEntityAttributeTermSynonymsRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeTermEntityHeaderRelations(attributes, ATTRIBUTE_SYNONYMS);
    }

    @AtlasEntityAttributeTermAntonymsRelation
    public List<TermEntityRelationHeaderDto> atlasEntityAttributeTermAntonymsRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeTermEntityHeaderRelations(attributes, ATTRIBUTE_ANTONYMS);
    }

    @AtlasEntityAttributeTermPreferredTermsRelation
    public List<TermEntityRelationHeaderDto> atlasEntityAttributeTermPreferredTermsRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeTermEntityHeaderRelations(attributes, ATTRIBUTE_PREFERRED_TERMS);
    }

    @AtlasEntityAttributeTermPreferredToTermsRelation
    public List<TermEntityRelationHeaderDto> atlasEntityAttributeTermPreferredToTermsRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeTermEntityHeaderRelations(attributes, ATTRIBUTE_PREFERRED_TO_TERMS);
    }

    @AtlasEntityAttributeTermReplacementTermsRelation
    public List<TermEntityRelationHeaderDto> atlasEntityAttributeTermReplacementTermsRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeTermEntityHeaderRelations(attributes, ATTRIBUTE_REPLACEMENT_TERMS);
    }

    @AtlasEntityAttributeTermReplacedByRelation
    public List<TermEntityRelationHeaderDto> atlasEntityAttributeTermReplacedByRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeTermEntityHeaderRelations(attributes, ATTRIBUTE_REPLACED_BY);
    }

    @AtlasEntityAttributeTermTranslationTermsRelation
    public List<TermEntityRelationHeaderDto> atlasEntityAttributeTermTranslationTermsRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeTermEntityHeaderRelations(attributes, ATTRIBUTE_TRANSLATION_TERMS);
    }

    @AtlasEntityAttributeTermTranslatedTermsRelation
    public List<TermEntityRelationHeaderDto> atlasEntityAttributeTermTranslatedTermsRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeTermEntityHeaderRelations(attributes, ATTRIBUTE_TRANSLATED_TERMS);
    }

    @AtlasEntityAttributeTermIsARelation
    public List<TermEntityRelationHeaderDto> atlasEntityAttributeTermIsARelation(Map<String, Object> attributes) {
        return atlasEntityAttributeTermEntityHeaderRelations(attributes, ATTRIBUTE_IS_A);
    }

    @AtlasEntityAttributeTermClassifiesRelation
    public List<TermEntityRelationHeaderDto> atlasEntityAttributeTermClassifiesRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeTermEntityHeaderRelations(attributes, ATTRIBUTE_CLASSIFIES);
    }

    @AtlasEntityAttributeTermValidValuesRelation
    public List<TermEntityRelationHeaderDto> atlasEntityAttributeTermValidValuesRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeTermEntityHeaderRelations(attributes, ATTRIBUTE_VALID_VALUES);
    }

    @AtlasEntityAttributeTermValidValuesForRelation
    public List<TermEntityRelationHeaderDto> atlasEntityAttributeTermValidValuesForRelation(Map<String, Object> attributes) {
        return atlasEntityAttributeTermEntityHeaderRelations(attributes, ATTRIBUTE_VALID_VALUES_FOR);
    }

    @AtlasEntityAttributeExpression
    public String atlasEntityAttributeExpression(Map<String, Object> attributes) {
        var key = ATTRIBUTE_EXPRESSION;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeSteward
    public String atlasEntityAttributeSteward(Map<String, Object> attributes) {
        var key = ATTRIBUTE_STEWARD;
        return getAttribute(attributes, key);
    }

    @AtlasEntityAttributeSource
    public String atlasEntityAttributeSource(Map<String, Object> attributes) {
        var key = ATTRIBUTE_SOURCE;
        return getAttribute(attributes, key);
    }

    public EntityRelationHeaderDto atlasEntityAttributeEntityHeaderRelation(Map<String, Object> attributes, String relation) {
        EntityRelationHeaderDto entityRelationHeaderDto = null;
        if (isAttributeNotNull(attributes, relation)) {
            AtlasRelatedObjectId atlasObjectId = new ObjectMapper().convertValue(attributes.get(relation), new TypeReference<AtlasRelatedObjectId>() {});
            entityRelationHeaderDto = AtlasEntityMapper.MAPPER.fromRelatedObjectId(atlasObjectId);
        }
        return entityRelationHeaderDto;
    }

    public List<EntityRelationHeaderDto> atlasEntityAttributeEntityHeaderRelations(Map<String, Object> attributes, String relation) {
        List<EntityRelationHeaderDto> entityRelationHeaderDtos = new ArrayList<>();
        if (isAttributeNotNull(attributes, relation)) {
            List<AtlasRelatedObjectId> atlasObjectIdList = new ObjectMapper().convertValue(attributes.get(relation), new TypeReference<List<AtlasRelatedObjectId>>() {});
            atlasObjectIdList.stream()
                    .filter(atlasRelatedObjectId -> atlasRelatedObjectId.getRelationshipStatus().equals(AtlasRelationship.Status.ACTIVE))
                    .forEach(atlasObjectId -> entityRelationHeaderDtos.add(
                    AtlasEntityMapper.MAPPER.fromRelatedObjectId(atlasObjectId)));
        }
        return entityRelationHeaderDtos;
    }

    public List<TermEntityRelationHeaderDto> atlasEntityAttributeTermEntityHeaderRelations(Map<String, Object> attributes, String relation) {
        List<TermEntityRelationHeaderDto> entityRelationHeaderDtos = new ArrayList<>();
        if (isAttributeNotNull(attributes, relation)) {
            List<AtlasRelatedObjectId> atlasObjectIdList = new ObjectMapper().convertValue(attributes.get(relation), new TypeReference<List<AtlasRelatedObjectId>>() {});
            atlasObjectIdList.stream()
                    .filter(atlasRelatedObjectId -> atlasRelatedObjectId.getRelationshipStatus().equals(AtlasRelationship.Status.ACTIVE))
                    .forEach(atlasObjectId -> entityRelationHeaderDtos.add(
                            AtlasGlossaryTermMapper.MAPPER.fromRelatedObjectId(atlasObjectId)));
        }
        return entityRelationHeaderDtos;
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeIdentifier {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeTitle {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeEntityName {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeDescription {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeEntityDescription {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeEntitySummary {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeShortDescription {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeLongDescription {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeCreator {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeEntityOwnerName {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeEntityOwner {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeEntityUpdatedBy {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeMediaTypeList {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeMediaType {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeConformsTo {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeChecksum {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeFieldDelimiter {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeDecimalDelimiter {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeAccessUrl {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeByteSize {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeFormat {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeQualifiedName {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeDBType {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeDataType {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributePosition {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeNumRecords {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeIsPrivate {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeSchema {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeUsage {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeAbbreviation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeExamples {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeLanguage {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeMetric {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeValue {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeCreatedInstant {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeCreatedAt {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeUpdatedAt {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeCreatedBy {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeQualityMeasurementParameters {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeCatalogRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeTablesRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeDatabaseRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeColumnsRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeDatasetsRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeDatasetRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeTermsRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeTermsInGlossaryRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeGlossaryRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeTermAssignationsRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeTermSeeAlsoRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeTermSynonymsRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeTermAntonymsRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeTermPreferredTermsRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeTermPreferredToTermsRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeTermReplacementTermsRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeTermReplacedByRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeTermTranslationTermsRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeTermTranslatedTermsRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeTermIsARelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeTermClassifiesRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeTermValidValuesRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeTermValidValuesForRelation {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeExpression {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeSteward {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface AtlasEntityAttributeSource {
    }


}
