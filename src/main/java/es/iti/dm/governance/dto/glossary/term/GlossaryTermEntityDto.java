/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.dto.glossary.term;

import es.iti.dm.governance.dto.abstractmodel.EntityHeaderExtDto;
import es.iti.dm.governance.dto.abstractmodel.EntityRelationHeaderDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@SuperBuilder
@Schema(description = "Glossary term entity schema")
public class GlossaryTermEntityDto extends GlossaryTermCommonEntityDto {
    private UUID id;
    private EntityRelationHeaderDto glossary;
    private List<EntityHeaderExtDto> assignations;
    private Instant createdAt;
    private Instant updatedAt;
    private List<TermEntityRelationHeaderDto> seeAlso;
    private List<TermEntityRelationHeaderDto> synonyms;
    private List<TermEntityRelationHeaderDto> antonyms;
    private List<TermEntityRelationHeaderDto> preferredTerms;
    private List<TermEntityRelationHeaderDto> preferredToTerms;
    private List<TermEntityRelationHeaderDto> replacementTerms;
    private List<TermEntityRelationHeaderDto> replacedBy;
    private List<TermEntityRelationHeaderDto> translationTerms;
    private List<TermEntityRelationHeaderDto> translatedTerms;
    private List<TermEntityRelationHeaderDto> isA;
    private List<TermEntityRelationHeaderDto> classifies;
    private List<TermEntityRelationHeaderDto> validValues;
    private List<TermEntityRelationHeaderDto> validValuesFor;
    private List<String> domains;
}
