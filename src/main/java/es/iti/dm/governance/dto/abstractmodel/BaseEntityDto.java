/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.dto.abstractmodel;

import es.iti.dm.governance.dto.domain.DomainRelationDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@SuperBuilder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Representation of the metadata object in the data catalog")
public class BaseEntityDto extends CreateOrUpdateEntityDto {

    @Schema(description = "Entity identifier")
    private UUID id;
    @Schema(description = "Entity owner identifier")
    private String owner;
    @Schema(description = "Entity owner name")
    private String ownerName;
    @Schema(description = "Entity creation time")
    private Instant createdAt;
    @Schema(description = "Entity last update time")
    private Instant updatedAt;
    @Schema(description = "Entity last update user")
    private String updatedBy;
    @Schema(description = "Entity status - ACTIVE or DELETED")
    private String status;

    @Schema(description = "Terms assigned to the entity")
    private List<EntityRelationHeaderDto> terms;
    @Schema(description = "Domains assigned to the entity")
    private List<DomainRelationDto> domains;
    @Schema(description = "Labels assigned to the entity")
    private Set<String> labels;
}
