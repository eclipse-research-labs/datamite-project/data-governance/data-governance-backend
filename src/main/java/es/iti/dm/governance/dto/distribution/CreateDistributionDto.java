/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.dto.distribution;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateDistributionDto {

    @NotBlank
    @Schema(description = "A name given to the distribution - dcterms:title")
    private String title;
    @Schema(description = "A free-text account of the distribution - dcterms:description")
    private String description;
    @NotBlank
    @Schema(description = "Distribution url of the dataset - dcat:accessUrl")
    private String accessUrl;
    @Schema(description = "Size of a distribution in bytes - dcat:byteSize")
    private Long byteSize;
    @Schema(description = "The media type of the distribution - dcat:mediaType")
    private String mediaType;
    @Schema(description = "The file format of the distribution - dcterms:format")
    private String format;
    @Schema(description = "An established standard to which the distribution conforms - dctemrs:conformsTo")
    private String conformsTo;
    @Schema(description = "The checksum property provides a mechanism that can be used to verify the contents of a file or package have not changed - spdx:checksum")
    private String checksum;
    @Schema(description = "The field delimiter for structured datasets")
    private String fieldDelimiter;
    @Schema(description = "The decimal delimiter for number fields")
    private String decimalDelimiter;

}
