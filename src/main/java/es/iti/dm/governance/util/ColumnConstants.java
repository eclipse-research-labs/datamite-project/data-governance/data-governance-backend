/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.util;

public final class ColumnConstants {

    public static final String COLUMN_API = "column";

    public static final String GET_COLUMN = "getColumn";
    public static final String GET_COLUMN_SUM = "Retrieves the indicated column type entity";
    public static final String GET_COLUMN_DESC = "Retrieves the indicated column type entity";

    public static final String DELETE_COLUMN = "deleteColumn";
    public static final String DELETE_COLUMN_SUM = "Delete the column entity";
    public static final String DELETE_COLUMN_DESC = "Delete the column entity";

    public static final String UPDATE_COLUMN = "updateColumn";
    public static final String UPDATE_COLUMN_SUM = "Update the column entity";
    public static final String UPDATE_COLUMN_DESC = "Update the column entity";

    public static final String CREATE_COLUMN = "createColumn";
    public static final String CREATE_COLUMN_SUM = "Create column entity for the dataset";
    public static final String CREATE_COLUMN_DESC = "Create column entity for the dataset indicated. If the dataset has a column with the same name, updates it instead. Also creates the quality measures for the column";
}
