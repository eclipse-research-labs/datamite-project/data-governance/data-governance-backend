/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.util;

public final class DatabaseConstants {

    public static final String DATABASE_API = "database";

    public static final String CREATE_DATABASE = "createDatabase";
    public static final String CREATE_DATABASE_SUM = "Create a database entity in the catalog";
    public static final String CREATE_DATABASE_DESC = "Create a database entity in the catalog";

    public static final String GET_DATABASE = "getDatabase";
    public static final String GET_DATABASE_SUM = "Retrieves the indicated database type entity";
    public static final String GET_DATABASE_DESC = "Retrieves the indicated database type entity. Includes the identifiers and names of the tables found in the database";

    public static final String UPDATE_DATABASE = "updateDatabase";
    public static final String UPDATE_DATABASE_SUM = "Update the database entity";
    public static final String UPDATE_DATABASE_DESC = "Update the database entity";

    public static final String DELETE_DATABASE = "deleteDatabase";
    public static final String DELETE_DATABASE_SUM = "Delete the database entity";
    public static final String DELETE_DATABASE_DESC = "Delete the database entity. All table entities in the database are also deleted";

    public static final String ADD_TABLES = "addTables";
    public static final String ADD_TABLES_SUM = "Add table entities to the database entity";
    public static final String ADD_TABLES_DESC = "Add table entities to the database entity";

    public static final String GET_TABLES = "getTables";
    public static final String GET_TABLES_SUM = "Retrieves table names with columns and quality";
    public static final String GET_TABLES_DESC = "Retrieves all the table names for the database, the column name of every column in the tables and the quality measures of the columns";
}
