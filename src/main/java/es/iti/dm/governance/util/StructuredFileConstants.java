/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.util;

public final class StructuredFileConstants {

    public static final String STRUCTURED_FILE_API = "structuredFile";

    public static final String CREATE_STRUCTURED_FILE = "createStructuredFile";
    public static final String CREATE_STRUCTURED_FILE_SUM = "Create a structured file entity in the catalog";
    public static final String CREATE_STRUCTURED_FILE_DESC = "Create a structured file entity in the catalog";

    public static final String GET_STRUCTURED_FILE = "getStructuredFile";
    public static final String GET_STRUCTURED_FILE_SUM = "Retrieves the indicated structured file type entity";
    public static final String GET_STRUCTURED_FILE_DESC = "Retrieves the indicated structured file type entity. Includes the identifiers and names of the columns found in the file";

    public static final String UPDATE_STRUCTURED_FILE = "updateStructuredFile";
    public static final String UPDATE_STRUCTURED_FILE_SUM = "Update the structured file entity";
    public static final String UPDATE_STRUCTURED_FILE_DESC = "Update the structured file entity";

    public static final String DELETE_STRUCTURED_FILE = "deleteStructuredFile";
    public static final String DELETE_STRUCTURED_FILE_SUM = "Delete the structured file entity";
    public static final String DELETE_STRUCTURED_FILE_DESC = "Delete the structured file entity. All column entities in the file are also deleted";

    public static final String GET_COLUMNS = "getColumns";
    public static final String GET_COLUMNS_SUM = "Retrieves columns from the file and their quality";
    public static final String GET_COLUMNS_DESC = "Retrieves all the column names from the file and the quality measures of the columns";
}
