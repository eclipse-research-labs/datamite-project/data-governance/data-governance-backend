/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.util;

public final class ErrorMessages {

    public static final String METADATA_REPOSITORY_ERROR = "Metadata repository error";
    public static final String ENTITY_TYPE_MISMATCH_ERROR = "Entity with id %s is not a %s";
    public static final String DISASSOCIATE_TERM_INVALID_INPUT_ERROR = "Id is not from a valid entity or the relation is not correct";
    public static final String DELETE_GLOSSARY_IN_USE_EXCEPTION = "Glossary %s cannot be deleted as it is in use";
    public static final String DELETE_GLOSSARY_TERM_IN_USE_EXCEPTION = "Glossary %s cannot be deleted. Term %s is in use";
    public static final String DELETE_TERM_TERM_RELATION_TYPE_MISMATCH_EXCEPTION = "Relation %s is not a relation between terms";
    private ErrorMessages() {
    }
}
