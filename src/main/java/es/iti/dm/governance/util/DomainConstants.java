/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.util;

public final class DomainConstants {

    public static final String DOMAIN_API = "domains";

    public static final String CREATE_DOMAIN = "createDomain";
    public static final String CREATE_DOMAIN_SUM = "Create a domain";
    public static final String CREATE_DOMAIN_DESC = "Create a domain";

    public static final String GET_DOMAIN = "getDomain";
    public static final String GET_DOMAIN_SUM = "Retrieves the domain";
    public static final String GET_DOMAIN_DESC = "Retrieves the domain";

    public static final String UPDATE_DOMAIN = "updateDomain";
    public static final String UPDATE_DOMAIN_SUM = "Update the domain";
    public static final String UPDATE_DOMAIN_DESC = "Update the domain";

    public static final String DELETE_DOMAIN = "deleteDomain";
    public static final String DELETE_DOMAIN_SUM = "Delete the domain";
    public static final String DELETE_DOMAIN_DESC = "Delete the domain";

    public static final String GET_DOMAINS = "getDomains";
    public static final String GET_DOMAINS_SUM = "Retrieves all the domains";
    public static final String GET_DOMAINS_DESC = "Retrieves all the domains";

    public static final String ADD_DOMAIN = "addDomain";
    public static final String ADD_DOMAIN_SUM = "Add the domain to the entity";
    public static final String ADD_DOMAIN_DESC = "Add the domain to the entity";

    public static final String REMOVE_DOMAIN = "removeDomain";
    public static final String REMOVE_DOMAIN_SUM = "Remove the domain from the entity";
    public static final String REMOVE_DOMAIN_DESC = "Remove the domain from the entity";

    public static final String PROPAGATE_PARAMETER = "Enables domain propagation. " +
            "Terms with a domain that have this option enabled will assign the domain when the term is assigned to an entity. " +
            "Entities with a domain that have this option enabled will assign the domain to entities in the lineage.";

}
