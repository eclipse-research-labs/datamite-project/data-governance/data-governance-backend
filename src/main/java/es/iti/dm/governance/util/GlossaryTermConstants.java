/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.util;

public final class GlossaryTermConstants {

    public static final String TERM_API = "term";

    public static final String CREATE_TERM = "createTerm";
    public static final String CREATE_TERM_SUM = "Create a term entity in the glossary";
    public static final String CREATE_TERM_DESC = "Create a term entity in the glossary";

    public static final String GET_TERM = "getTerm";
    public static final String GET_TERM_SUM = "Retrieves the indicated term type entity";
    public static final String GET_TERM_DESC = "Retrieves the indicated term type entity. Includes the identifiers and names of the entities that use the term";

    public static final String UPDATE_TERM = "updateTerm";
    public static final String UPDATE_TERM_SUM = "Update the term entity";
    public static final String UPDATE_TERM_DESC = "Update the tern entity";

    public static final String DELETE_TERM = "deleteTerm";
    public static final String DELETE_TERM_SUM = "Delete the term entity";
    public static final String DELETE_TERM_DESC = "Delete the term entity. The term does not have to be in use";

    public static final String ASSIGN_TERM = "assignTerm";
    public static final String ASSIGN_TERM_SUM = "Assign term to entity";
    public static final String ASSIGN_TERM_DESC = "Assign term to entity";

    public static final String DISASSOCIATE_TERM = "disassociateTerm";
    public static final String DISASSOCIATE_TERM_SUM = "Remove term from entities";
    public static final String DISASSOCIATE_TERM_DESC = "Remove term from assigned entities by their relation";

    public static final String CREATE_TERM_RELATION = "createTermRelation";
    public static final String CREATE_TERM_RELATION_SUM = "Create a relation between two terms";
    public static final String CREATE_TERM_RELATION_DESC = "Create a relation between two terms";

    public static final String DELETE_TERM_RELATION = "deleteTermRelation";
    public static final String DELETE_TERM_RELATION_SUM = "Delete a relation between two terms";
    public static final String DELETE_TERM_RELATION_DESC = "Delete a relation between two terms";

    public static final String PARAM_ASSIGNMENTS_LIMIT = "Page size for assignments";
    public static final String PARAM_ASSIGNMENTS_OFFSET = "Offset of pagination purpose in assignments";
    public static final String DEFAULT_LIMIT = "25";
    public static final int DEFAULT_LIMIT_VALUE = 25;
    public static final String DEFAULT_OFFSET = "0";
    public static final int DEFAULT_OFFSET_VALUE = 0;
}
