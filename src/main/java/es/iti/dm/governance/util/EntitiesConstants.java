/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.util;

public final class EntitiesConstants {

    private EntitiesConstants() {}

    //ENTITIES
    public static final String COMMON_ENTITY_TYPE = "Common";
    public static final String STRUCTURED_DATASET_ENTITY_TYPE = "StructuredDataset";
    public static final String DISTRIBUTION_ENTITY = "Distribution";
    public static final String COLUMN_ENTITY_TYPE = "DatasetColumn";
    public static final String DATASET_ENTITY_TYPE = "DcatDataset";
    public static final String CATALOG_ENTITY_TYPE = "Catalog";
    public static final String QUALITY_MEASUREMENT_ENTITY_TYPE = "QualityMeasurement";
    public static final String DATABASE_ENTITY_TYPE = "DatasetDatabase";
    public static final String TABLE_ENTITY_TYPE = "DatasetTable";
    public static final String GLOSSARY = "Glossary";
    public static final String GLOSSARY_TERM = "GlossaryTerm";
    public static final String ATLAS_GLOSSARY_TERM = "AtlasGlossaryTerm";
    public static final String ATLAS_GLOSSARY = "AtlasGlossary";

    //ENTITY TYPE ATTRIBUTES
    public static final String ENTITY_NAME_ATTRIBUTE = "name";
    public static final String ENTITY_DESCRIPTION_ATTRIBUTE = "userDescription";
    public static final String ENTITY_SUMMARY_ATTRIBUTE = "entitySummary";
    public static final String ENTITY_OWNER_ATTRIBUTE = "entityOwner";
    public static final String ENTITY_OWNER_NAME_ATTRIBUTE = "entityOwnerName";
    public static final String ENTITY_UPDATED_BY_ATTRIBUTE = "entityUpdatedBy";

    //COMMON TYPE ATTRIBUTES
    public static final String IDENTIFIER_ATTRIBUTE = "identifier";
    public static final String TITLE_ATTRIBUTE = "displayName";
    public static final String DESCRIPTION_ATTRIBUTE = "description";
    public static final String CREATOR_ATTRIBUTE = "owner";
    public static final String CREATED_ATTRIBUTE = "created";

    //CATALOG TYPE ATTRIBUTES
    public static final String VISIBILITY_ATTRIBUTE = "visibility";

    //DISTRIBUTION TYPE ATTRIBUTES
    public static final String ACCESS_URL_ATTRIBUTE = "accessUrl";
    public static final String BYTE_SIZE_ATTRIBUTE = "byteSize";
    public static final String MEDIA_TYPE_ATTRIBUTE = "mediaType";
    public static final String FORMAT_ATTRIBUTE = "format";
    public static final String CONFORMS_TO_ATTRIBUTE = "conformsTo";
    public static final String CHECKSUM_ATTRIBUTE = "checksum";
    public static final String FIELD_DELIMITER_ATTRIBUTE = "fieldDelimiter";
    public static final String DECIMAL_DELIMITER_ATTRIBUTE = "decimalDelimiter";

    //DATABASE TYPE ATTRIBUTES
    public static final String DB_TYPE_ATTRIBUTE = "dbType";

    //TABLE TYPE ATTRIBUTES
    public static final String SCHEMA_ATTRIBUTE = "dbSchema";

    //COLUMN TYPE ATTRIBUTES
    public static final String DATA_TYPE_ATTRIBUTE = "dataType";
    public static final String POSITION_ATTRIBUTE = "position";
    public static final String NUM_RECORDS_ATTRIBUTE = "numRecords";
    public static final String IS_PRIVATE_ATTRIBUTE = "isPrivate";

    //QUALITY MEASUREMENT TYPE ATTRIBUTES
    public static final String VALUE_ATTRIBUTE = "value";
    public static final String CREATED_BY_ATTRIBUTE = "createdBy";
    public static final String PARAMETERS_ATTRIBUTE = "parameters";
    public static final String METRIC_ATTRIBUTE = "metric";

    //RELATIONSHIP ATTRIBUTES
    public static final String CATALOG_RELATION_ATTRIBUTE = "catalog";
    public static final String DATASETS_RELATION_ATTRIBUTE = "datasets";
    public static final String DATASET_RELATION_ATTRIBUTE = "dataset";
    public static final String DATABASE_RELATION_ATTRIBUTE = "database";
    public static final String TABLES_RELATION_ATTRIBUTE = "tables";

    public static final String COLUMNS_RELATION_ATTRIBUTE = "columns";
    public static final String COLUMN_RELATION_ATTRIBUTE = "column";
    public static final String TERMS_RELATION_ATTRIBUTE = "terms";
    public static final String DISTRIBUTIONS_RELATION_ATTRIBUTE = "distributions";
    public static final String QUALITY_MEASUREMENTS_RELATION_ATTRIBUTE = "qualityMeasurements";
}
