/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.util;

public final class GlossaryConstants {

    public static final String GLOSSARY_API = "glossary";

    public static final String CREATE_GLOSSARY = "createGlossary";
    public static final String CREATE_GLOSSARY_SUM = "Create a glossary entity in the catalog";
    public static final String CREATE_GLOSSARY_DESC = "Create a glossary entity in the catalog";

    public static final String GET_GLOSSARY = "getGlossary";
    public static final String GET_GLOSSARY_SUM = "Retrieves the indicated glossary type entity";
    public static final String GET_GLOSSARY_DESC = "Retrieves the indicated glossary type entity. Includes the identifiers and names of the terms found in the glossary";

    public static final String UPDATE_GLOSSARY = "updateGlossary";
    public static final String UPDATE_GLOSSARY_SUM = "Update the glossary entity";
    public static final String UPDATE_GLOSSARY_DESC = "Update the glossary entity";

    public static final String DELETE_GLOSSARY = "deleteGlossary";
    public static final String DELETE_GLOSSARY_SUM = "Delete the glossary entity";
    public static final String DELETE_GLOSSARY_DESC = "Delete the glossary entity. All terms entities in the catalog are also deleted";

    public static final String GET_GLOSSARIES = "getGlossaries";
    public static final String GET_GLOSSARIES_SUM = "Retrieves all the glossaries";
    public static final String GET_GLOSSARIES_DESC = "Retrieves all the glossaries. The result is paginated and can be sorted by glossary attributes";

    public static final String PARAM_LIMIT = "Page size - by default there is no paging";
    public static final String PARAM_OFFSET = "Offset of pagination purpose";
    public static final String PARAM_ORDER_BY = "Attribute to sort by";
}
