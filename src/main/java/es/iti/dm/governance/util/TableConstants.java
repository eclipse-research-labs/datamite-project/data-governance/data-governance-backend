/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.util;

public final class TableConstants {

    public static final String TABLE_API = "table";

    public static final String GET_TABLE = "getTable";
    public static final String GET_TABLE_SUM = "Retrieves the indicated table type entity";
    public static final String GET_TABLE_DESC = "Retrieves the indicated table type entity. Includes the identifiers and names of the columns found in the table";

    public static final String DELETE_TABLE = "deleteTable";
    public static final String DELETE_TABLE_SUM = "Delete the table entity";
    public static final String DELETE_TABLE_DESC = "Delete the table entity. All Column entities in the table are also deleted";

    public static final String UPDATE_TABLE = "updateTable";
    public static final String UPDATE_TABLE_SUM = "Update the table entity";
    public static final String UPDATE_TABLE_DESC = "Update the table entity";

    public static final String GET_COLUMNS = "getColumns";
    public static final String GET_COLUMNS_SUM = "Retrieves columns from the table and their quality";
    public static final String GET_COLUMNS_DESC = "Retrieves all the column names from the table and the quality measures of the columns";
}
