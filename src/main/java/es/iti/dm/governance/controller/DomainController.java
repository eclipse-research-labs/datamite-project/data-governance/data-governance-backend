/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.controller;

import es.iti.dm.governance.dto.domain.CreateDomainDto;
import es.iti.dm.governance.dto.domain.DomainDto;
import es.iti.dm.governance.dto.domain.UpdateDomainDto;
import es.iti.dm.governance.service.DomainService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static es.iti.dm.governance.util.DomainConstants.*;
import static es.iti.dm.governance.util.GlossaryTermConstants.*;

@Tag(name = DOMAIN_API)
@RestController
@RequestMapping(value = "/domains", produces = {"application/json"})
public class DomainController {

    @Autowired
    private DomainService domainService;

    @Operation(summary = CREATE_DOMAIN_SUM,
            tags = {DOMAIN_API},
            operationId = CREATE_DOMAIN,
            description = CREATE_DOMAIN_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = DomainDto.class)))})
    @PostMapping
    public ResponseEntity<DomainDto> createDomain(
            @Valid @RequestBody(
                    content = @Content(
                            schema = @Schema(implementation = CreateDomainDto.class))) @org.springframework.web.bind.annotation.RequestBody CreateDomainDto createDomainDto){
        return new ResponseEntity<>(domainService.create(createDomainDto), HttpStatus.ACCEPTED);
    }

    @Operation(summary = GET_DOMAIN_SUM,
            tags = {DOMAIN_API},
            operationId = GET_DOMAIN,
            description = GET_DOMAIN_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = DomainDto.class)))})
    @GetMapping("/{domainName}")
    public ResponseEntity<DomainDto> getDomain(
            @Parameter(description = PARAM_ASSIGNMENTS_LIMIT) @RequestParam(required = false, defaultValue = DEFAULT_LIMIT) int limit,
            @Parameter(description = PARAM_ASSIGNMENTS_OFFSET) @RequestParam(required = false, defaultValue = DEFAULT_OFFSET) int offset,
            @Parameter @PathVariable String domainName){
        return new ResponseEntity<>(domainService.get(domainName, limit, offset), HttpStatus.ACCEPTED);
    }

    @Operation(summary = UPDATE_DOMAIN_SUM,
            tags = {DOMAIN_API},
            operationId = UPDATE_DOMAIN,
            description = UPDATE_DOMAIN_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = DomainDto.class)))})
    @PutMapping("/{domainName}")
    public ResponseEntity<DomainDto> updateDomain(
            @Parameter @PathVariable String domainName,
            @Valid @RequestBody(
                    content = @Content(
                            schema = @Schema(implementation = UpdateDomainDto.class))) @org.springframework.web.bind.annotation.RequestBody UpdateDomainDto updateDomainDto){
        return new ResponseEntity<>(domainService.update(updateDomainDto, domainName), HttpStatus.ACCEPTED);
    }

    @Operation(summary = DELETE_DOMAIN_SUM,
            tags = {DOMAIN_API},
            operationId = DELETE_DOMAIN,
            description = DELETE_DOMAIN_DESC,
            responses = {
                    @ApiResponse(responseCode = "200")})
    @DeleteMapping("/{domainName}")
    public ResponseEntity<Void> deleteDomain(
            @Parameter @PathVariable String domainName){
        domainService.delete(domainName);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = GET_DOMAINS_SUM,
            tags = {DOMAIN_API},
            operationId = GET_DOMAINS,
            description = GET_DOMAINS_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    array = @ArraySchema(
                                        schema = @Schema(implementation = DomainDto.class))))})
    @GetMapping
    public ResponseEntity<List<DomainDto>> getDomains(){
        return new ResponseEntity<>(domainService.getAll(), HttpStatus.OK);
    }

    @Operation(summary = ADD_DOMAIN_SUM,
            tags = {DOMAIN_API},
            operationId = ADD_DOMAIN,
            description = ADD_DOMAIN_DESC,
            responses = {
                    @ApiResponse(responseCode = "200")})
    @PostMapping("/{domainName}/entity/{entityId}")
    public ResponseEntity<Void> addDomain(
            @Parameter @PathVariable String domainName,
            @Parameter @PathVariable UUID entityId,
            @Parameter(description = PROPAGATE_PARAMETER) @RequestParam(required = false, defaultValue = "false") Boolean propagate){
        domainService.add(domainName, entityId, propagate);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = REMOVE_DOMAIN_SUM,
            tags = {DOMAIN_API},
            operationId = REMOVE_DOMAIN,
            description = REMOVE_DOMAIN_DESC,
            responses = {
                    @ApiResponse(responseCode = "200")})
    @DeleteMapping("/{domainName}/entity/{entityId}")
    public ResponseEntity<Void> removeDomain(
            @Parameter @PathVariable String domainName,
            @Parameter @PathVariable UUID entityId){
        domainService.remove(domainName, entityId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
