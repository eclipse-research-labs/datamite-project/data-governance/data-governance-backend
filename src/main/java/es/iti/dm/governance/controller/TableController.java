/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.controller;

import es.iti.dm.governance.dto.abstractmodel.CreateOrUpdateEntityDto;
import es.iti.dm.governance.dto.column.ColumnSimplDto;
import es.iti.dm.governance.dto.table.TableEntityDto;
import es.iti.dm.governance.service.TableService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static es.iti.dm.governance.util.TableConstants.*;


@Tag(name = TABLE_API)
@RestController
@RequestMapping(value = "/tables", produces = {"application/json"})
public class TableController {

    @Autowired
    private TableService tableService;

    @Operation(summary = GET_TABLE_SUM,
            tags = {TABLE_API},
            operationId = GET_TABLE,
            description = GET_TABLE_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = TableEntityDto.class)))})
    @GetMapping("/{tableId}")
    public ResponseEntity<TableEntityDto> getTable(
            @Parameter @PathVariable UUID tableId){
        return new ResponseEntity<>(tableService.get(tableId), HttpStatus.ACCEPTED);
    }

    @Operation(summary = DELETE_TABLE_SUM,
            tags = {TABLE_API},
            operationId = DELETE_TABLE,
            description = DELETE_TABLE_DESC,
            responses = {
                    @ApiResponse(responseCode = "200")})
    @DeleteMapping("/{tableId}")
    public ResponseEntity<Void> deleteTable(
            @Parameter @PathVariable UUID tableId){
        tableService.delete(tableId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = UPDATE_TABLE_SUM,
            tags = {TABLE_API},
            operationId = UPDATE_TABLE,
            description = UPDATE_TABLE_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = TableEntityDto.class)))})
    @PutMapping("/{tableId}")
    public ResponseEntity<TableEntityDto> updateTable(
            @Parameter @PathVariable UUID tableId,
            @Valid @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    content = @Content(
                            schema = @Schema(implementation = CreateOrUpdateEntityDto.class))) @org.springframework.web.bind.annotation.RequestBody CreateOrUpdateEntityDto createOrUpdateEntityDto){
        return new ResponseEntity<>(tableService.update(createOrUpdateEntityDto, tableId), HttpStatus.ACCEPTED);
    }

    @Operation(summary = GET_COLUMNS_SUM,
            tags = {TABLE_API},
            operationId = GET_COLUMNS,
            description = GET_COLUMNS_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    array = @ArraySchema(
                                            schema = @Schema(implementation = ColumnSimplDto.class))))})
    @GetMapping("/{tableId}/columns")
    public ResponseEntity<List<ColumnSimplDto>> getColumns(
            @Parameter @PathVariable UUID tableId){
        return new ResponseEntity<>(tableService.getColumns(tableId), HttpStatus.ACCEPTED);
    }
}
