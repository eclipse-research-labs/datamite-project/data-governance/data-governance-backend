/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.controller;

import es.iti.dm.governance.dto.abstractmodel.CreateOrUpdateEntityDto;
import es.iti.dm.governance.dto.column.ColumnSimplDto;
import es.iti.dm.governance.dto.structureddataset.CreateStructuredDatasetEntityDto;
import es.iti.dm.governance.dto.structureddataset.StructuredDatasetEntityDto;
import es.iti.dm.governance.service.StructuredDatasetService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static es.iti.dm.governance.util.StructuredFileConstants.*;

@Tag(name = STRUCTURED_FILE_API)
@RestController
@RequestMapping(value = "/structuredFiles", produces = {"application/json"})
public class StructuredFileController {

    @Autowired
    private StructuredDatasetService structuredDatasetService;

    @Operation(summary = CREATE_STRUCTURED_FILE_SUM,
            tags = {STRUCTURED_FILE_API},
            operationId = CREATE_STRUCTURED_FILE,
            description = CREATE_STRUCTURED_FILE_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = StructuredDatasetEntityDto.class)))})
    @PostMapping
    public ResponseEntity<StructuredDatasetEntityDto> createStructuredFile(
            @Valid @RequestBody(
                    content = @Content(
                            schema = @Schema(implementation = CreateStructuredDatasetEntityDto.class))) @org.springframework.web.bind.annotation.RequestBody CreateStructuredDatasetEntityDto createStructuredDatasetEntityDto){
        return new ResponseEntity<>(structuredDatasetService.create(createStructuredDatasetEntityDto), HttpStatus.ACCEPTED);
    }

    @Operation(summary = GET_STRUCTURED_FILE_SUM,
            tags = {STRUCTURED_FILE_API},
            operationId = GET_STRUCTURED_FILE,
            description = GET_STRUCTURED_FILE_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = StructuredDatasetEntityDto.class)))})
    @GetMapping("/{structuredFileId}")
    public ResponseEntity<StructuredDatasetEntityDto> getStructuredFile(
            @Parameter @PathVariable UUID structuredFileId){
        return new ResponseEntity<>(structuredDatasetService.get(structuredFileId), HttpStatus.ACCEPTED);
    }

    @Operation(summary = UPDATE_STRUCTURED_FILE_SUM,
            tags = {STRUCTURED_FILE_API},
            operationId = UPDATE_STRUCTURED_FILE,
            description = UPDATE_STRUCTURED_FILE_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = StructuredDatasetEntityDto.class)))})
    @PutMapping("/{structuredFileId}")
    public ResponseEntity<StructuredDatasetEntityDto> updateStructuredFile(
            @Parameter @PathVariable UUID structuredFileId,
            @Valid @RequestBody(
                    content = @Content(
                            schema = @Schema(implementation = CreateOrUpdateEntityDto.class))) @org.springframework.web.bind.annotation.RequestBody CreateOrUpdateEntityDto createOrUpdateEntityDto){
        return new ResponseEntity<>(structuredDatasetService.update(createOrUpdateEntityDto, structuredFileId), HttpStatus.ACCEPTED);
    }

    @Operation(summary = DELETE_STRUCTURED_FILE_SUM,
            tags = {STRUCTURED_FILE_API},
            operationId = DELETE_STRUCTURED_FILE,
            description = DELETE_STRUCTURED_FILE_DESC,
            responses = {
                    @ApiResponse(responseCode = "200")})
    @DeleteMapping("/{structuredFileId}")
    public ResponseEntity<Void> deleteStructuredFile(
            @Parameter @PathVariable UUID structuredFileId){
        structuredDatasetService.delete(structuredFileId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = GET_COLUMNS_SUM,
            tags = {STRUCTURED_FILE_API},
            operationId = GET_COLUMNS,
            description = GET_COLUMNS_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    array = @ArraySchema(
                                            schema = @Schema(implementation = ColumnSimplDto.class))))})
    @GetMapping("/{structuredFileId}/columns")
    public ResponseEntity<List<ColumnSimplDto>> getColumns(
            @Parameter @PathVariable UUID structuredFileId){
        return new ResponseEntity<>(structuredDatasetService.getColumns(structuredFileId), HttpStatus.ACCEPTED);
    }
}
