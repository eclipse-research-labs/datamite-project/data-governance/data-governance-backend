/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.controller;

import es.iti.dm.governance.dto.abstractmodel.CreateOrUpdateEntityDto;
import es.iti.dm.governance.dto.catalog.CatalogEntityDto;
import es.iti.dm.governance.dto.catalog.CreateCatalogEntityDto;
import es.iti.dm.governance.service.CatalogService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static es.iti.dm.governance.util.CatalogConstants.*;

@Tag(name = CATALOG_API)
@RestController
@RequestMapping(value = "/catalogs", produces = {"application/json"})
public class CatalogController {

    @Autowired
    private CatalogService catalogService;

    @Operation(summary = CREATE_CATALOG_SUM,
            tags = {CATALOG_API},
            operationId = CREATE_CATALOG,
            description = CREATE_CATALOG_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = CatalogEntityDto.class)))})
    @PostMapping
    public ResponseEntity<CatalogEntityDto> createCatalog(
            @Valid @RequestBody(
                    content = @Content(
                            schema = @Schema(implementation = CreateCatalogEntityDto.class))) @org.springframework.web.bind.annotation.RequestBody CreateCatalogEntityDto createCatalogEntityDto){
        return new ResponseEntity<>(catalogService.create(createCatalogEntityDto), HttpStatus.ACCEPTED);
    }

    @Operation(summary = GET_CATALOG_SUM,
            tags = {CATALOG_API},
            operationId = GET_CATALOG,
            description = GET_CATALOG_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = CatalogEntityDto.class)))})
    @GetMapping("/{catalogId}")
    public ResponseEntity<CatalogEntityDto> getCatalog(
            @Parameter @PathVariable UUID catalogId){
        return new ResponseEntity<>(catalogService.get(catalogId), HttpStatus.ACCEPTED);
    }

    @Operation(summary = UPDATE_CATALOG_SUM,
            tags = {CATALOG_API},
            operationId = UPDATE_CATALOG,
            description = UPDATE_CATALOG_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = CatalogEntityDto.class)))})
    @PutMapping("/{catalogId}")
    public ResponseEntity<CatalogEntityDto> updateCatalog(
            @Parameter @PathVariable UUID catalogId,
            @Valid @RequestBody(
                    content = @Content(
                            schema = @Schema(implementation = CreateOrUpdateEntityDto.class))) @org.springframework.web.bind.annotation.RequestBody CreateOrUpdateEntityDto createOrUpdateEntityDto){
        return new ResponseEntity<>(catalogService.update(createOrUpdateEntityDto, catalogId), HttpStatus.ACCEPTED);
    }

    @Operation(summary = DELETE_CATALOG_SUM,
            tags = {CATALOG_API},
            operationId = DELETE_CATALOG,
            description = DELETE_CATALOG_DESC,
            responses = {
                    @ApiResponse(responseCode = "200")})
    @DeleteMapping("/{catalogId}")
    public ResponseEntity<Void> deleteCatalog(
            @Parameter @PathVariable UUID catalogId){
        catalogService.delete(catalogId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
