/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.controller;

import es.iti.dm.governance.dto.abstractmodel.CreateOrUpdateEntityDto;
import es.iti.dm.governance.dto.column.ColumnEntityDto;
import es.iti.dm.governance.dto.column.CreateColumnEntityDto;
import es.iti.dm.governance.service.ColumnService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static es.iti.dm.governance.util.ColumnConstants.*;


@Tag(name = COLUMN_API)
@RestController
@RequestMapping(value = "/columns", produces = {"application/json"})
public class ColumnController {

    @Autowired
    private ColumnService columnService;

    @Operation(summary = GET_COLUMN_SUM,
            tags = {COLUMN_API},
            operationId = GET_COLUMN,
            description = GET_COLUMN_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = ColumnEntityDto.class)))})
    @GetMapping("/{columnId}")
    public ResponseEntity<ColumnEntityDto> getColumn(
            @Parameter @PathVariable UUID columnId){
        return new ResponseEntity<>(columnService.get(columnId), HttpStatus.ACCEPTED);
    }

    @Operation(summary = DELETE_COLUMN_SUM,
            tags = {COLUMN_API},
            operationId = DELETE_COLUMN,
            description = DELETE_COLUMN_DESC,
            responses = {
                    @ApiResponse(responseCode = "200")})
    @DeleteMapping("/{columnId}")
    public ResponseEntity<Void> deleteColumn(
            @Parameter @PathVariable UUID columnId){
        columnService.delete(columnId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = UPDATE_COLUMN_SUM,
            tags = {COLUMN_API},
            operationId = UPDATE_COLUMN,
            description = UPDATE_COLUMN_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = ColumnEntityDto.class)))})
    @PutMapping("/{columnId}")
    public ResponseEntity<ColumnEntityDto> updateColumn(
            @Parameter @PathVariable UUID columnId,
            @Valid @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    content = @Content(
                            schema = @Schema(implementation = CreateOrUpdateEntityDto.class))) @org.springframework.web.bind.annotation.RequestBody CreateOrUpdateEntityDto createOrUpdateEntityDto){
        return new ResponseEntity<>(columnService.update(createOrUpdateEntityDto, columnId), HttpStatus.ACCEPTED);
    }

    @Operation(summary = CREATE_COLUMN_SUM,
            tags = {COLUMN_API},
            operationId = CREATE_COLUMN,
            description = CREATE_COLUMN_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = ColumnEntityDto.class)))})
    @PostMapping
    public ResponseEntity<ColumnEntityDto> createColumn(
            @Valid @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    content = @Content(
                            schema = @Schema(implementation = CreateColumnEntityDto.class))) @org.springframework.web.bind.annotation.RequestBody CreateColumnEntityDto createColumnEntityDto){
        return new ResponseEntity<>(columnService.create(createColumnEntityDto), HttpStatus.ACCEPTED);
    }
}
