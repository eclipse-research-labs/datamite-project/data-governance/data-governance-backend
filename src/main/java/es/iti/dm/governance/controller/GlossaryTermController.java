/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.controller;

import es.iti.dm.governance.dto.glossary.term.CreateOrUpdateGlossaryTermEntityDto;
import es.iti.dm.governance.dto.glossary.term.CreateTermRelationDto;
import es.iti.dm.governance.dto.glossary.term.DisassociateTermDto;
import es.iti.dm.governance.dto.glossary.term.GlossaryTermEntityDto;
import es.iti.dm.governance.service.GlossaryTermService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static es.iti.dm.governance.util.GlossaryTermConstants.*;

@Tag(name = TERM_API)
@RestController
@RequestMapping(value = "/terms", produces = {"application/json"})
public class GlossaryTermController {

    @Autowired
    private GlossaryTermService termService;

    @Operation(summary = CREATE_TERM_SUM,
            tags = {TERM_API},
            operationId = CREATE_TERM,
            description = CREATE_TERM_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = GlossaryTermEntityDto.class)))})
    @PostMapping
    public ResponseEntity<GlossaryTermEntityDto> createGlossaryTerm(
            @Valid @RequestBody(
                    content = @Content(
                            schema = @Schema(implementation = CreateOrUpdateGlossaryTermEntityDto.class))) @org.springframework.web.bind.annotation.RequestBody CreateOrUpdateGlossaryTermEntityDto createOrUpdateGlossaryTermEntityDto){
        return new ResponseEntity<>(termService.create(createOrUpdateGlossaryTermEntityDto), HttpStatus.ACCEPTED);
    }

    @Operation(summary = GET_TERM_SUM,
            tags = {TERM_API},
            operationId = GET_TERM,
            description = GET_TERM_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = GlossaryTermEntityDto.class)))})
    @GetMapping("/{termId}")
    public ResponseEntity<GlossaryTermEntityDto> getGlossaryTerm(
            @Parameter(description = PARAM_ASSIGNMENTS_LIMIT) @RequestParam(required = false, defaultValue = DEFAULT_LIMIT) int limit,
            @Parameter(description = PARAM_ASSIGNMENTS_OFFSET) @RequestParam(required = false, defaultValue = DEFAULT_OFFSET) int offset,
            @Parameter @PathVariable UUID termId){
        return new ResponseEntity<>(termService.get(termId, limit, offset), HttpStatus.ACCEPTED);
    }

    @Operation(summary = UPDATE_TERM_SUM,
            tags = {TERM_API},
            operationId = UPDATE_TERM,
            description = UPDATE_TERM_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = GlossaryTermEntityDto.class)))})
    @PutMapping("/{termId}")
    public ResponseEntity<GlossaryTermEntityDto> updateGlossaryTerm(
            @Parameter @PathVariable UUID termId,
            @Valid @RequestBody(
                    content = @Content(
                            schema = @Schema(implementation = CreateOrUpdateGlossaryTermEntityDto.class))) @org.springframework.web.bind.annotation.RequestBody CreateOrUpdateGlossaryTermEntityDto createOrUpdateGlossaryTermEntityDto){
        return new ResponseEntity<>(termService.update(createOrUpdateGlossaryTermEntityDto, termId), HttpStatus.ACCEPTED);
    }

    @Operation(summary = DELETE_TERM_SUM,
            tags = {TERM_API},
            operationId = DELETE_TERM,
            description = DELETE_TERM_DESC,
            responses = {
                    @ApiResponse(responseCode = "200")})
    @DeleteMapping("/{termId}")
    public ResponseEntity<Void> deleteGlossaryTerm(
            @Parameter @PathVariable UUID termId){
        termService.delete(termId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = ASSIGN_TERM_SUM,
            tags = {TERM_API},
            operationId = ASSIGN_TERM,
            description = ASSIGN_TERM_DESC,
            responses = {
                    @ApiResponse(responseCode = "200")})
    @PostMapping("/{termId}/entities")
    public ResponseEntity<Void> assignGlossaryTerm(
            @Parameter @PathVariable UUID termId,
            @RequestBody(
                    content = @Content(
                            array = @ArraySchema(
                                    schema = @Schema(implementation = UUID.class)))) @org.springframework.web.bind.annotation.RequestBody List<UUID> entities){
        termService.assign(termId, entities);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = DISASSOCIATE_TERM_SUM,
            tags = {TERM_API},
            operationId = DISASSOCIATE_TERM,
            description = DISASSOCIATE_TERM_DESC,
            responses = {
                    @ApiResponse(responseCode = "200")})
    @PutMapping("/{termId}/entities")
    public ResponseEntity<Void> disassociateGlossaryTerm(
            @Parameter @PathVariable UUID termId,
            @RequestBody(
                    content = @Content(
                            array = @ArraySchema(
                                    schema = @Schema(implementation = DisassociateTermDto.class)))) @Valid @org.springframework.web.bind.annotation.RequestBody List<DisassociateTermDto> entities){
        termService.disassociate(termId, entities);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = CREATE_TERM_RELATION_SUM,
            tags = {TERM_API},
            operationId = CREATE_TERM_RELATION,
            description = CREATE_TERM_RELATION_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = GlossaryTermEntityDto.class)))})
    @PostMapping("/{termId}/relations")
    public ResponseEntity<GlossaryTermEntityDto> createTermRelation(
            @Parameter @PathVariable UUID termId,
            @RequestBody(
                    content = @Content(
                                schema = @Schema(implementation = CreateTermRelationDto.class))) @org.springframework.web.bind.annotation.RequestBody CreateTermRelationDto createTermRelationDto){
        return new ResponseEntity<>(termService.createRelation(termId, createTermRelationDto),HttpStatus.OK);
    }

    @Operation(summary = DELETE_TERM_RELATION,
            tags = {TERM_API},
            operationId = DELETE_TERM_RELATION,
            description = DELETE_TERM_RELATION_DESC,
            responses = {
                    @ApiResponse(responseCode = "200")})
    @DeleteMapping("/relations/{relationId}")
    public ResponseEntity<Void> deleteTermRelation(
            @Parameter @PathVariable UUID relationId){
        termService.deleteRelation(relationId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
