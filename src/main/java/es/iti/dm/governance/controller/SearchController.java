/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.controller;

import es.iti.dm.governance.dto.search.SearchDto;
import es.iti.dm.governance.dto.search.SearchResultDto;
import es.iti.dm.governance.dto.search.TermSearchResultDto;
import es.iti.dm.governance.service.SearchService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static es.iti.dm.governance.util.SearchConstants.*;


@Tag(name = SEARCH_API)
@RestController
@RequestMapping(value = "/search", produces = {"application/json"})
public class SearchController {

    @Autowired
    private SearchService searchService;

    @Operation(summary = POST_SEARCH_SUM,
            tags = {SEARCH_API},
            operationId = POST_SEARCH,
            description = POST_SEARCH_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    array = @ArraySchema(
                                        schema = @Schema(implementation = SearchResultDto.class))))})
    @PostMapping
    public ResponseEntity<List<SearchResultDto>> postSearch(
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = SEARCH_PARAMETERS_BODY_DESC, required = true,
                    content = @Content(
                            schema = @Schema(implementation = SearchDto.class))) @org.springframework.web.bind.annotation.RequestBody @Valid SearchDto searchDto) {
        return new ResponseEntity<>(searchService.post(searchDto), HttpStatus.ACCEPTED);
    }

    @Operation(summary = GET_TERMS_SUM,
            tags = {SEARCH_API},
            operationId = GET_TERMS,
            description = GET_TERMS_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    array = @ArraySchema(
                                            schema = @Schema(implementation = TermSearchResultDto.class))))})
    @GetMapping("/terms/{text}")
    public ResponseEntity<List<TermSearchResultDto>> getTermsByText(
            @Parameter @PathVariable String text
    ) {
        return new ResponseEntity<>(searchService.getTermsByText(text), HttpStatus.ACCEPTED);
    }

}
