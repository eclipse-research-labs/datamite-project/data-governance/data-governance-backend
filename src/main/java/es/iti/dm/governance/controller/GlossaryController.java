/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.controller;

import es.iti.dm.governance.dto.glossary.CreateOrUpdateGlossaryEntityDto;
import es.iti.dm.governance.dto.glossary.GlossaryEntityDto;
import es.iti.dm.governance.service.GlossaryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static es.iti.dm.governance.util.GlossaryConstants.*;

@Tag(name = GLOSSARY_API)
@RestController
@RequestMapping(value = "/glossaries", produces = {"application/json"})
public class GlossaryController {

    @Autowired
    private GlossaryService glossaryService;

    @Operation(summary = CREATE_GLOSSARY_SUM,
            tags = {GLOSSARY_API},
            operationId = CREATE_GLOSSARY,
            description = CREATE_GLOSSARY_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = GlossaryEntityDto.class)))})
    @PostMapping
    public ResponseEntity<GlossaryEntityDto> createGlossary(
            @Valid @RequestBody(
                    content = @Content(
                            schema = @Schema(implementation = CreateOrUpdateGlossaryEntityDto.class))) @org.springframework.web.bind.annotation.RequestBody CreateOrUpdateGlossaryEntityDto createOrUpdateGlossaryEntityDto){
        return new ResponseEntity<>(glossaryService.create(createOrUpdateGlossaryEntityDto), HttpStatus.ACCEPTED);
    }

    @Operation(summary = GET_GLOSSARY_SUM,
            tags = {GLOSSARY_API},
            operationId = GET_GLOSSARY,
            description = GET_GLOSSARY_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = GlossaryEntityDto.class)))})
    @GetMapping("/{glossaryId}")
    public ResponseEntity<GlossaryEntityDto> getGlossary(
            @Parameter @PathVariable UUID glossaryId){
        return new ResponseEntity<>(glossaryService.get(glossaryId), HttpStatus.ACCEPTED);
    }

    @Operation(summary = UPDATE_GLOSSARY_SUM,
            tags = {GLOSSARY_API},
            operationId = UPDATE_GLOSSARY,
            description = UPDATE_GLOSSARY_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    schema = @Schema(implementation = GlossaryEntityDto.class)))})
    @PutMapping("/{glossaryId}")
    public ResponseEntity<GlossaryEntityDto> updateGlossary(
            @Parameter @PathVariable UUID glossaryId,
            @Valid @RequestBody(
                    content = @Content(
                            schema = @Schema(implementation = CreateOrUpdateGlossaryEntityDto.class))) @org.springframework.web.bind.annotation.RequestBody CreateOrUpdateGlossaryEntityDto createOrUpdateGlossaryEntityDto){
        return new ResponseEntity<>(glossaryService.update(createOrUpdateGlossaryEntityDto, glossaryId), HttpStatus.ACCEPTED);
    }

    @Operation(summary = DELETE_GLOSSARY_SUM,
            tags = {GLOSSARY_API},
            operationId = DELETE_GLOSSARY,
            description = DELETE_GLOSSARY_DESC,
            responses = {
                    @ApiResponse(responseCode = "200")})
    @DeleteMapping("/{glossaryId}")
    public ResponseEntity<Void> deleteGlossary(
            @Parameter @PathVariable UUID glossaryId){
        glossaryService.delete(glossaryId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = GET_GLOSSARIES_SUM,
            tags = {GLOSSARY_API},
            operationId = GET_GLOSSARIES,
            description = GET_GLOSSARIES_DESC,
            responses = {
                    @ApiResponse(responseCode = "200",
                            content = @Content(
                                    array = @ArraySchema(
                                        schema = @Schema(implementation = GlossaryEntityDto.class))))})
    @GetMapping
    public ResponseEntity<List<GlossaryEntityDto>> getGlossaries(
            @Parameter(description = PARAM_LIMIT) @RequestParam(required = false, defaultValue = "25") int limit,
            @Parameter(description = PARAM_OFFSET) @RequestParam(required = false, defaultValue = "0") int offset
    ){
        return new ResponseEntity<>(glossaryService.getAll(limit, offset), HttpStatus.ACCEPTED);
    }
}
