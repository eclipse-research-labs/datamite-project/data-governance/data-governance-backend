/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.service;

import es.iti.dm.governance.dto.glossary.term.CreateOrUpdateGlossaryTermEntityDto;
import es.iti.dm.governance.dto.glossary.term.CreateTermRelationDto;
import es.iti.dm.governance.dto.glossary.term.DisassociateTermDto;
import es.iti.dm.governance.dto.glossary.term.GlossaryTermEntityDto;

import java.util.List;
import java.util.UUID;

public interface GlossaryTermService {
    GlossaryTermEntityDto create(CreateOrUpdateGlossaryTermEntityDto createOrUpdateGlossaryTermEntityDto);
    GlossaryTermEntityDto get(UUID glossaryTermId, int limit, int offset);
    GlossaryTermEntityDto update(CreateOrUpdateGlossaryTermEntityDto createOrUpdateGlossaryTermEntityDto, UUID glossaryTermId);
    void delete(UUID glossaryTermId);
    void assign(UUID termId, List<UUID> entities);
    void disassociate(UUID termId, List<DisassociateTermDto> entities);
    GlossaryTermEntityDto createRelation(UUID termId, CreateTermRelationDto createTermRelationDto);
    void deleteRelation(UUID relationId);
}
