/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.service.impl;

import es.iti.dm.governance.dto.abstractmodel.CreateOrUpdateEntityDto;
import es.iti.dm.governance.dto.column.ColumnEntityDto;
import es.iti.dm.governance.dto.column.CreateColumnEntityDto;
import es.iti.dm.governance.integration.atlas.service.AtlasColumnEntityService;
import es.iti.dm.governance.service.ColumnService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@AllArgsConstructor
public class ColumnServiceImpl implements ColumnService {

    private AtlasColumnEntityService atlasColumnEntityService;

    @Override
    public ColumnEntityDto get(UUID columnId) {
        return atlasColumnEntityService.get(columnId);
    }

    @Override
    public void delete(UUID columnId) {
        atlasColumnEntityService.delete(columnId);
    }

    @Override
    public ColumnEntityDto update(CreateOrUpdateEntityDto createOrUpdateEntityDto, UUID columnId) {
        return atlasColumnEntityService.update(createOrUpdateEntityDto, columnId);
    }

    @Override
    public ColumnEntityDto create(CreateColumnEntityDto columnEntityDto) {
        return atlasColumnEntityService.create(columnEntityDto);
    }
}
