/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.service.impl;

import es.iti.dm.governance.dto.abstractmodel.CreateOrUpdateEntityDto;
import es.iti.dm.governance.dto.catalog.CatalogEntityDto;
import es.iti.dm.governance.dto.catalog.CreateCatalogEntityDto;
import es.iti.dm.governance.integration.atlas.service.AtlasCatalogEntityService;
import es.iti.dm.governance.service.CatalogService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@AllArgsConstructor
public class CatalogServiceImpl implements CatalogService {

    private AtlasCatalogEntityService atlasCatalogEntityService;

    @Override
    public CatalogEntityDto create(CreateCatalogEntityDto createCatalogEntityDto) {
        return atlasCatalogEntityService.create(createCatalogEntityDto);
    }

    @Override
    public CatalogEntityDto get(UUID catalogId) {
        return atlasCatalogEntityService.get(catalogId);
    }

    @Override
    public CatalogEntityDto update(CreateOrUpdateEntityDto createOrUpdateEntityDto, UUID catalogId) {
        return atlasCatalogEntityService.update(createOrUpdateEntityDto, catalogId);
    }

    @Override
    public void delete(UUID catalogId) {
        atlasCatalogEntityService.delete(catalogId);
    }
}
