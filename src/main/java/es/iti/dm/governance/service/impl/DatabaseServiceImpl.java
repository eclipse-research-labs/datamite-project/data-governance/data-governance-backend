/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.service.impl;

import es.iti.dm.governance.dto.abstractmodel.CreateOrUpdateEntityDto;
import es.iti.dm.governance.dto.database.CreateDatabaseEntityDto;
import es.iti.dm.governance.dto.database.DatabaseEntityDto;
import es.iti.dm.governance.dto.table.CreateTableEntityDto;
import es.iti.dm.governance.dto.table.TableSimplDto;
import es.iti.dm.governance.integration.atlas.service.AtlasDatabaseEntityService;
import es.iti.dm.governance.integration.atlas.service.AtlasTableEntityService;
import es.iti.dm.governance.service.DatabaseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class DatabaseServiceImpl implements DatabaseService {

    private AtlasDatabaseEntityService atlasDatabaseEntityService;
    private AtlasTableEntityService atlasTableEntityService;

    @Override
    public DatabaseEntityDto create(CreateDatabaseEntityDto createDatabaseEntityDto) {
        return atlasDatabaseEntityService.create(createDatabaseEntityDto);
    }

    @Override
    public DatabaseEntityDto get(UUID databaseId) {
        return atlasDatabaseEntityService.get(databaseId);
    }

    @Override
    public DatabaseEntityDto update(CreateOrUpdateEntityDto createOrUpdateEntityDto, UUID databaseId) {
        return atlasDatabaseEntityService.update(createOrUpdateEntityDto, databaseId);
    }

    @Override
    public void delete(UUID databaseId) {
        atlasDatabaseEntityService.delete(databaseId);
    }

    @Override
    public DatabaseEntityDto addTables(UUID databaseId, List<CreateTableEntityDto> createTableEntityDtoList) {
        atlasTableEntityService.addTables(databaseId, createTableEntityDtoList);
        return get(databaseId);
    }

    @Override
    public List<TableSimplDto> getTables(UUID databaseId) {
        return atlasTableEntityService.getTables(databaseId);
    }

}
