/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.service.impl;

import es.iti.dm.governance.dto.domain.DomainDto;
import es.iti.dm.governance.dto.domain.CreateDomainDto;
import es.iti.dm.governance.dto.domain.UpdateDomainDto;
import es.iti.dm.governance.integration.atlas.service.AtlasTypesService;
import es.iti.dm.governance.service.DomainService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class DomainServiceImpl implements DomainService {

    private AtlasTypesService atlasTypesService;

    @Override
    public DomainDto create(CreateDomainDto createDomainDto) {
        return atlasTypesService.createClassification(createDomainDto);
    }

    @Override
    public DomainDto get(String domainName, int limit, int offset) {
        return atlasTypesService.getClassification(domainName, limit, offset);
    }

    @Override
    public DomainDto update(UpdateDomainDto updateDomainDto, String domainName) {
        return atlasTypesService.updateClassification(updateDomainDto, domainName);
    }

    @Override
    public void delete(String domainName) {
        atlasTypesService.deleteClassification(domainName);
    }

    @Override
    public List<DomainDto> getAll() {
        return atlasTypesService.getAllClassifications();
    }

    @Override
    public void add(String domainName, UUID entityId, Boolean propagate) {
        atlasTypesService.addClassification(domainName, entityId, propagate);
    }

    @Override
    public void remove(String domainName, UUID entityId) {
        atlasTypesService.removeClassification(domainName, entityId);
    }
}
