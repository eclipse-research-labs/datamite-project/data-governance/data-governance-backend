/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.service.impl;

import es.iti.dm.governance.dto.glossary.CreateOrUpdateGlossaryEntityDto;
import es.iti.dm.governance.dto.glossary.GlossaryEntityDto;
import es.iti.dm.governance.integration.atlas.service.AtlasGlossaryService;
import es.iti.dm.governance.service.GlossaryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class GlossaryServiceImpl implements GlossaryService {

    private AtlasGlossaryService atlasGLossaryService;

    @Override
    public GlossaryEntityDto create(CreateOrUpdateGlossaryEntityDto createOrUpdateGlossaryEntityDto) {
        return atlasGLossaryService.create(createOrUpdateGlossaryEntityDto);
    }

    @Override
    public GlossaryEntityDto get(UUID glossaryId) {
        return atlasGLossaryService.get(glossaryId);
    }

    @Override
    public GlossaryEntityDto update(CreateOrUpdateGlossaryEntityDto createOrUpdateGlossaryEntityDto, UUID glossaryId) {
        return atlasGLossaryService.update(createOrUpdateGlossaryEntityDto, glossaryId);
    }

    @Override
    public void delete(UUID glossaryId) {
        atlasGLossaryService.delete(glossaryId);
    }

    @Override
    public List<GlossaryEntityDto> getAll(int limit, int offset) {
        return atlasGLossaryService.getAll(limit, offset);
    }
}
