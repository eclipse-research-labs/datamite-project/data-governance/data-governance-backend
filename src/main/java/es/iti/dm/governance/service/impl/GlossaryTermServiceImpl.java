/*
 * MIT License
 * Copyright (c) 2024.  ITI
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package es.iti.dm.governance.service.impl;

import es.iti.dm.governance.dto.glossary.term.CreateOrUpdateGlossaryTermEntityDto;
import es.iti.dm.governance.dto.glossary.term.CreateTermRelationDto;
import es.iti.dm.governance.dto.glossary.term.DisassociateTermDto;
import es.iti.dm.governance.dto.glossary.term.GlossaryTermEntityDto;
import es.iti.dm.governance.integration.atlas.service.AtlasGlossaryTermService;
import es.iti.dm.governance.integration.atlas.service.AtlasRelationsService;
import es.iti.dm.governance.service.GlossaryTermService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static es.iti.dm.governance.util.GlossaryTermConstants.DEFAULT_LIMIT_VALUE;
import static es.iti.dm.governance.util.GlossaryTermConstants.DEFAULT_OFFSET_VALUE;

@Service
@AllArgsConstructor
public class GlossaryTermServiceImpl implements GlossaryTermService {

    private AtlasGlossaryTermService atlasGlossaryTermService;
    private AtlasRelationsService atlasRelationsService;

    @Override
    public GlossaryTermEntityDto create(CreateOrUpdateGlossaryTermEntityDto createOrUpdateGlossaryTermEntityDto) {
        return atlasGlossaryTermService.create(createOrUpdateGlossaryTermEntityDto);
    }

    @Override
    public GlossaryTermEntityDto get(UUID glossaryTermId, int limit, int offset) {
        return atlasGlossaryTermService.get(glossaryTermId, limit, offset);
    }

    @Override
    public GlossaryTermEntityDto update(CreateOrUpdateGlossaryTermEntityDto createOrUpdateGlossaryTermEntityDto, UUID glossaryTermId) {
        return atlasGlossaryTermService.update(createOrUpdateGlossaryTermEntityDto, glossaryTermId);
    }

    @Override
    public void delete(UUID glossaryTermId) {
        atlasGlossaryTermService.delete(glossaryTermId);
    }

    @Override
    public void assign(UUID termId, List<UUID> entities) {
        atlasGlossaryTermService.assign(termId, entities);
    }

    @Override
    public void disassociate(UUID termId, List<DisassociateTermDto> entities) {
        atlasGlossaryTermService.disassociate(termId, entities);
    }

    @Override
    public GlossaryTermEntityDto createRelation(UUID termId, CreateTermRelationDto createTermRelationDto) {
        atlasRelationsService.createTermRelation(termId, createTermRelationDto);
        return get(termId, DEFAULT_LIMIT_VALUE, DEFAULT_OFFSET_VALUE);
    }

    @Override
    public void deleteRelation(UUID relationId) {
        atlasRelationsService.deleteTermRelation(relationId);
    }
}
