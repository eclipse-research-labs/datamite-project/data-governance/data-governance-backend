## CONTRIBUTION GUIDELINES

This repository follow the same rules that you cand find on the [main contribution file](https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/docs/-/blob/main/CONTRIBUTING.md) of DATAMITE project.

Please, **read it carefully before making your contribution**, especially if the purpose of your contribution is to update or provide new code on this repository.

If you have any question, comment or problem, do not hesitate to contact us at:

* Jordi Arjona: jarjona@iti.es (repository's owner)
* Raul Costa: rcosta@iti.es