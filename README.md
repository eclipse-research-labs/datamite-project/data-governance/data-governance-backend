# DESCRIPTION
This project will host the developments for the Data Governance Backend, one of the components of the Data Governance Module, as can be seen in the architecture figure below.

![architecture_DGB](docs/figures/arch_extended_DGbackend.png)

The Data Governance Backend is the interface to the metadata repository and implements the APIs needed to upload, update, delete or retrieve metadata from the metadata repository. Similarly, these APIs are related to the fields that can be seen in the metadata schema from the following figure.

![metadata_schema](docs/figures/metadata_schema.png)

Both figures can be found in the [docs/figures](https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-governance/data-governance-backend/-/tree/main/docs/figures) folder. An overview of the APIs proposed can be found in the file [API Proposal](https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-governance/data-governance-backend/-/blob/main/docs/api-proposal.md).

## INSTALLATION & REQUIREMENTS

The Data Governance Backend will be provided as a container to be executed in Docker/Kubernetes/similar. The main requirement is the existence of a metadata repository to interact with and where to store or retrieve the metadata In principle, the metadata repository is assumed to be Apache Atlas.

## ROADMAP

- [x] Initial version of the metadata schema 
- [x] Initial definition of the API
- [ ] Revision of the metadata schema and APIs
- [ ] Implementation of the APIs
- [ ] Automate the deployment of the Data Governance Backend
- [ ] Integration with metadata repository
- [ ] Integration with Data Security Module

## LICENSE
In principle this component is distributed under MIT license.
