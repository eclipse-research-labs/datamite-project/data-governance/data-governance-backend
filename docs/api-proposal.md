Data Governance Backend API
------------------------------

[[_TOC_]]

# Dataset

## Create dataset
- **Description:** Create a dataset entity to contain artifacts
- **Method:** POST
- **Path:** `/datasets`
- **Body:**
    - name: String
    - displayName: String
    - description: String
- **Response:**
    - id: UUID
    - qualifiedName: String
    - name: String
    - displayName: String
    - description: String
    - owner: UUID
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date

## Update dataset
- **Description:** Update the dataset básic info
- **Method:** PUT
- **Path:** `/datasets/datasetId`
- **Body:**
    - name: String
    - displayName: String
    - description: String
- **Response:**
    - id: UUID
    - qualifiedName: String
    - name: String
    - displayName: String
    - description: String
    - owner: UUID
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date

## Delete dataset
- **Description:** Delete the dataset and all the artifacts that contains
- **Method:** DELETE
- **Path:** `/datasets/datasetId`

## Get dataset
- **Description:** Get the dataset info including artifacts that are visible
- **Method:** GET
- **Path:** `/datasets/datasetId`
- **Response:**
    - id: UUID
    - qualifiedName: String
    - name: String
    - displayName: String
    - description: String
    - owner: UUID
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date
    - terms: List<Term>
    - classifications: List<String>
    - tags: List<String>
    - artifacts: List<Artifact>

## Add terms to the dataset
- **Description:** Add a list of terms to the dataset
- **Method:** POST
- **Path:** `/datasets/datasetId/terms`
- **Body:**
    - List<UUID>

## Remove terms from dataset
- **Description:** Remove a list of terms from the dataset
- **Method:** DELETE
- **Path:** `/datasets/datasetId/terms`
- **Body:**
    - List<UUID>

## Add classifications to the dataset
- **Description:** Add a list of classifications to the dataset
- **Method:** POST
- **Path:** `/datasets/datasetId/classifications`
- **Body:**
    - List<String>

## Remove classifications from dataset
- **Description:** Remove a list of classifications from the dataset
- **Method:** DELETE
- **Path:** `/datasets/datasetId/classifications`
- **Body:**
    - List<String>

## Add tags to the dataset
- **Description:** Add a list of tags to the dataset
- **Method:** POST
- **Path:** `/datasets/datasetId/tags`
- **Body:**
    - List<String>

## Remove tags from dataset
- **Description:** Remove a list of tags from the dataset
- **Method:** DELETE
- **Path:** `/datasets/datasetId/tags`
- **Body:**
    - List<String>

# Database

## Create database
- **Description:** Create a database entity
- **Method:** POST
- **Path:** `/databases`
- **Body:**
    - dataset: UUID
    - name: String
    - displayName: String
    - description: String
    - sourceUrl: String
    - size: int
    - dbType: String
- **Response:**
    - dataset: UUID
    - id: UUID
    - qualifiedName: String
    - name: String
    - displayName: String
    - description: String
    - sourceUrl: String
    - size: int
    - dbType: String
    - owner: UUID
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date

## Update database
- **Description:** Update a database entity
- **Method:** PUT
- **Path:** `/databases/databaseId`
- **Body:**
    - displayName: String
    - description: String
- **Response:**
    - dataset: UUID
    - id: UUID
    - qualifiedName: String
    - name: String
    - displayName: String
    - description: String
    - sourceUrl: String
    - size: int
    - dbType: String
    - owner: UUID
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date

## Delete database
- **Description:** Delete a database entity and all the tables that contains
- **Method:** DELETE
- **Path:** `/databases/databaseId`

## Get database
- **Description:** Get the database info including tables that are visible
- **Method:** GET
- **Path:** `/databases/databaseId`
- **Response:**
    - dataset: UUID
    - id: UUID
    - qualifiedName: String
    - name: String
    - displayName: String
    - description: String
    - sourceUrl: String
    - size: int
    - dbType: String
    - owner: UUID
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date
    - terms: List<Term>
    - classifications: List<String>
    - tags: List<String>
    - tables: List<ArtifactTable>

## Add terms to the database
- **Description:** Add a list of terms to the database
- **Method:** POST
- **Path:** `/databases/databaseId/terms`
- **Body:**
    - List<UUID>

## Remove terms from database
- **Description:** Remove a list of terms from the database
- **Method:** DELETE
- **Path:** `/databases/databaseId/terms`
- **Body:**
    - List<UUID>

## Add classifications to the database
- **Description:** Add a list of classifications to the database
- **Method:** POST
- **Path:** `/databases/databaseId/classifications`
- **Body:**
    - List<String>

## Remove classifications from database
- **Description:** Remove a list of classifications from the database
- **Method:** DELETE
- **Path:** `/databases/databaseId/classifications`
- **Body:**
    - List<String>

## Add tags to the database
- **Description:** Add a list of tags to the database
- **Method:** POST
- **Path:** `/databases/databaseId/tags`
- **Body:**
    - List<String>

## Remove tags from database
- **Description:** Remove a list of tags from the database
- **Method:** DELETE
- **Path:** `/databases/databaseId/tags`
- **Body:**
    - List<String>


# Table

## Create table
- **Description:** Create a table entity
- **Method:** POST
- **Path:** `/tables`
- **Body:**
    - database: UUID
    - name: String
    - displayName: String
    - description: String
    - schema: String
    - isPrivate: boolean
    - encoding: String
- **Response:**
    - database: UUID
    - id: UUID
    - qualifiedName: String
    - name: String
    - displayName: String
    - description: String
    - schema: String
    - isPrivate: boolean
    - encoding: String
    - owner: UUID
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date

## Update table
- **Description:** Update a table entity
- **Method:** PUT
- **Path:** `/tables/tableId`
- **Body:**
    - displayName: String
    - description: String
    - isPrivate: boolean
- **Response:**
    - database: UUID
    - id: UUID
    - qualifiedName: String
    - name: String
    - displayName: String
    - description: String
    - schema: String
    - isPrivate: boolean
    - encoding: String
    - owner: UUID
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date

## Delete table
- **Description:** Delete a table entity and all the columns that contains
- **Method:** DELETE
- **Path:** `/tables/tableId`

## Get table
- **Description:** Get the table info including columns that are visible
- **Method:** GET
- **Path:** `/tables/tableId`
- **Response:**
    - database: UUID
    - id: UUID
    - qualifiedName: String
    - name: String
    - displayName: String
    - description: String
    - schema: String
    - isPrivate: boolean
    - encoding: String
    - owner: UUID
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date
    - terms: List<Term>
    - classifications: List<String>
    - tags: List<String>
    - columns: List<ArtifactColumn>

## Add terms to the table
- **Description:** Add a list of terms to the table
- **Method:** POST
- **Path:** `/tables/tableId/terms`
- **Body:**
    - List<UUID>

## Remove terms from table
- **Description:** Remove a list of terms from the table
- **Method:** DELETE
- **Path:** `/tables/tableId/terms`
- **Body:**
    - List<UUID>

## Add classifications to the table
- **Description:** Add a list of classifications to the table
- **Method:** POST
- **Path:** `/tables/tableId/classifications`
- **Body:**
    - List<String>

## Remove classifications from table
- **Description:** Remove a list of classifications from the table
- **Method:** DELETE
- **Path:** `/tables/tableId/classifications`
- **Body:**
    - List<String>

## Add tags to the table
- **Description:** Add a list of tags to the table
- **Method:** POST
- **Path:** `/tables/tableId/tags`
- **Body:**
    - List<String>

## Remove tags from table
- **Description:** Remove a list of tags from the table
- **Method:** DELETE
- **Path:** `/tables/tableId/tags`
- **Body:**
    - List<String>

# File

## Create file
- **Description:** Create a file entity
- **Method:** POST
- **Path:** `/files`
- **Body:**
    - dataset: UUID
    - name: String
    - displayName: String
    - description: String
    - fileType: String
    - hash: String
    - encoding: String
- **Response:**
    - dataset: UUID
    - id: UUID
    - qualifiedName: String
    - name: String
    - displayName: String
    - description: String
    - fileType: String
    - hash: String
    - encoding: String
    - owner: UUID
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date

## Update file
- **Description:** Update a file entity
- **Method:** PUT
- **Path:** `/files/fileId`
- **Body:**
    - displayName: String
    - description: String
- **Response:**
    - dataset: UUID
    - id: UUID
    - qualifiedName: String
    - name: String
    - displayName: String
    - description: String
    - fileType: String
    - hash: String
    - encoding: String
    - owner: UUID
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date

## Delete file
- **Description:** Delete a file entity and all the columns that contains
- **Method:** DELETE
- **Path:** `/files/fileId`

## Get file
- **Description:** Get the file info including columns that are visible
- **Method:** GET
- **Path:** `/files/fileId`
- **Response:**
    - dataset: UUID
    - id: UUID
    - qualifiedName: String
    - name: String
    - description: String
    - fileType: String
    - hash: String
    - encoding: String
    - owner: UUID
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date
    - terms: List<Term>
    - classifications: List<String>
    - tags: List<String>
    - columns: List<ArtifactColumn>

## Add terms to the file
- **Description:** Add a list of terms to the file
- **Method:** POST
- **Path:** `/files/fileId/terms`
- **Body:**
    - List<UUID>

## Remove terms from file
- **Description:** Remove a list of terms from the file
- **Method:** DELETE
- **Path:** `/files/fileId/terms`
- **Body:**
    - List<UUID>

## Add classifications to the file
- **Description:** Add a list of classifications to the file
- **Method:** POST
- **Path:** `/files/fileId/classifications`
- **Body:**
    - List<String>

## Remove classifications from file
- **Description:** Remove a list of classifications from the file
- **Method:** DELETE
- **Path:** `/files/fileId/classifications`
- **Body:**
    - List<String>

## Add tags to the file
- **Description:** Add a list of tags to the file
- **Method:** POST
- **Path:** `/files/fileId/tags`
- **Body:**
    - List<String>

## Remove tags from file
- **Description:** Remove a list of tags from the file
- **Method:** DELETE
- **Path:** `/files/fileId/tags`
- **Body:**
    - List<String>

# Column

## Create column
- **Description:** Create a column entity
- **Method:** POST
- **Path:** `/columns`
- **Body:**
    - artifact: UUID
    - name: String
    - displayName: String
    - description: String
    - dataType: String
    - position: int
    - numRecords: int
    - isVisible: boolean
- **Response:**
    - artifact: UUID
    - id: UUID
    - qualifiedName: String
    - name: String
    - displayName: String
    - description: String
    - dataType: String
    - position: int
    - numRecords: int
    - isVisible: boolean
    - owner: UUID
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date

## Update column
- **Description:** Update a column entity
- **Method:** PUT
- **Path:** `/columns/columnId`
- **Body:**
    - displayName: String
    - description: String
    - isVisible: boolean
- **Response:**
    - artifact: UUID
    - id: UUID
    - qualifiedName: String
    - name: String
    - displayName: String
    - description: String
    - dataType: String
    - position: int
    - numRecords: int
    - isVisible: boolean
    - owner: UUID
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date

## Delete column
- **Description:** Delete a column entity
- **Method:** DELETE
- **Path:** `/columns/columnId`

## Get column
- **Description:** Get the column if it is visible
- **Method:** GET
- **Path:** `/columns/columnId`
- **Response:**
    - artifact: UUID
    - id: UUID
    - qualifiedName: String
    - name: String
    - displayName: String
    - description: String
    - dataType: String
    - position: int
    - numRecords: int
    - isVisible: boolean
    - owner: UUID
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date
    - terms: List<Term>
    - classifications: List<String>
    - tags: List<String>
    - columns: List<ArtifactColumn>

## Add terms to the column
- **Description:** Add a list of terms to the column
- **Method:** POST
- **Path:** `/columns/columnId/terms`
- **Body:**
    - List<UUID>

## Remove terms from column
- **Description:** Remove a list of terms from the column
- **Method:** DELETE
- **Path:** `/columns/columnId/terms`
- **Body:**
    - List<UUID>

## Add classifications to the column
- **Description:** Add a list of classifications to the column
- **Method:** POST
- **Path:** `/columns/columnId/classifications`
- **Body:**
    - List<String>

## Remove classifications from column
- **Description:** Remove a list of classifications from the column
- **Method:** DELETE
- **Path:** `/columns/columnId/classifications`
- **Body:**
    - List<String>

## Add tags to the column
- **Description:** Add a list of tags to the column
- **Method:** POST
- **Path:** `/columns/columnId/tags`
- **Body:**
    - List<String>

## Remove tags from column
- **Description:** Remove a list of tags from the column
- **Method:** DELETE
- **Path:** `/columns/columnId/tags`
- **Body:**
    - List<String>

# Glossary

## Create glossary
- **Description:** Create a glossary of terms
- **Method:** POST
- **Path:** `/glossaries`
- **Body:**
    - name: String
    - shortDescription: String
    - longDescription: String
    - usage: String
    - language: String
    - isPrivate: boolean
- **Response:**
    - id: UUID
    - qualifiedName: String
    - name: String
    - shortDescription: String
    - longDescription: String
    - usage: String
    - language: String
    - isPrivate: boolean
    - owner: UUID
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date

## Update glossary
- **Description:** Update a glossary of terms
- **Method:** PUT
- **Path:** `/glossaries/glossaryId`
- **Body:**
    - name: String
    - shortDescription: String
    - longDescription: String
    - usage: String
    - language: String
    - isPrivate: boolean
- **Response:**
    - id: UUID
    - qualifiedName: String
    - name: String
    - shortDescription: String
    - longDescription: String
    - usage: String
    - language: String
    - isPrivate: boolean
    - owner: UUID
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date

## Delete glossary
- **Description:** Delete a glossary of terms including the terms that contains
- **Method:** DELETE
- **Path:** `/glossaries/glossaryId`

## Get glossary
- **Description:** Get the glossary of terms including the terms if it is visible
- **Method:** GET
- **Path:** `/glossaries/glossaryId`
- **Response:**
    - id: UUID
    - qualifiedName: String
    - name: String
    - shortDescription: String
    - longDescription: String
    - usage: String
    - language: String
    - isPrivate: boolean
    - owner: UUID
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date
    - terms: List<Term>

## Get all glossaries
- **Description:** Get all the glossaries visibles
- **Method:** GET
- **Path:** `/glossaries`
- **Response:**
    - List<Glossary>

## Import glossary
- **Description:** Create a glossary and terms from a file. The file must comply with the required format
- **Method:** POST
- **Path:** `/glossaries/import`
- **Body:**
    - file: File
- **Response:**
    - id: UUID
    - qualifiedName: String
    - name: String
    - shortDescription: String
    - longDescription: String
    - usage: String
    - language: String
    - isPrivate: boolean
    - owner: UUID
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date
    - terms: List<Term>

# Term

## Create term
- **Description:** create a term in a glossary
- **Method:** POST
- **Path:** `/terms`
- **Body:**
    - glossary: UUID
    - name: String
    - shortDescription: String
    - longDescription: String
    - usage: String
    - examples: List<String>
    - abbreviation: String
    - relatedTerms:
        - seeAlso: List<UUID>
        - synonyms: List<UUID>
        - antonyms: List<UUID>
        - preferredTerms: List<UUID>
        - preferredToTerms: List<UUID>
        - replacementTerms: List<UUID>
        - replacedBy: List<UUID>
        - translationTerms: List<UUID>
        - translatedTerms: List<UUID>
        - isA: List<UUID>
        - classifies: List<UUID>
        - validValues: List<UUID>
        - validValuesFor: List<UUID>
- **Response:**
    - glossary: UUID
    - id: UUID
    - name: String
    - shortDescription: String
    - longDescription: String
    - usage: String
    - examples: List<String>
    - abbreviation: String
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date
    - relatedTerms:
        - seeAlso: List<TermHeader>
        - synonyms: List<TermHeader>
        - antonyms: List<TermHeader>
        - preferredTerms: List<TermHeader>
        - preferredToTerms: List<TermHeader>
        - replacementTerms: List<TermHeader>
        - replacedBy: List<TermHeader>
        - translationTerms: List<TermHeader>
        - translatedTerms: List<TermHeader>
        - isA: List<TermHeader>
        - classifies: List<TermHeader>
        - validValues: List<TermHeader>
        - validValuesFor: List<TermHeader>

## Update term
- **Description:** Update a term
- **Method:** PUT
- **Path:** `/terms/termId`
- **Body:**
    - name: String
    - shortDescription: String
    - longDescription: String
    - usage: String
    - examples: List<String>
    - abbreviation: String
    - relatedTerms:
        - seeAlso: List<UUID>
        - synonyms: List<UUID>
        - antonyms: List<UUID>
        - preferredTerms: List<UUID>
        - preferredToTerms: List<UUID>
        - replacementTerms: List<UUID>
        - replacedBy: List<UUID>
        - translationTerms: List<UUID>
        - translatedTerms: List<UUID>
        - isA: List<UUID>
        - classifies: List<UUID>
        - validValues: List<UUID>
        - validValuesFor: List<UUID>
- **Response:**
    - glossary: UUID
    - id: UUID
    - name: String
    - shortDescription: String
    - longDescription: String
    - usage: String
    - examples: List<String>
    - abbreviation: String
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date
    - relatedTerms:
        - seeAlso: List<TermHeader>
        - synonyms: List<TermHeader>
        - antonyms: List<TermHeader>
        - preferredTerms: List<TermHeader>
        - preferredToTerms: List<TermHeader>
        - replacementTerms: List<TermHeader>
        - replacedBy: List<TermHeader>
        - translationTerms: List<TermHeader>
        - translatedTerms: List<TermHeader>
        - isA: List<TermHeader>
        - classifies: List<TermHeader>
        - validValues: List<TermHeader>
        - validValuesFor: List<TermHeader>

## Delete term
- **Description:** Delete a term that there is not in use
- **Method:** DELETE
- **Path:** `/terms/termId`

## Get term
- **Description:** Get the term including its assignments
- **Method:** GET
- **Path:** `/terms/termId`
- **Response:**
    - glossary: UUID
    - id: UUID
    - name: String
    - shortDescription: String
    - longDescription: String
    - usage: String
    - examples: List<String>
    - abbreviation: String
    - createdAt: Date
    - updatedAt: Date
    - updatedBy: Date
    - relatedTerms
        - seeAlso: List<TermHeader>
        - synonyms: List<TermHeader>
        - antonyms: List<TermHeader>
        - preferredTerms: List<TermHeader>
        - preferredToTerms: List<TermHeader>
        - replacementTerms: List<TermHeader>
        - replacedBy: List<TermHeader>
        - translationTerms: List<TermHeader>
        - translatedTerms: List<TermHeader>
        - isA: List<TermHeader>
        - classiefies: List<TermHeader>
        - validValues: List<TermHeader>
        - validValuesFor: List<TermHeader>
    - assignments: List<EntityHeader>

## Get all terms
- **Description:** Get all terms from glossaries that are visible
- **Method:** GET
- **Path:** `/terms`
- **Response:**
    - List<Term>

## Assign Term
- **Description:** Assign a term to a list of entities
- **Method:** POST
- **Path:** `/terms/termId/assign`
- **Body:**
    - List<UUID>

## Unassign Term
- **Description:** Unassign a term from a list of entities
- **Method:** DELETE
- **Path:** `/terms/termId/assign`
- **Body:**
    - List<UUID>

# Classification

## Create Classification
- **Description:** Create a classification
- **Method:** POST
- **Path:** `/classifications`
- **Body:**
    - name: String
    - description: String
    - parent: String
- **Response:**
    - name: String
    - description: String
    - parent: String

## Update Classification
- **Description:** Update a classification
- **Method:** PUT
- **Path:** `/classifications/classificationName`
- **Body:**
    - name: String
    - description: String
    - parent: String
- **Response:**
    - name: String
    - description: String
    - parent: String

## Delete Classification
- **Description:** Delete a classification and the subclassifications
- **Method:** DELETE
- **Path:** `/classifications/classificationName`

## Get Classification
- **Description:** Get the classification including subclassifications and the assignments
- **Method:** GET
- **Path:** `/classifications/classificationName`
- **Response:**
    - name: String
    - description: String
    - parent: String
    - subclassifications: List<String>
    - assignments: List<EntityHeader>

## Get All Classifications
- **Description:** Get all the classifications
- **Method:** GET
- **Path:** `/classifications`
- **Response:** List<Classification>

## Assign Classification
- **Description:** Assign a classification to a list of entities
- **Method:** POST
- **Path:** `/classifications/classificationName/assign`
- **Body:** List<UUID>

## Unassign Classification
- **Description:** Unassign a classification from a list of entities
- **Method:** DELETE
- **Path:** `/classifications/classificationName/assign`
- **Body:** List<UUID>

# Search
- **Description:** Search entities by the criteria
- **Method:** POST
- **Path:** `/search`
- **Body:**
    - type: String (including the option to all types)
    - term: UUID
    - classification: String
    - text: String
    - limit: int
    - offset: int
    - orderBy: string
- **Response:**
    - numResults: int
    - datasets: List<DatasetHeader>
    - databases: List<DatabaseHeader>
    - tables: List<TableHeader>
    - files: List<FileHeader>
    - columns: List<ColumnHeader>
