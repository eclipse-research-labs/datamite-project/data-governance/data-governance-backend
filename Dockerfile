FROM maven:3-eclipse-temurin-17 AS dep

# Directory containing compiled JAR. Default value can be overwritten in Settings > CI/CD > Variables
ARG MAVEN_TARGET=target 
 # "--batch-mode" => Do not prompt for user input
ARG MAVEN_CLI_OPTS="-s .m2/settings.xml --batch-mode"
ARG MAVEN_OPTS="-Dmaven.repo.local=.m2/repository"

COPY . ./

RUN mvn ${MAVEN_CLI_OPTS} clean package

FROM eclipse-temurin:17-jre-alpine

RUN apk add dumb-init curl

ARG PORT=8091
ENV PORT=${PORT}
EXPOSE ${PORT}
HEALTHCHECK --interval=20s --timeout=5s CMD curl localhost:${PORT} || exit 1

RUN mkdir /app
WORKDIR /app
COPY --from=dep /target/*.jar java-application.jar

ARG DEFAULT_USER=iti-dm
RUN addgroup --system ${DEFAULT_USER} && adduser -S -h ${PWD} -s /bin/sh -G ${DEFAULT_USER} ${DEFAULT_USER}
RUN chown -R ${DEFAULT_USER}:${DEFAULT_USER} /app
USER ${DEFAULT_USER}

ENTRYPOINT [ "dumb-init", "--", "sh", "-c", "java -Xmx512m $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom  -jar ./java-application.jar" ]
